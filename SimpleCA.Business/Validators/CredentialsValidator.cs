﻿using FluentValidation;
using Microsoft.Extensions.Localization;
using ResourceManager.Validators;
using SimpleCA.Domain.Models.Users;

namespace SimpleCA.Business.Validators
{
    /// <summary>
    /// Валидатор для модели авторизации
    /// </summary>
    /// <seealso cref="AbstractValidator{T}" />
    public sealed class CredentialsValidator : AbstractValidator<CredentialsModel>
    {
        private readonly IStringLocalizer<ValidationConstants> _localizer;
        /// <summary>
        /// Initializes a new instance of the <see cref="CredentialsValidator"/> class.
        /// </summary>
        public CredentialsValidator(
            IStringLocalizer<ValidationConstants> localizer,
            IStringLocalizer<ValidationFieldNameConstants> fieldLocalizer)
        {
            _localizer = localizer;
            RuleFor(_ => _.UserName)
                .Must(StringIsNotEmpty)
                .WithMessage(_localizer[ValidationConstants.NotEmptyFieldMessage])
                .WithName(fieldLocalizer[nameof(CredentialsModel.UserName)]);

            RuleFor(_ => _.Password)
                .Must(StringIsNotEmpty)
                .WithMessage(_localizer[ValidationConstants.NotEmptyFieldMessage])
                .WithName(fieldLocalizer[nameof(CredentialsModel.Password)]);
        }

        private bool StringIsNotEmpty(string? str)
            => !string.IsNullOrWhiteSpace(str);
    }
}
