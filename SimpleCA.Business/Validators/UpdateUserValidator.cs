﻿using System.Globalization;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using ResourceManager.Validators;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Helpers;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Domain.Models.Users;

namespace SimpleCA.Business.Validators
{
    /// <summary>
    /// Валидатор модели создания пользователя
    /// </summary>
    /// <seealso cref="AbstractValidator{T}" />
    public sealed class UpdateUserValidator : AbstractValidator<UserUpdateDto>
    {
        private readonly IUnitOfWork _ufw;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateUserValidator"/> class.
        /// </summary>
        /// <param name="ufw">The ufw.</param>
        /// <param name="configuration">The configuration.</param>
        public UpdateUserValidator(
            IUnitOfWork ufw, 
            IConfiguration configuration, 
            IStringLocalizer<ValidationConstants> localizer,
            IStringLocalizer<ValidationFieldNameConstants> fieldLocalizer)
        {
            _ufw = ufw;
            var rulesBuilder = new RulesBuilder<UserUpdateDto>(this, localizer);

            var passSettings = configuration.GetSection(nameof(PasswordPolicy)).Get<PasswordPolicy>();

            RuleFor(_ => _.UserName)
                .NotEmpty()
                .WithMessage(localizer[ValidationConstants.NotEmptyFieldMessage])
                .Length(0, 50)
                .WithMessage(localizer[ValidationConstants.Max50FieldLength])
                .WithName(fieldLocalizer[nameof(UserUpdateDto.UserName)]);

            RuleFor(_ => _.UserName)
                .MustAsync((x, y, c) => IsUnique(x))
                .When(UserNameNotEmpty)
                .WithMessage(localizer[ValidationConstants.UniqueFieldMessage])
                .Must(ValidationHelper.ObjectNameIsValid)
                .WithMessage(localizer[ValidationConstants.OnlyLatinAndSpecSymbolsTemplate])
                .WithName(fieldLocalizer[nameof(UserUpdateDto.UserName)]);

            rulesBuilder.Password(_ => _.Password!, passSettings,
                fieldLocalizer[nameof(UserUpdateDto.Password)])
                .Build();

            RuleFor(_ => _.Password)
                .Length(passSettings.RequiredLength, 50)
                .WithMessage(localizer[ValidationConstants.RequireLength])
                .When(_ => !string.IsNullOrWhiteSpace(_.Password))
                .WithName(fieldLocalizer[nameof(UserUpdateDto.Password)]);
        }

        private async Task<bool> IsUnique(UserUpdateDto model)
        {
            try
            {
                string userName = model.UserName.ToLower(CultureInfo.CurrentCulture);
#pragma warning disable CA1304 // Specify CultureInfo
                return !(await _ufw.Repository<User>()
                    .Query
                    .AnyAsync(_ => _.UserName.ToLower() == userName &&
                                   _.Id != model.Id)
                    .ConfigureAwait(false));
#pragma warning restore CA1304 // Specify CultureInfo
            }
            catch
            {
                return false;
            }
        }

        private bool UserNameNotEmpty(UserUpdateDto model)
            => !string.IsNullOrWhiteSpace(model.UserName);
    }
}
