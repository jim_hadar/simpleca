﻿using System.Globalization;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Localization;
using ResourceManager.Validators;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Helpers;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Domain.Models.Users;

namespace SimpleCA.Business.Validators
{
    /// <summary>
    /// Валидатор модели создания пользователя
    /// </summary>
    /// <seealso cref="AbstractValidator{T}" />
    public sealed class CreateUserValidator : AbstractValidator<UserAddDto>
    {
        private readonly IUnitOfWork _ufw;
        private readonly IStringLocalizer<ValidationConstants> _localizer;

        /// <summary>
        /// Initializes a new instance of the <see cref="CreateUserValidator"/> class.
        /// </summary>
        /// <param name="ufw">The ufw.</param>
        /// <param name="configuration">The configuration.</param>
        /// <param name="localizer"></param>
        /// <param name="fieldLocalizer"></param>
        public CreateUserValidator(
            IUnitOfWork ufw, 
            IConfiguration configuration, 
            IStringLocalizer<ValidationConstants> localizer,
            IStringLocalizer<ValidationFieldNameConstants> fieldLocalizer)
        {
            _ufw = ufw;
            _localizer = localizer;
            var rulesBuilder = new RulesBuilder<UserAddDto>(this, localizer);

            var passSettings = configuration.GetSection(nameof(PasswordPolicy)).Get<PasswordPolicy>();

            RuleFor(_ => _.UserName)
                .NotEmpty()
                .WithMessage(_localizer[ValidationConstants.NotEmptyFieldMessage])
                .Length(0, 50)
                .WithMessage(_localizer[ValidationConstants.Max50FieldLength])
                .WithName(fieldLocalizer[nameof(UserAddDto.UserName)]);

            RuleFor(_ => _.UserName)
                .MustAsync((x, y) => IsUnique(x))
                .When(UserNameNotEmpty)
                .WithMessage(_localizer[ValidationConstants.UniqueFieldMessage])
                .Must(ValidationHelper.ObjectNameIsValid)
                .WithMessage(_localizer[ValidationConstants.OnlyLatinAndSpecSymbolsTemplate])
                .WithName(fieldLocalizer[nameof(UserAddDto.UserName)]);

            rulesBuilder.Password(_ => _.Password!, passSettings, fieldLocalizer[nameof(UserAddDto.Password)])
                .Build();

            RuleFor(_ => _.Password)
                .NotEmpty()
                .WithMessage(_localizer[ValidationConstants.NotEmptyFieldMessage])
                .WithName(fieldLocalizer[nameof(UserAddDto.Password)]);

            RuleFor(_ => _.Password)
                .Length(passSettings.RequiredLength, 50)
                .WithMessage(_localizer[ValidationConstants.RequireLength])
                .When(_ => !string.IsNullOrWhiteSpace(_.Password))
                .WithName(fieldLocalizer[nameof(UserAddDto.Password)]);
        }

        private async Task<bool> IsUnique(string userName)
        {
            try
            {
                userName = userName.ToLower(CultureInfo.CurrentCulture);
#pragma warning disable CA1304 // Specify CultureInfo
                return !(await _ufw.Repository<User>()
                    .Query
                    .AnyAsync(_ => _.UserName.ToLower() == userName)
                    .ConfigureAwait(false));
#pragma warning restore CA1304 // Specify CultureInfo
            }
            catch
            {
                return false;
            }
        }

        private bool UserNameNotEmpty(UserAddDto model)
            => !string.IsNullOrWhiteSpace(model.UserName);
    }
}
