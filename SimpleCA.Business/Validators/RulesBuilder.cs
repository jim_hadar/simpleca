﻿using System;
using System.Linq.Expressions;
using FluentValidation;
using Microsoft.Extensions.Localization;
using ResourceManager.Validators;
using SimpleCA.Core.Helpers;
using SimpleCA.Domain.Models.Users;

namespace SimpleCA.Business.Validators
{
    /// <summary>
    /// Helper-builder для правил
    /// </summary>
    public class RulesBuilder<T>
    {
        private readonly AbstractValidator<T> _validator;
        private readonly IStringLocalizer<ValidationConstants> _localizer;

        /// <summary>
        /// Initializes a new instance of the <see cref="RulesBuilder{T}"/> class.
        /// </summary>
        /// <param name="validator">The validator.</param>
        public RulesBuilder(
            AbstractValidator<T> validator,
            IStringLocalizer<ValidationConstants> localizer)
        {
            _validator = validator;
            _localizer = localizer;
        }

        /// <summary>
        /// Устанавливает правила валидации для пароля
        /// </summary>
        /// <param name="expressionField">Поле, для которого необходимо установить правила валиацдии пароля</param>
        /// <param name="passwordPolicy"></param>
        /// <param name="fieldName">Название поля для ошибок</param>
        /// <returns></returns>
        public RulesBuilder<T> Password(Expression<Func<T, string>> expressionField, 
                                        PasswordPolicy passwordPolicy, 
                                        string fieldName = "Пароль")
        {
            _validator.RuleFor(expressionField)
                .Must(ValidationHelper.ContainsDigits)
                .WithMessage(_localizer[ValidationConstants.ContainsDigitsTemplate])
                .When(dto => passwordPolicy.RequireDigit, ApplyConditionTo.CurrentValidator)
                .WithName(fieldName);

            _validator.RuleFor(expressionField)
                .Must(ValidationHelper.ContainsLowerCase)
                .WithMessage(_localizer[ValidationConstants.ContainsLowerCaseTemplate])
                .When(dto => passwordPolicy.RequireLowercase, ApplyConditionTo.CurrentValidator)
                .WithName(fieldName);

            _validator.RuleFor(expressionField)
                .Must(ValidationHelper.ContainsUpperCase)
                .WithMessage(_localizer[ValidationConstants.ContainsUpperCaseTemplate])
                .When(dto => passwordPolicy.RequireUppercase, ApplyConditionTo.CurrentValidator)
                .WithName(fieldName);

            _validator.RuleFor(expressionField)
                .Must(ValidationHelper.ContainsNonAlphaNumeric)
                .WithMessage(_localizer[ValidationConstants.ContainsNonAlphaNumericTemplate])
                .When(dto => passwordPolicy.RequireNonAlphanumeric, ApplyConditionTo.CurrentValidator)
                .WithName(fieldName);

            return this;
        }

        /// <summary>
        /// Возвращает валидатор
        /// </summary>
        /// <returns></returns>
        public AbstractValidator<T> Build()
        {
            return _validator;
        }
    }
}
