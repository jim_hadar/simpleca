using System.Linq;
using AutoMapper;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Domain.Models.Users;

namespace SimpleCA.Business.AutoMapperProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserAddDto, User>()
                .ForMember(_ => _.UserName, o => o.MapFrom(_ => _.UserName))
                .ForMember(_ => _.Description, o => o.MapFrom(_ => _.Description))
                .ForAllOtherMembers(_ => _.Ignore());
            
            CreateMap<UserUpdateDto, User>()
                .ForMember(_ => _.UserName, o => o.MapFrom(_ => _.UserName))
                .ForMember(_ => _.Description, o => o.MapFrom(_ => _.Description))
                .ForAllOtherMembers(_ => _.Ignore());
            
            CreateMap<User, UserDto>()
                .ForMember(_ => _.RoleName, o => o.MapFrom(_ => _.Roles.First().Role.Name))
                .ForMember(_ => _.RoleId, o => o.MapFrom(_ => _.Roles.First().RoleId))
                .ForMember(_ => _.Password, o => o.Ignore())
                .ForAllOtherMembers(_ => _.UseDestinationValue());
            
            CreateMap<Role, RoleDto>()
                .ForAllMembers(_ => _.UseDestinationValue());
        }
    }
}
