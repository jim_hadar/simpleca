﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using ResourceManager.Users;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Domain.Commands.Users;
using SimpleCA.Logging.Contracts;

namespace SimpleCA.Business.Handlers.Commands.Users
{
    /// <summary>
    /// Обработчик команды создания пользователя.
    /// </summary>
    public class CreateUserHandler : IRequestHandler<CreateUserCommand, int>
    {
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IUnitOfWork _ufw;
        private readonly IStringLocalizer<UserConstants> _localizer;

        public CreateUserHandler(
            ILogger logger,
            IMapper mapper,
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            IUnitOfWork ufw,
            IStringLocalizer<UserConstants> localizer)
        {
            _logger = logger;
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
            _ufw = ufw;
            _localizer = localizer;
        }
        
        /// <inheritdoc />
        public async Task<int> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var model = request.Model;
            int roleId = (int)model.RoleId;
            try
            {
                _ufw.BeginTransaction();
                var entity = _mapper.Map<User>(model);
                var identityResult = await _userManager.CreateAsync(entity, model.Password).ConfigureAwait(false);
                if (!identityResult.Succeeded)
                {
                    throw new Exception(string.Join(", ", identityResult.Errors));
                }
                
                var role = _roleManager.Roles.First(_ => _.Id == roleId);
                identityResult = await _userManager.AddToRoleAsync(entity, role.Name).ConfigureAwait(false);
                if(!identityResult.Succeeded)
                    throw new Exception(string.Join(", ", identityResult.Errors));

                _logger.Info(_localizer[UserConstants.CreateUserSuccess].ToInvariantFormat(model.UserName));
                _ufw.CommitTransaction();
                
                return entity.Id;
            }
            catch (Exception ex)
            {
                _ufw.RollbackTransaction();
                string errorMessage = _localizer[UserConstants.CreateUserError].ToInvariantFormat(model.UserName, ex.Message);
                _logger.Error(ex, errorMessage);
                throw new CommonException(errorMessage, ex);
            }
        }
    }
}
