﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using ResourceManager.Users;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Domain.Commands.Users;
using SimpleCA.Logging.Contracts;

namespace SimpleCA.Business.Handlers.Commands.Users
{
    /// <summary>
    /// Обработчик команды обновления пользователя.
    /// </summary>
    public class UpdateUserHandler : INotificationHandler<UpdateUserCommand>
    {
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IUnitOfWork _ufw;
        private readonly IStringLocalizer<UserConstants> _localizer;

        public UpdateUserHandler(
            ILogger logger,
            IMapper mapper,
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            IUnitOfWork ufw,
            IStringLocalizer<UserConstants> localizer)
        {
            _logger = logger;
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
            _ufw = ufw;
            _localizer = localizer;
        }

        /// <inheritdoc />
        public async Task Handle(UpdateUserCommand notification, CancellationToken cancellationToken)
        {
            var model = notification.Model;
            int roleId = (int)model.RoleId;
            try
            {
                _ufw.BeginTransaction();
                // ReSharper disable once CA1305
                var entity = await _userManager.FindByIdAsync(model.Id.ToString(CultureInfo.InvariantCulture)).ConfigureAwait(false);
                entity = _mapper.Map(model, entity);
                var identityResult = await _userManager.UpdateAsync(entity).ConfigureAwait(false);
                if (!identityResult.Succeeded)
                {
                    throw new Exception(string.Join(", ", identityResult.Errors.Select(_ => _.Description)));
                }

                if (entity.Roles.All(_ => _.RoleId != roleId))
                {
                    identityResult = await _userManager.RemoveFromRoleAsync(entity, entity.Roles.First().Role.Name)
                        .ConfigureAwait(false);
                    var role = _roleManager.Roles.First(_ => _.Id == roleId);
                    identityResult = await _userManager.AddToRoleAsync(entity, role.Name).ConfigureAwait(false);
                    if (!identityResult.Succeeded)
                        throw new Exception(string.Join(", ", identityResult.Errors));
                }
                
                if (!string.IsNullOrWhiteSpace(model.Password))
                {
                    await _userManager.RemovePasswordAsync(entity).ConfigureAwait(false);
                    await _userManager.AddPasswordAsync(entity, model.Password).ConfigureAwait(false);
                }
                
                _logger.Info(_localizer[UserConstants.UpdateUserSuccess].ToInvariantFormat(model.UserName));
                _ufw.CommitTransaction();
            }
            catch (Exception ex)
            {
                _ufw.RollbackTransaction();
                string errorMessage = _localizer[UserConstants.UpdateUserError].ToInvariantFormat(model.UserName, ex.Message);
                _logger.Error(ex, errorMessage);
                throw new CommonException(errorMessage, ex);
            }
        }
    }
}
