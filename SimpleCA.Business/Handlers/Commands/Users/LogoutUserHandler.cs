using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SimpleCA.Auth.Services.Contracts;
using SimpleCA.Domain.Commands.Users;

namespace SimpleCA.Business.Handlers.Commands.Users
{
    /// <summary>
    /// Обработчик команды разлогинивания пользователя.
    /// </summary>
    public class LogoutUserHandler : INotificationHandler<LogoutUserCommand>
    {
        private readonly IAuthService _authService;
        
        public LogoutUserHandler(IAuthService authService)
        {
            _authService = authService;
        }

        /// <inheritdoc />
        public async Task Handle(LogoutUserCommand notification, CancellationToken cancellationToken)
        {
            await _authService.Logout().ConfigureAwait(false);
        }
    }
}
