using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using SimpleCA.Auth.Services.Contracts;
using SimpleCA.Domain.Commands.Users;
using SimpleCA.Domain.Models.Users;
using SimpleCA.Logging.Contracts;

namespace SimpleCA.Business.Handlers.Commands.Users
{
    /// <summary>
    /// Обработчик команды логина пользователя.
    /// </summary>
    public class LoginUserHandler : IRequestHandler<LoginUserCommand, JwtAuthSuccessResponse>
    {
        private readonly IAuthService _authService;
        
        public LoginUserHandler(IAuthService authService)
        {
            _authService = authService;
        }
        
        /// <inheritdoc />
        public async Task<JwtAuthSuccessResponse> Handle(LoginUserCommand request, CancellationToken cancellationToken)
        {
            return await _authService.LoginJwt(request.Model).ConfigureAwait(false);
        }
    }
}
