﻿using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using ResourceManager.Users;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Domain.Commands.Users;
using SimpleCA.Logging.Contracts;

namespace SimpleCA.Business.Handlers.Commands.Users
{
    /// <summary>
    /// Обработчик команды удаления пользователя.
    /// </summary>
    public class DeleteUserHandler : INotificationHandler<DeleteUserCommand>
    {
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly IStringLocalizer<UserConstants> _localizer;

        public DeleteUserHandler(
            ILogger logger,
            IMapper mapper,
            UserManager<User> userManager,
            RoleManager<Role> roleManager,
            IStringLocalizer<UserConstants> localizer)
        {
            _logger = logger;
            _mapper = mapper;
            _userManager = userManager;
            _roleManager = roleManager;
            _localizer = localizer;
        }
        
        /// <inheritdoc />
        public async Task Handle(DeleteUserCommand notification, CancellationToken cancellationToken)
        {
            var userId = notification.UserId;
            try
            {
                var user = await _userManager.FindByIdAsync(userId.ToString(CultureInfo.InvariantCulture)).ConfigureAwait(false);
                if (user != null)
                {
                    await _userManager.DeleteAsync(user).ConfigureAwait(false);
                    _logger.Info(_localizer[UserConstants.UserDeleteSuccess].ToInvariantFormat(user.UserName));
                }
            }
            catch (Exception ex)
            {
                string message = _localizer[UserConstants.UserDeleteError].ToInvariantFormat(userId, ex.Message);
                _logger.Error(ex, message);
                throw new CommonException(message);
            }
        }
    }
}
