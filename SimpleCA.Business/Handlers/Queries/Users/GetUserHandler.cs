﻿using System;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using ResourceManager.Users;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Domain.Models.Users;
using SimpleCA.Domain.Queries.Users;
using SimpleCA.Logging.Contracts;

namespace SimpleCA.Business.Handlers.Queries.Users
{
    /// <summary>
    /// Обработчик запроса получения пользователя по его идентификатору.
    /// </summary>
    public class GetUserHandler : IRequestHandler<GetUserQuery, UserDto>
    {
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly IStringLocalizer<UserConstants> _localizer;


        public GetUserHandler(
            ILogger logger,
            IMapper mapper,
            UserManager<User> userManager,
            IStringLocalizer<UserConstants> localizer)
        {
            _logger = logger;
            _mapper = mapper;
            _userManager = userManager;
            _localizer = localizer;
        }
        
        /// <inheritdoc />
        public async Task<UserDto> Handle(GetUserQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var user = await _userManager.FindByIdAsync(request.UserId.ToString(CultureInfo.InvariantCulture)).ConfigureAwait(false);
                if (user == null)
                    throw new EntityNotFoundException(_localizer[UserConstants.UserNotFound].ToInvariantFormat());
                return _mapper.Map<UserDto>(user);
            }
            catch (EntityNotFoundException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new CommonException(_localizer[UserConstants.GetUserQueryError].ToInvariantFormat(request.UserId, ex.Message), ex);
            }
        }
    }
}
