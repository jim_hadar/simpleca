﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using SimpleCA.Core.Extensions;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Domain.Models.Paging;
using SimpleCA.Domain.Models.Users;
using SimpleCA.Domain.Queries.Users;
using SimpleCA.Logging.Contracts;
using System.Linq.Dynamic.Core;
using SimpleCA.Domain.Extensions;
using Microsoft.Extensions.Localization;
using ResourceManager.Users;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Exceptions;

namespace SimpleCA.Business.Handlers.Queries.Users
{
    /// <summary>
    /// Обработчик запроса получения списка пользователей с постраничной разбивкой.
    /// </summary>
    public class GetPagedUsersHandler : IRequestHandler<GetPagedUsersQuery, PagedList<UserDto>>
    {
        private readonly IUnitOfWork _ufw;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly IStringLocalizer<UserConstants> _localizer;

        public GetPagedUsersHandler(
            IUnitOfWork ufw,
            ILogger logger,
            IMapper mapper,
            IStringLocalizer<UserConstants> localizer)
        {
            _ufw = ufw;
            _logger = logger;
            _mapper = mapper;
            _localizer = localizer;
        }
        /// <inheritdoc />
        public async Task<PagedList<UserDto>> Handle(GetPagedUsersQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var query = _ufw.Repository<User>().Query;
                query = Search(query, request.Filter.Filter.Search);
                query = Sorting(query, request.Filter.Filter.OrderBy, request.Filter.Filter.SortField);
                var list = await query.TakePaged(request.Filter).ConfigureAwait(false);
                var resultList = _mapper.Map<List<UserDto>>(list);
                return resultList.ToPagedList(request.Filter, query.Count());
            }
            catch (Exception ex)
            {
                string message = _localizer[UserConstants.GetPagedUsersQueryError].ToInvariantFormat(ex.Message);
                _logger.Error(ex, message);
                throw new CommonException(message);
            }
        }
        
        private IQueryable<User> Search(IQueryable<User> data, string search)
        {
            if (string.IsNullOrWhiteSpace(search))
                return data;
            string lowerSort = search.ToLower(CultureInfo.CurrentCulture);
#pragma warning disable CA1304 // Specify CultureInfo
            return data.Where(_ => !string.IsNullOrWhiteSpace(_.UserName) && _.UserName.ToLower().Contains(lowerSort) ||
                                   !string.IsNullOrWhiteSpace(_.Description) && _.Description.ToLower().Contains(lowerSort));
#pragma warning restore CA1304 // Specify CultureInfo
        }


        private IQueryable<User> Sorting(IQueryable<User> data, SortOrderBy orderBy, string sortField)
        {
            return data.OrderBy($"{sortField} {orderBy.ToString()}");
        }
    }
}
