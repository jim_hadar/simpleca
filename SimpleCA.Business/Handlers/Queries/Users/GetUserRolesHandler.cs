using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Domain.Models.Users;
using SimpleCA.Domain.Queries.Users;
using SimpleCA.Logging.Contracts;

namespace SimpleCA.Business.Handlers.Queries.Users
{
    /// <summary>
    /// Обработчик запроса получения списка ролей пользователей.
    /// </summary>
    public class GetUserRolesHandler : IRequestHandler<GetUserRolesQuery, List<RoleDto>>
    {
        private readonly IMapper _mapper;
        private readonly RoleManager<Role> _roleManager;
        private readonly ILogger _logger;

        public GetUserRolesHandler(
            IMapper mapper,
            RoleManager<Role> roleManager,
            ILogger logger)
        {
            _mapper = mapper;
            _roleManager = roleManager;
            _logger = logger;
        }

        /// <inheritdoc />
        public async Task<List<RoleDto>> Handle(GetUserRolesQuery request, CancellationToken cancellationToken)
        {
            var roles = await _roleManager.Roles.ToListAsync(cancellationToken).ConfigureAwait(false);
            return _mapper.Map<List<RoleDto>>(roles);
        }
    }
}
