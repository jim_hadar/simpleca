namespace SimpleCA.Core.Contracts
{
    /// <summary>
    /// Базовый провайдер
    /// </summary>
    public interface IProviderBase<T> : IProviderBase
        where T : class, new()
    {
        public IRepository<T> EntityRepo { get; }
    }

    public interface IProviderBase
    {
        IUnitOfWork Ufw { get; }
    }
}
