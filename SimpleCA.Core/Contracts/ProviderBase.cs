using AutoMapper;

namespace SimpleCA.Core.Contracts
{
    /// <inheritdoc />
    public abstract class ProviderBase<T> : IProviderBase<T>
        where T : class, new()
    {
        public IUnitOfWork Ufw { get; }
        protected readonly IMapper Mapper;

        public IRepository<T> EntityRepo => Ufw.Repository<T>();

        protected ProviderBase(IUnitOfWork ufw, IMapper mapper)
        {
            Ufw = ufw;
            Mapper = mapper;
        }
    }
}
