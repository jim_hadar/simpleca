﻿using System.Globalization;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Localization;

namespace SimpleCA.Core.Extensions
{
    /// <summary>
    /// Расширения для работы со строками
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Преобразование входной строки к нотации snack_case.
        /// </summary>
        /// <param name="input">Входная строка для преобразования.</param>
        /// <returns>Преобразованная строка</returns>
        public static string ToSnakeCase(this string input)
        {
            if (string.IsNullOrEmpty(input)) { return input; }

            var startUnderscores = Regex.Match(input, @"^_+");
            return startUnderscores + Regex.Replace(input, @"([a-z0-9])([A-Z])", "$1_$2").ToLower(CultureInfo.InvariantCulture);
        }

        public static string ToInvariantFormat(this string template, params object[] args)
        {
            return string.Format(CultureInfo.InvariantCulture, template, args);
        }

        public static string ToInvariantFormat(this LocalizedString template, params object[] args)
        {
            var value = template.Value;
            return string.Format(CultureInfo.InvariantCulture, value, args);
        }
    }
}
