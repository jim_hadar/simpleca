﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleCA.Core.Extensions
{
    public static class AsyncExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public static Task<T> GetActionAsync<T>(Func<T> action, CancellationToken token = default)
            where T : class
        {
            return Task.Factory.StartNew(action, token, TaskCreationOptions.AttachedToParent, TaskScheduler.Default);
        }

        /// <summary>
        /// Запуск ожидания, если время <paramref name="delay"/> превышает Int.MaxValue
        /// </summary>
        /// <param name="delay">Задержка</param>
        /// <param name="cancellationToken">CancellationToken</param>
        /// <returns></returns>
        public static async Task LongDelay(TimeSpan delay, CancellationToken cancellationToken = default)
        {
            var start = DateTime.UtcNow;
            while (!cancellationToken.IsCancellationRequested)
            {
                cancellationToken.ThrowIfCancellationRequested();
                var remaining = (delay - (DateTime.UtcNow - start)).TotalMilliseconds;
                if (remaining <= 0)
                    break;
                if (remaining > short.MaxValue)
                    remaining = short.MaxValue;
                await Task.Delay(TimeSpan.FromMilliseconds(remaining), cancellationToken).ConfigureAwait(false);
            }
        }
    }
}
