using System;

namespace SimpleCA.Core.Exceptions
{
    /// <summary>
    /// Исключение, возникающее при неудачной авторизации
    /// </summary>
    public sealed class AuthorizationErrorException 
        : CommonException
    {
        
        /// <inheritdoc />
        public AuthorizationErrorException(string? message) : base(message)
        {
        }

        /// <inheritdoc />
        public AuthorizationErrorException(string? message, Exception? innerException) 
            : base(message, innerException)
        {
        }
    }
}
