using System;

namespace SimpleCA.Core.Exceptions
{
    /// <summary>
    /// Исключение для security операций
    /// </summary>
    public class SecurityException : CommonException
    {
        /// <inheritdoc />
        public SecurityException(string? message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public SecurityException(string? message, Exception? innerException)
            : base(message, innerException)
        {
        }
    }
}
