using System;

namespace SimpleCA.Core.Exceptions
{
    /// <summary>
    /// Исключение, возникающее в случае, если сущность не найдена. 
    /// </summary>
    public class EntityNotFoundException : CommonException
    {
        /// <inheritdoc />
        public EntityNotFoundException(string message) : base(message)
        {
        }
        
        /// <inheritdoc />
        public EntityNotFoundException(string? message, Exception? innerException)
            : base(message, innerException)
        {
        }
    }
}
