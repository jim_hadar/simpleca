using System;

namespace SimpleCA.Core.Exceptions
{
    /// <summary>
    /// Общее исключение для проекта.
    /// </summary>
    public class CommonException : Exception
    {
        /// <inheritdoc />
        public CommonException(string? message)
            : base(message)
        {
        }

        /// <inheritdoc />
        public CommonException(string? message, Exception? innerException)
            : base(message, innerException)
        {
        }

        public CommonException()
        {
        }
    }
}
