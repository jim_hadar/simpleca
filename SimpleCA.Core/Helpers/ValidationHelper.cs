﻿using System.Linq;
using System.Text.RegularExpressions;

namespace SimpleCA.Core.Helpers
{
    /// <summary>
    /// Методы валидации
    /// </summary>
    public static class ValidationHelper
    {
        /// <summary>
        /// Проверка названия сущности на корректность:
        /// - содержит только буквы или цифры латинского алфавита;
        /// - содержит знаки "-", "_"
        /// </summary>
        /// <param name="objectName">Строка для проверки</param>
        /// <returns>Результат проверки</returns>
        public static bool ObjectNameIsValid(string? objectName) =>
            !string.IsNullOrEmpty(objectName) &&
            Regex.IsMatch(objectName, "^([a-zA-Z0-9_-]*)$");

        /// <summary>
        /// Проверка валидности IP адреса
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static bool BeIp(string ip)
        {
            if(!string.IsNullOrWhiteSpace(ip))
            {
                string pattern = @"^(?:(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])(\.(?!$)|$)){4}$";
                var result = Regex.IsMatch(ip, pattern);
                return result;
            }

            return false;
        }

        /// <summary>
        /// Валидация для полей запроса на сертификат и шаблона клиентского сертификата.
        /// </summary>
        /// <param name="subjectPart">Часть поля "Субъект".</param>
        /// <returns>Результат валидации.</returns>
        public static bool CertificateSubjectFieldValid(string subjectPart)
        {
            if (string.IsNullOrWhiteSpace(subjectPart))
                return true;
            var rx = new Regex("^[a-zA-Z|0-9|а-яА-Я|  ]*$", RegexOptions.Compiled | RegexOptions.CultureInvariant);
            return rx.IsMatch(subjectPart);
        }

        #region [ Password policy validator ]

        /// <summary>
        /// Проверка строки на то, что она содержит кириллические буквы
        /// </summary>
        /// <param name="template"></param>
        /// <returns></returns>
        public static bool NotContainsCyrillic(string? template)
        {
            if (string.IsNullOrEmpty(template))
            {
                return true;
            }
            string pattern = "[а-яА-Я]";
            return !Regex.IsMatch(template, pattern);
        }

        /// <summary>
        /// Проверка наличия символов в верхнем регистре
        /// </summary>
        /// <param name="str">Строка для проверки</param>
        /// <returns>Флаг наличия</returns>
        public static bool ContainsUpperCase(string? str) 
            => string.IsNullOrEmpty(str) || str.Any(char.IsUpper);

        /// <summary>
        /// Проверка наличия символов в верхнем регистре
        /// </summary>
        /// <param name="str">Строка для проверки</param>
        /// <returns>Флаг наличия</returns>
        public static bool ContainsLowerCase(string? str) 
            => string.IsNullOrEmpty(str) || str.Any(char.IsLower);

        /// <summary>
        /// Проверка наличия нечисловых и нецифровых символов в верхнем регистре
        /// </summary>
        /// <param name="str">Строка для проверки</param>
        /// <returns>Флаг наличия</returns>
        public static bool ContainsNonAlphaNumeric(string? str) 
            => string.IsNullOrEmpty(str) || !str.All(char.IsLetterOrDigit);

        /// <summary>
        /// Проверка наличия цифр
        /// </summary>
        /// <param name="str">Строка для проверки</param>
        /// <returns>Флаг наличия</returns>
        public static bool ContainsDigits(string? str) 
            => string.IsNullOrEmpty(str) || str.Any(char.IsDigit);

        #endregion
    }
}
