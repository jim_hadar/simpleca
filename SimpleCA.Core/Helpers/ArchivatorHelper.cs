﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Zip.Compression;
using SimpleCA.Core.Exceptions;

namespace SimpleCA.Core.Helpers
{
    public static class ArchivatorHelper
    {
        /// <summary>
        /// Создание архива типа *.zip с абсолютным путем <seealso cref="fullArchivePath"/>
        /// с содержимым файла <seealso cref="pathToFileForArchive"/>
        /// </summary>
        /// <param name="fullArchivePath">Создаваемый файл архива</param>
        /// <param name="pathToFileForArchive">Путь до файла, который необходимо архивировать</param>
        /// <param name="password">Пароль, с которым создается архив</param>
        /// <exception cref="FileNotFoundException">Если файл для архивации не найден</exception>
        public static void CreateZipArchive(
            string fullArchivePath,
            string pathToFileForArchive,
            string? password = null)
        {
            if (Path.GetExtension(fullArchivePath) != ".zip")
            {
                throw new CommonException("Not supported archive type. Supported only zip type");
            }

            if (!File.Exists(pathToFileForArchive))
            {
                throw new CommonException("File for archivating not found");
            }

            if (File.Exists(fullArchivePath))
            {
                File.Delete(fullArchivePath);
            }

            var fastZip = new FastZip
            {
                Password = password,
                CompressionLevel = Deflater.CompressionLevel.BEST_COMPRESSION
            };

            fastZip.CreateZip(fullArchivePath, Path.GetDirectoryName(pathToFileForArchive), false, Path.GetFileName(pathToFileForArchive));
        }

        /// <summary>
        /// Создание архива типа *.zip с абсолютным путем <seealso cref="fullArchivePath"/>
        /// с содержимым файла <seealso cref="pathToFileForArchive"/>
        /// </summary>
        /// <param name="fullArchivePath">Создаваемый файл архива</param>
        /// <param name="pathToDirForArchive">Путь до директории, который необходимо архивировать</param>
        /// <param name="password">Пароль, с которым создается архив</param>
        /// <exception cref="FileNotFoundException">Если файл для архивации не найден</exception>
        public static void CreateZipArchiveForDir(
            string fullArchivePath,
            string pathToDirForArchive,
            string? password = null)
        {
            if (Path.GetExtension(fullArchivePath) != ".zip")
            {
                throw new CommonException("Not supported archive type. Supported only zip type");
            }

            if (!Directory.Exists(pathToDirForArchive))
            {
                throw new CommonException("File for archivating not found");
            }

            if (File.Exists(fullArchivePath))
            {
                File.Delete(fullArchivePath);
            }

            var fastZip = new FastZip
            {
                Password = password,
                CompressionLevel = Deflater.CompressionLevel.BEST_COMPRESSION
            };

            fastZip.CreateZip(fullArchivePath, pathToDirForArchive, false, null);
        }

        /// <summary>
        /// Извлечение файла <see cref="fileToExport"/> из архива <see cref="fullArchivePath"/>
        /// Если указан пароль <see cref="password"/>, то производится попытка ввода пароля для архива
        /// </summary>
        /// <param name="fullArchivePath">Полный путь до архива</param>
        /// <param name="pathToExtract">Директория, куда необходимо извлечь файл</param>
        /// <param name="fileToExport">Название файла, который необходимо получить из архива</param>
        /// <param name="password">Пароль для архива</param>
        /// <exception cref="FileNotFoundException">Если не найден архив</exception>
        public static void ExtractFileFromArchive(
            string fullArchivePath,
            string pathToExtract,
            string fileToExport,
            string? password =
            null)
        {
            if (!File.Exists(fullArchivePath))
            {
                throw new FileNotFoundException("Archive not found.");
            }

            var fastZip = new FastZip
            {
                Password = password
            };
            fastZip.ExtractZip(fullArchivePath, pathToExtract, fileToExport);
        }
    }
}
