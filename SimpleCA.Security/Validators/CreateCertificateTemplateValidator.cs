﻿using System.Linq;
using FluentValidation;
using Microsoft.Extensions.Localization;
using ResourceManager.Validators;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Helpers;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Domain.Models.Certificates;

namespace SimpleCA.Security.Validators
{
    /// <summary>
    /// Валидатор шаблонов на сертификаты.
    /// </summary>
    public class CreateCertificateTemplateValidator : AbstractValidator<CertificateTemplateCreateDto>
    {
        private readonly IUnitOfWork _ufw;
        public CreateCertificateTemplateValidator(
            IUnitOfWork ufw,
            IStringLocalizer<ValidationConstants> localizer,
            IStringLocalizer<ValidationFieldNameConstants> fieldLocalizer)
        {
            _ufw = ufw;

            RuleFor(_ => _.Name)
                .NotEmpty()
                .WithMessage(localizer[ValidationConstants.NotEmptyFieldMessage])
                .Length(0, 50)
                .WithMessage(localizer[ValidationConstants.Max50FieldLength])
                .WithName(fieldLocalizer[nameof(CertificateTemplateCreateDto.Name)]);
            
            RuleFor(_ => _.Name)
                .Must(IsUnique)
                .WithMessage(localizer[ValidationConstants.UniqueFieldMessage])
                .Must(ValidationHelper.ObjectNameIsValid)
                .WithMessage(localizer[ValidationConstants.OnlyLatinAndSpecSymbolsTemplate])
                .When(_ => !string.IsNullOrWhiteSpace(_.Name))
                .WithName(fieldLocalizer[nameof(CertificateTemplateCreateDto.Name)]);

            RuleFor(_ => _.ValidPeriod)
                .GreaterThanOrEqualTo(180)
                .WithMessage(localizer[ValidationConstants.Min180DaysCertificateValidPeriod])
                .LessThanOrEqualTo(3652)
                .WithMessage(localizer[ValidationConstants.Max3652DaysCertificateValidPeriod])
                .WithName(fieldLocalizer[nameof(CertificateTemplateCreateDto.ValidPeriod)]);

            RuleFor(_ => _.Country)
                .Length(1, 2)
                .WithMessage(localizer[ValidationConstants.Min1SymbolMax2SymbolLength])
                .Must(ValidationHelper.CertificateSubjectFieldValid)
                .WithMessage(localizer[ValidationConstants.CertificateSubjectTemplate])
                .WithName(fieldLocalizer[nameof(CertificateTemplateCreateDto.Country)]);

            RuleFor(_ => _.City)
                .Length(0, 250)
                .WithMessage(localizer[ValidationConstants.Max250FieldLength])
                .Must(ValidationHelper.CertificateSubjectFieldValid)
                .WithMessage(localizer[ValidationConstants.CertificateSubjectTemplate])
                .WithName(fieldLocalizer[nameof(CertificateTemplateCreateDto.City)]);

            RuleFor(_ => _.CommonName)
                .Length(0, 250)
                .WithMessage(localizer[ValidationConstants.Max250FieldLength])
                .Must(ValidationHelper.CertificateSubjectFieldValid)
                .WithMessage(localizer[ValidationConstants.CertificateSubjectTemplate])
                .WithName(fieldLocalizer[nameof(CertificateTemplateCreateDto.CommonName)]);

            RuleFor(_ => _.Organization)
                .NotEmpty()
                .WithMessage(localizer[ValidationConstants.NotEmptyFieldMessage])
                .Length(0, 250)
                .WithMessage(localizer[ValidationConstants.Max250FieldLength])
                .Must(ValidationHelper.CertificateSubjectFieldValid)
                .WithMessage(localizer[ValidationConstants.CertificateSubjectTemplate])
                .WithName(fieldLocalizer[nameof(CertificateTemplateCreateDto.Organization)]);

            RuleFor(_ => _.OrganizationUnit)                
                .Length(0, 250)
                .WithMessage(localizer[ValidationConstants.Max250FieldLength])
                .Must(ValidationHelper.CertificateSubjectFieldValid)
                .WithMessage(localizer[ValidationConstants.CertificateSubjectTemplate])
                .WithName(fieldLocalizer[nameof(CertificateTemplateCreateDto.OrganizationUnit)]);

            RuleFor(_ => _.State)
                .NotEmpty()
                .WithMessage(localizer[ValidationConstants.NotEmptyFieldMessage])
                .Length(0, 250)
                .WithMessage(localizer[ValidationConstants.Max250FieldLength])
                .Must(ValidationHelper.CertificateSubjectFieldValid)
                .WithMessage(localizer[ValidationConstants.CertificateSubjectTemplate])
                .WithName(fieldLocalizer[nameof(CertificateTemplateCreateDto.State)]);

            RuleFor(_ => _.ExtendedKeyUsageClientAuth)
                .Must((x, _) => x.ExtendedKeyUsageClientAuth && !x.ExtendedKeyUsageServerAuth)
                .When(_ => _.ExtendedKeyUsageClientAuth)
                .WithMessage(localizer[ValidationConstants.CertificateTemplateMustBeExtendedClient])
                .WithName(fieldLocalizer[nameof(CertificateTemplateCreateDto.ExtendedKeyUsageClientAuth)]);

            RuleFor(_ => _.ExtendedKeyUsageServerAuth)
                .Must((x, _) => x.ExtendedKeyUsageServerAuth && !x.ExtendedKeyUsageClientAuth)
                .When(_ => _.ExtendedKeyUsageServerAuth)
                .WithMessage(localizer[ValidationConstants.CertificateTemplateMustBeExtendedServer])
                .WithName(fieldLocalizer[nameof(CertificateTemplateCreateDto.ExtendedKeyUsageServerAuth)]);
        }

        private bool IsUnique(string name)
        {
            return !(_ufw.Repository<IssuedCertificateTemplate>().Query
                .Any(_ => _.Name.ToLower() == name.ToLower()));
        }
    }
}
