﻿using System;
using System.Collections.Generic;
using System.Text;
using FluentValidation;
using Microsoft.EntityFrameworkCore.Internal;
using SimpleCA.Core.Contracts;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Domain.Models.Certificates.IssuedCertificateModels;
using System.Linq;
using Microsoft.Extensions.Localization;
using ResourceManager.Validators;

namespace SimpleCA.Security.Validators
{
    /// <summary>
    /// Валидатор для модели экспорт сертификата.
    /// </summary>
    public class ExportIssuedCertificateValidator : AbstractValidator<ExportIssuedCertificateDto>
    {
        public ExportIssuedCertificateValidator(
            IUnitOfWork ufw,
            IStringLocalizer<ValidationConstants> localizer,
            IStringLocalizer<ValidationFieldNameConstants> fieldLocalizer)
        {
            RuleFor(_ => _.CertificateId)
                .Must(_ => ufw.Repository<IssuedCertificate>().Query.Any(c => c.Id == _ && !c.RevocationDate.HasValue))
                .WithMessage(localizer[ValidationConstants.IssuedCertificateNotFoundOrRevoked]);
        }
    }
}
