﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentValidation;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.Localization;
using ResourceManager.Validators;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Helpers;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Domain.Models.Certificates.IssuedCertificateModels;

namespace SimpleCA.Security.Validators
{
    public sealed class CreateIssuedCertificateValidator : AbstractValidator<IssuedCertificateCreateDto>
    {
        private readonly IStringLocalizer<ValidationConstants> _localizer;
        public CreateIssuedCertificateValidator(
            IStringLocalizer<ValidationConstants> localizer,
            IStringLocalizer<ValidationFieldNameConstants> fieldNamesLocalizer,
            IUnitOfWork ufw)
        {
            _localizer = localizer;

            RuleFor(_ => _.CommonName)
                .NotEmpty()
                .WithMessage(localizer[ValidationConstants.NotEmptyFieldMessage])
                .Must(ValidationHelper.CertificateSubjectFieldValid)
                .WithMessage(localizer[ValidationConstants.CertificateSubjectTemplate])
                .WithName(fieldNamesLocalizer[nameof(IssuedCertificateCreateDto.CommonName)]);

            RuleFor(_ => _.Sans)
                .NotEmpty()
                .WithMessage(localizer[ValidationConstants.NotEmptyFieldMessage])
                .WithName(fieldNamesLocalizer[nameof(IssuedCertificateCreateDto.Sans)]);

            RuleFor(_ => _.TemplateId)
                .Must(_ => ufw.Repository<IssuedCertificateTemplate>().Query.Any(t => t.Id == _))
                .WithMessage(localizer[ValidationConstants.IssuedCertificateTemplateNotFound]);
        }
    }
}
