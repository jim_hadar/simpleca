﻿using System.Linq;
using FluentValidation;
using Microsoft.Extensions.Localization;
using ResourceManager.Validators;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Helpers;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Domain.Models.Certificates;

namespace SimpleCA.Security.Validators
{
    /// <summary>
    /// Валидатор модели обновления шаблона.
    /// </summary>
    public class UpdateCertificateTemplateValidator : AbstractValidator<CertificateTemplateUpdateDto>
    {
        private readonly IUnitOfWork _ufw;
        public UpdateCertificateTemplateValidator(
            IUnitOfWork ufw,
            IStringLocalizer<ValidationConstants> localizer,
            IStringLocalizer<ValidationFieldNameConstants> fieldLocalizer)
        {
            _ufw = ufw;
            RuleFor(_ => _.Name)
                .NotEmpty()
                .WithMessage(localizer[ValidationConstants.NotEmptyFieldMessage])
                .Length(0, 50)
                .WithMessage(localizer[ValidationConstants.Max50FieldLength])
                .WithName(fieldLocalizer[nameof(CertificateTemplateUpdateDto.Name)]);

            RuleFor(_ => _.Name)
                .Must((x, y) => IsUnique(x))
                .WithMessage(localizer[ValidationConstants.UniqueFieldMessage])
                .Must(ValidationHelper.ObjectNameIsValid)
                .WithMessage(localizer[ValidationConstants.OnlyLatinAndSpecSymbolsTemplate])
                .When(_ => !string.IsNullOrWhiteSpace(_.Name))
                .WithName(fieldLocalizer[nameof(CertificateTemplateUpdateDto.Name)]);

            RuleFor(_ => _.ValidPeriod)
                .GreaterThanOrEqualTo(180)
                .WithMessage(localizer[ValidationConstants.Min180DaysCertificateValidPeriod])
                .LessThanOrEqualTo(3652)
                .WithMessage(localizer[ValidationConstants.Max3652DaysCertificateValidPeriod])
                .WithName(fieldLocalizer[nameof(CertificateTemplateUpdateDto.ValidPeriod)]);

            RuleFor(_ => _.Country)
                .Length(1, 2)
                .WithMessage(localizer[ValidationConstants.Min1SymbolMax2SymbolLength])
                .Must(ValidationHelper.CertificateSubjectFieldValid)
                .WithMessage(localizer[ValidationConstants.CertificateSubjectTemplate])
                .WithName(fieldLocalizer[nameof(CertificateTemplateUpdateDto.Country)]);

            RuleFor(_ => _.City)
                .Length(0, 250)
                .WithMessage(localizer[ValidationConstants.Max250FieldLength])
                .Must(ValidationHelper.CertificateSubjectFieldValid)
                .WithMessage(localizer[ValidationConstants.CertificateSubjectTemplate])
                .WithName(fieldLocalizer[nameof(CertificateTemplateUpdateDto.City)]);

            RuleFor(_ => _.CommonName)
                .Length(0, 250)
                .WithMessage(localizer[ValidationConstants.Max250FieldLength])
                .Must(ValidationHelper.CertificateSubjectFieldValid)
                .WithMessage(localizer[ValidationConstants.CertificateSubjectTemplate])
                .WithName(fieldLocalizer[nameof(CertificateTemplateUpdateDto.CommonName)]);

            RuleFor(_ => _.Organization)
                .Length(0, 250)
                .WithMessage(localizer[ValidationConstants.Max250FieldLength])
                .Must(ValidationHelper.CertificateSubjectFieldValid)
                .WithMessage(localizer[ValidationConstants.CertificateSubjectTemplate])
                .WithName(fieldLocalizer[nameof(CertificateTemplateUpdateDto.Organization)]);

            RuleFor(_ => _.OrganizationUnit)
                .Length(0, 250)
                .WithMessage(localizer[ValidationConstants.Max250FieldLength])
                .Must(ValidationHelper.CertificateSubjectFieldValid)
                .WithMessage(localizer[ValidationConstants.CertificateSubjectTemplate])
                .WithName(fieldLocalizer[nameof(CertificateTemplateUpdateDto.OrganizationUnit)]);

            RuleFor(_ => _.State)
                .Length(0, 250)
                .WithMessage(localizer[ValidationConstants.Max250FieldLength])
                .Must(ValidationHelper.CertificateSubjectFieldValid)
                .WithMessage(localizer[ValidationConstants.CertificateSubjectTemplate])
                .WithName(fieldLocalizer[nameof(CertificateTemplateUpdateDto.State)]);

            RuleFor(_ => _.ExtendedKeyUsageClientAuth)
                .Must((x, _) => x.ExtendedKeyUsageClientAuth && !x.ExtendedKeyUsageServerAuth)
                .When(_ => _.ExtendedKeyUsageClientAuth)
                .WithMessage(localizer[ValidationConstants.CertificateTemplateMustBeExtendedClient])
                .WithName(fieldLocalizer[nameof(CertificateTemplateUpdateDto.ExtendedKeyUsageClientAuth)]);

            RuleFor(_ => _.ExtendedKeyUsageServerAuth)
                .Must((x, _) => x.ExtendedKeyUsageServerAuth && !x.ExtendedKeyUsageClientAuth)
                .When(_ => _.ExtendedKeyUsageServerAuth)
                .WithMessage(localizer[ValidationConstants.CertificateTemplateMustBeExtendedServer])
                .WithName(fieldLocalizer[nameof(CertificateTemplateUpdateDto.ExtendedKeyUsageServerAuth)]);
        }

        private bool IsUnique(CertificateTemplateUpdateDto model)
        {
            return !(_ufw.Repository<IssuedCertificateTemplate>().Query
                .Any(_ => _.Name.ToLower() == model.Name.ToLower() && _.Id != model.Id));
        }
    }
}
