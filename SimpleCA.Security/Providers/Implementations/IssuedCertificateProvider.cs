﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using ResourceManager.Security;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Domain.Models.Certificates;
using SimpleCA.Domain.Models.Certificates.IssuedCertificateModels;
using SimpleCA.Domain.Models.Paging;
using SimpleCA.Domain.Models.Paging.Filters;
using SimpleCA.Security.Internal;
using SimpleCA.Security.Providers.Contracts;
using System.Linq.Dynamic.Core;
using SimpleCA.Domain.Extensions;

namespace SimpleCA.Security.Providers.Implementations
{
    /// <inheritdoc cref="IIssuedCertificateProvider"/>
    public sealed class IssuedCertificateProvider : ProviderBase<IssuedCertificate>, IIssuedCertificateProvider
    {
        private readonly IStringLocalizer<SecurityMessageConstants> _securityMessageLocalizer;
        private readonly IRootCertificateProvider _rootCertificateProvider;
        private readonly IStringLocalizer<IssuedCertificateConstants> _localizer;
        private IRepository<IssuedCertificateTemplate> TemplateRepo => Ufw.Repository<IssuedCertificateTemplate>();
        public IssuedCertificateProvider(
            IUnitOfWork ufw,
            IMapper mapper,
            IStringLocalizer<SecurityMessageConstants> securityMessageLocalizer,
            IStringLocalizer<IssuedCertificateConstants> localizer,
            IRootCertificateProvider rootCertificateProvider)
            : base(ufw, mapper)
        {
            _localizer = localizer;
            _securityMessageLocalizer = securityMessageLocalizer;
            _rootCertificateProvider = rootCertificateProvider;
        }

        /// <inheritdoc />
        public async Task<IssuedCertificateDto> CreateAsync(IssuedCertificateCreateDto model, CancellationToken cancellationToken)
        {
            var caCertEntity = await _rootCertificateProvider.GetActualRootCertificate(cancellationToken).ConfigureAwait(false);
            var template = await TemplateRepo.Query.FirstAsync(_ => _.Id == model.TemplateId).ConfigureAwait(false);

            var issuedKey = RsaHelper.GenerateRsaKey();
            var issuedCert = CreateIssuedCertificate(template, model, issuedKey, caCertEntity, out var sansString);

            var issuedCertEntity = new IssuedCertificate
            {
                PrivateKey = issuedKey.ToBase64(),
                Encoded = issuedCert.ToPKCS8String(),
                RootCertificateId = caCertEntity.Id,
                RootCertificate = caCertEntity,
                Sans = sansString
            };
            issuedCert.ConvertEncoded(issuedCertEntity);
            issuedCertEntity = await EntityRepo.CreateAsync(issuedCertEntity, cancellationToken).ConfigureAwait(false);

            return Mapper.Map<IssuedCertificateDto>(issuedCertEntity);
        }

        /// <inheritdoc />
        public async Task<PagedList<IssuedCertificateDto>> GetPagedAsync(
            PagingParams<IssuedCertificateFilter> filter,
            CancellationToken cancellationToken)
        {
            var query = Ufw.Repository<IssuedCertificate>().Query;
            query = Search(query, filter.Filter.Search);
            query = Sorting(query, filter.Filter.OrderBy, filter.Filter.SortField);
            var list = await query.TakePaged(filter).ConfigureAwait(false);
            var resultList = Mapper.Map<List<IssuedCertificateDto>>(list);
            return resultList.ToPagedList(filter, query.Count());
        }

        /// <inheritdoc />
        public async Task RevokeAsync(int id, CancellationToken cancellationToken)
        {
            var entity = await EntityRepo.FirstOrDefaultAsync(_ => _.Id == id, cancellationToken).ConfigureAwait(false);
            if (entity == null)
                throw new EntityNotFoundException(_localizer[IssuedCertificateConstants.NotFound.ToInvariantFormat(id)]);
            entity.RevocationDate = DateTime.Now;
            await EntityRepo.UpdateAsync(entity, cancellationToken).ConfigureAwait(false);
        }

        /// <inheritdoc />
        public async Task<(IssuedCertificate issuedCertifificate, RootCertificate rootCert)> Export(
            ExportIssuedCertificateDto model,
            CancellationToken token)
        {
            IssuedCertificate certEntity = (await EntityRepo.FirstOrDefaultAsync(_ => _.Id == model.CertificateId).ConfigureAwait(false))!;
            return (issuedCertifificate: certEntity, rootCert: certEntity.RootCertificate);
        }

        /// <inheritdoc />
        public async Task<List<IssuedCertificate>> GetRevokedCertificates(int rootCertId)
        {
            return await EntityRepo
                .GetAsync(_ => _.RevocationDate.HasValue && 
                _.RootCertificateId == rootCertId)
                .ConfigureAwait(false);
        }

        #region [ private methods ]

        private X509Certificate2 CreateIssuedCertificate(
            IssuedCertificateTemplate template,
            IssuedCertificateCreateDto model,
            RSA issuedKey,
            RootCertificate caCertEntity,
            out string sansString)
        {
            var issuedReq = CreateCsr(template, model, issuedKey, out sansString);
            byte[] serialNumber = BitConverter.GetBytes(DateTime.Now.ToBinary());
            int validPeriodCa = (caCertEntity.ValidTo - DateTime.Now).Days;
            var expired = DateTimeOffset.Now.AddDays(
                    validPeriodCa > template.ValidPeriod
                        ? template.ValidPeriod
                        : validPeriodCa);
            var caCert = caCertEntity.Encoded.GetX509Cert().ImportPrivateKeyToCert(caCertEntity.PrivateKey);
            var issuedCert = issuedReq.Create(caCert, DateTimeOffset.Now, expired, serialNumber);
            return issuedCert;
        }

        private CertificateRequest CreateCsr(
            IssuedCertificateTemplate template,
            IssuedCertificateCreateDto model,
            RSA issuedKey,
            out string sansString)
        {
            var certificateSubject = template.Subject.FromString<CertificateSubject>();
            certificateSubject.CommonName = model.CommonName;
            string subject = certificateSubject.ToString();

            var sansExtension = CertificatesFactoryHelper.BuildSubjectAlternativeNamesExt(
                model.Sans,
                _securityMessageLocalizer,
                out sansString);
            var clientExtensions = new List<X509Extension>
            {
                sansExtension,
                new X509BasicConstraintsExtension(false, false, 0, false),
                new X509KeyUsageExtension(
                    X509KeyUsageFlags.DigitalSignature |
                    X509KeyUsageFlags.NonRepudiation |
                    X509KeyUsageFlags.KeyCertSign |
                    X509KeyUsageFlags.DataEncipherment, false),
            };
            if (template.ExtendedKeyUsageClientAuth)
            {
                clientExtensions.Add(new X509EnhancedKeyUsageExtension(
                new OidCollection
                {
                    new Oid(OidConstants.OidAuthClientExt),
                },
                false));
            }
            else if (template.ExtendedKeyUsageServerAuth)
            {
                clientExtensions.Add(new X509EnhancedKeyUsageExtension(
                new OidCollection
                {
                    new Oid(OidConstants.OidAuthServerExt),
                },
                false));
            }
            var issuedReq = CertificatesFactoryHelper.CreateCsr(
                subject,
                issuedKey,
                clientExtensions.ToArray());
            return issuedReq;
        }

        private IQueryable<IssuedCertificate> Search(IQueryable<IssuedCertificate> data, string search)
        {
            if (string.IsNullOrWhiteSpace(search))
                return data;
            string lowerSort = search.ToLower(CultureInfo.CurrentCulture);
#pragma warning disable CA1304 // Specify CultureInfo
            return data.Where(_ => !string.IsNullOrWhiteSpace(_.Issuer) && _.Issuer.ToLower().Contains(lowerSort) ||
                                   !string.IsNullOrWhiteSpace(_.Subject) && _.Subject.ToLower().Contains(lowerSort));
#pragma warning restore CA1304 // Specify CultureInfo
        }

        private IQueryable<IssuedCertificate> Sorting(IQueryable<IssuedCertificate> data, SortOrderBy orderBy, string sortField)
        {
            return data.OrderBy($"{sortField} {orderBy}");
        }

        #endregion [ private methods ]
    }
}
