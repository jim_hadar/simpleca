using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Localization;
using ResourceManager.Security;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Domain.Models.Certificates;
using SimpleCA.Security.Providers.Contracts;

namespace SimpleCA.Security.Providers.Implementations
{
    /// <inheritdoc cref="IIssuedCertificateTemplateProvider"/>
    public class IssuedCertificateTemplateProvider
        : ProviderBase<IssuedCertificateTemplate>, IIssuedCertificateTemplateProvider
    {
        private readonly IStringLocalizer<CertificateTemplateConstants> _localizer;
        public IssuedCertificateTemplateProvider(IUnitOfWork ufw, IMapper mapper, IStringLocalizer<CertificateTemplateConstants> localizer)
            : base(ufw, mapper)
        {
            _localizer = localizer;
        }

        /// <inheritdoc />
        public async Task<CertificateTemplateDto> CreateAsync(CertificateTemplateCreateDto model,
            CancellationToken token = default)
        {
            var entity = Mapper.Map<IssuedCertificateTemplate>(model);
            await EntityRepo.CreateAsync(entity, token).ConfigureAwait(false);
            return await GetByIdAsync(entity.Id, token).ConfigureAwait(false);
        }

        /// <inheritdoc />
        public async Task<CertificateTemplateDto> UpdateAsync(CertificateTemplateUpdateDto model,
            CancellationToken token = default)
        {
            var entity = await GetOriginByIdAsync(model.Id, token).ConfigureAwait(false);
            var mappedEntity = Mapper.Map(model, entity);
            entity = await EntityRepo.UpdateAsync(mappedEntity, token).ConfigureAwait(false);
            return await GetByIdAsync(entity.Id, token).ConfigureAwait(false);
        }

        /// <inheritdoc />
        public async Task<int> DeleteAsync(int id, CancellationToken token = default)
        {
            return await EntityRepo.DeleteAsync(_ => _.Id == id, token).ConfigureAwait(false);
        }

        /// <inheritdoc />
        public async Task<CertificateTemplateDto> GetByIdAsync(int id, CancellationToken token = default)
        {
            var entity = await GetOriginByIdAsync(id, token).ConfigureAwait(false);
            return Mapper.Map<CertificateTemplateDto>(entity);
        }

        /// <inheritdoc />
        public Task<List<CertificateTemplateDto>> GetAllAsync(CancellationToken token = default)
        {
            return EntityRepo.Query.ProjectTo<CertificateTemplateDto>(Mapper.ConfigurationProvider).ToListAsync(token);
        }

        #region [ Help methods ]

        private async Task<IssuedCertificateTemplate> GetOriginByIdAsync(int id, CancellationToken token)
        {
            var entity = await EntityRepo.FirstOrDefaultAsync(_ => _.Id == id, token).ConfigureAwait(false);
            if(entity == null)
                throw new EntityNotFoundException(_localizer[CertificateTemplateConstants.CertificateTemplateNotFound].ToInvariantFormat(id));
            return entity;
        }

        #endregion
    }
}
