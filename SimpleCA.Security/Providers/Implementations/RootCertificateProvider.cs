﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SimpleCA.Core.Contracts;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Domain.Models.Certificates;
using SimpleCA.Security.Providers.Contracts;

namespace SimpleCA.Security.Providers.Implementations
{
    /// <inheritdoc cref="IRootCertificateProvider"/>
    public class RootCertificateProvider : ProviderBase<RootCertificate>, IRootCertificateProvider
    {
        /// <inheritdoc cref="ProviderBase{RootCertificate}"/>
        public RootCertificateProvider(IUnitOfWork ufw, IMapper mapper) : base(ufw, mapper)
        {
        }

        /// <inheritdoc />
        public async Task<RootCertificate> GetActualRootCertificate(CancellationToken cancellationToken = default)
        {
            var curDate = DateTime.Now.Date;
            var entity = await EntityRepo.FirstOrDefaultAsync(_ => _.ValidFrom.Date <= curDate && curDate <= _.ValidTo, cancellationToken);
            return entity!;
        }
    }
}
