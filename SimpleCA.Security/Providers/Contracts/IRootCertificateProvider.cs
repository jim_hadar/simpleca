﻿using System.Threading;
using System.Threading.Tasks;
using SimpleCA.Core.Contracts;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Domain.Models.Certificates;

namespace SimpleCA.Security.Providers.Contracts
{
    /// <summary>
    /// Провайдер для работы с корневыми сертификатами.
    /// </summary>
    public interface IRootCertificateProvider : IProviderBase<RootCertificate>
    {
        /// <summary>
        /// Получение корневого сертификата сервера.
        /// </summary>
        /// <returns></returns>
        Task<RootCertificate> GetActualRootCertificate(CancellationToken cancellationToken = default);
    }
}
