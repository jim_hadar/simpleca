using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Exceptions;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Domain.Models.Certificates;

namespace SimpleCA.Security.Providers.Contracts
{
    /// <summary>
    /// Провайдер для работы 
    /// </summary>
    public interface IIssuedCertificateTemplateProvider : IProviderBase<IssuedCertificateTemplate>
    {
        /// <summary>
        /// Создание шаблона сертификата.
        /// </summary>
        /// <param name="model">Модель шаблона.</param>
        /// <param name="token">The token.</param>
        /// <returns>Созданный шаблон</returns>
        Task<CertificateTemplateDto> CreateAsync(CertificateTemplateCreateDto model, CancellationToken token = default);

        /// <summary>
        /// Обновление шаблона.
        /// </summary>
        /// <param name="model">Модель шаблона для обновления.</param>
        /// <param name="token"></param>
        /// <exception cref="EntityNotFoundException"></exception>
        /// <returns>Обновленный шаблон.</returns>
        Task<CertificateTemplateDto> UpdateAsync(CertificateTemplateUpdateDto model, CancellationToken token = default);

        /// <summary>
        /// Удаление шаблона по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор шаблона для удаления.</param>
        /// <param name="token"></param>
        /// <returns>Количество удаелнных записей.</returns>
        Task<int> DeleteAsync(int id, CancellationToken token = default);

        /// <summary>
        /// Получение шаблона по идентификатору.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <exception cref="EntityNotFoundException"></exception>
        /// <returns>Шаблон сертификата.</returns>
        Task<CertificateTemplateDto> GetByIdAsync(int id, CancellationToken token = default);

        /// <summary>
        /// Получение всех шаблонов на сертификаты.
        /// </summary>
        /// <param name="token"></param>
        /// <returns>Список шаблонов.</returns>
        Task<List<CertificateTemplateDto>> GetAllAsync(CancellationToken token = default);
    }
}
