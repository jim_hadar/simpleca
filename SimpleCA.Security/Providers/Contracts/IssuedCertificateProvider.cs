﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using SimpleCA.Core.Contracts;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Domain.Models.Certificates.IssuedCertificateModels;
using SimpleCA.Domain.Models.Paging;
using SimpleCA.Domain.Models.Paging.Filters;

namespace SimpleCA.Security.Providers.Contracts
{
    /// <summary>
    /// Провайдер для работы с выпущенными сертификатами.
    /// </summary>
    public interface IIssuedCertificateProvider : IProviderBase<IssuedCertificate>
    {
        /// <summary>
        /// Выпуск сертификата.
        /// </summary>
        /// <param name="model">Модель сертификата.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Идетификатор сертификата.</returns>
        Task<IssuedCertificateDto> CreateAsync(
            IssuedCertificateCreateDto model, 
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Отзыв сертификата.
        /// </summary>
        /// <param name="id">Идентификатор сертификата для отзыва.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Асинхронная операция.</returns>
        Task RevokeAsync(
            int id, 
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Получение сертификатов с постраничной разбивкой.
        /// </summary>
        /// <param name="filter">Фильтрация сертификатов.</param>
        /// <param name="cancellationToken"></param>
        /// <returns>Результат.</returns>
        Task<PagedList<IssuedCertificateDto>> GetPagedAsync(
            PagingParams<IssuedCertificateFilter> filter,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Экспорт сертификата.
        /// </summary>
        /// <param name="model">Параметры экспорта.</param>
        /// <param name="token"></param>
        /// <returns>Выпущенный сертификат и соотвествубщий корневой сертификат.</returns>
        Task<(IssuedCertificate issuedCertifificate, RootCertificate rootCert)> Export(
            ExportIssuedCertificateDto model, 
            CancellationToken token = default);

        /// <summary>
        /// Получение списка отозванных сертификатов для указанного корневого сертификата.
        /// </summary>
        /// <param name="rootCertId">Идентификатор корневого сертификата.</param>
        /// <returns>Список отозванных сертификатов.</returns>
        Task<List<IssuedCertificate>> GetRevokedCertificates(int rootCertId);

    }
}
