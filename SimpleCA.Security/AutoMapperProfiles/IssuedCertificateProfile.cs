﻿using AutoMapper;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Domain.Models.Certificates;
using SimpleCA.Domain.Models.Certificates.IssuedCertificateModels;
using SimpleCA.Security.Internal;

namespace SimpleCA.Security.AutoMapperProfiles
{
    public class IssuedCertificateProfile : Profile
    {
        public IssuedCertificateProfile()
        {
            CreateMap<IssuedCertificate, IssuedCertificateDto>()
                .ForMember(_ => _.ParsedSubject, o => o.MapFrom(_ => _.Subject.FromString<CertificateSubject>()))
                .ForAllOtherMembers(_ => _.UseDestinationValue());
        }
    }
}
