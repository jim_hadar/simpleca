﻿using AutoMapper;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Domain.Models.Certificates;
using SimpleCA.Security.Internal;

namespace SimpleCA.Security.AutoMapperProfiles
{
    public class IssuedCertificateTemplateProfile : Profile
    {
        public IssuedCertificateTemplateProfile()
        {
            CreateMap<CertificateTemplateCreateDto, IssuedCertificateTemplate>()
                .ForMember(_ => _.Id, o => o.Ignore())
                .ForMember(_ => _.Subject, o => o.MapFrom(_ => _.ToString()))
                .ForAllOtherMembers(_ => _.UseDestinationValue());
            
            CreateMap<CertificateTemplateUpdateDto, IssuedCertificateTemplate>()
                .ForMember(_ => _.Id, o => o.Ignore())
                .ForMember(_ => _.Subject, o => o.MapFrom(_ => _.ToString()))
                .ForAllOtherMembers(_ => _.UseDestinationValue());
            
            CreateMap<IssuedCertificateTemplate, CertificateTemplateDto>()
                .ForMember(_ => _.ParsedSubject, o => o.MapFrom(_ => _.Subject.FromString<CertificateSubject>()))
                .ForAllOtherMembers(_ => _.UseDestinationValue());
        }
    }
}
