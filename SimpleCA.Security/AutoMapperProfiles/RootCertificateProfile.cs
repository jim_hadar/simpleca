﻿using AutoMapper;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Domain.Models.Certificates;

namespace SimpleCA.Security.AutoMapperProfiles
{
    public class RootCertificateProfile : Profile
    {
        public RootCertificateProfile()
        {
            CreateMap<RootCertificate, RootCertificateDto>()
                .ForAllOtherMembers(_ => _.UseDestinationValue());
        }
    }
}
