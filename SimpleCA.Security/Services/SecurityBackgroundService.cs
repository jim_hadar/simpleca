﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Localization;
using ResourceManager.Security;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Extensions;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Logging.Contracts;
using SimpleCA.Security.Internal;

namespace SimpleCA.Security.Services
{
    /// <summary>
    /// Фоновый сервис для модуля Security.
    /// </summary>
    public sealed class SecurityBackgroundService : BackgroundService
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly ILogger _logger;
        private readonly IStringLocalizer<RootCertificateConstants> _localizer;

        public SecurityBackgroundService(
            IServiceProvider serviceProvider,
            ILogger logger,
            IStringLocalizer<RootCertificateConstants> localizer)
        {
            _serviceProvider = serviceProvider;
            _logger = logger;
            _localizer = localizer;
        }

        /// <inheritdoc />
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    stoppingToken.ThrowIfCancellationRequested();
                    if(!await ActualRootCertExists(stoppingToken).ConfigureAwait(false))
                    {
                        await CreateRootCertificate(stoppingToken).ConfigureAwait(false);
                    }
                    await Sleep(TimeSpan.FromDays(1), stoppingToken).ConfigureAwait(false);
                }
                catch (OperationCanceledException)
                {
                    break;
                }
                catch(Exception ex)
                {
                    _logger.Error(ex, _localizer[RootCertificateConstants.RootCertificateCheckError].ToInvariantFormat(ex.Message));
                    await Sleep(TimeSpan.FromMinutes(1), stoppingToken).ConfigureAwait(false);
                }
            }
        }

        #region [ private methods ]

        /// <summary>
        /// Поиск действительного корневого сертификата.
        /// </summary>
        /// <returns></returns>
        private async Task<bool> ActualRootCertExists(CancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            using var scope = _serviceProvider.CreateScope();
            var ufw = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
            var curDate = DateTime.Now.Date;
            return await ufw.Repository<RootCertificate>()
                .Query
                .AnyAsync(_ => _.ValidFrom.Date <= curDate && curDate <= _.ValidTo.Date, token)
                .ConfigureAwait(false);
        }

        /// <summary>
        /// Генерация нового корневого сертификата.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        private async Task CreateRootCertificate(CancellationToken token)
        {
            token.ThrowIfCancellationRequested();
            using var scope = _serviceProvider.CreateScope();
            var ufw = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
            string subjectName = CaConstants.CaSubjectName;
            using var privateKey = RsaHelper.GenerateRsaKey();
            var req = CertificatesFactoryHelper.CreateCsr(subjectName, privateKey);
            req.CertificateExtensions.Add(new X509BasicConstraintsExtension(true, false, 0, true));
            req.CertificateExtensions.Add(new X509SubjectKeyIdentifierExtension(req.PublicKey, false));
            var expirateCert = DateTimeOffset.Now.AddYears(CaConstants.ExpirateCaCertYears).AddDays(1);
            var caCert = req.CreateSelfSigned(DateTimeOffset.Now, expirateCert);
            var certEntity = new RootCertificate
            {
                Description = $"Root certificate {caCert.SerialNumber}",
                Name = $"Root certificate",
                PrivateKey = privateKey.ToBase64(),
                Encoded = caCert.ToPKCS8String(),
            };
            caCert.ConvertEncoded(certEntity);

            await ufw.Repository<RootCertificate>()
                .CreateAsync(certEntity, token)
                .ConfigureAwait(false);
            _logger.Info(_localizer[RootCertificateConstants.RootCertificateCreate].ToInvariantFormat(certEntity.Id));
        }

        /// <summary>
        /// "Засыпание" на указанное время
        /// </summary>
        /// <param name="delay"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        private async Task Sleep(TimeSpan delay, CancellationToken token)
        {
            try
            {
                await AsyncExtensions.LongDelay(delay, token).ConfigureAwait(false);
            }
            catch (OperationCanceledException)
            {
                return;
            }
        }

        #endregion [ private methods ]
    }
}
