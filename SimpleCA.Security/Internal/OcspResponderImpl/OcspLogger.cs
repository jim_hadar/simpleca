﻿using OcspResponder.Contracts;
using SimpleCA.Logging.Contracts;

namespace SimpleCA.Security.Internal.OcspResponderImpl
{
    /// <summary>
    /// Реализация логгера для Ocsp-ответчика.
    /// </summary>
    internal sealed class OcspLogger : IOcspLogger
    {
        private readonly ILogger _logger;
        private const string OcspLoggerName = "OcspLogger";
        public OcspLogger(ILoggerFactory loggerFactory)
        {
            _logger = loggerFactory.GetLogger(OcspLoggerName);
        }
        public void Debug(string message)
        {
            _logger.Debug(message);
        }

        public void Error(string message)
        {
            _logger.Error(message);
        }

        public void Warn(string message)
        {
            _logger.Warn(message);
        }
    }
}
