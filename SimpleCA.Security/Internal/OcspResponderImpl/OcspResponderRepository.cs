﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OcspResponder.Contracts;
using OcspResponder.Models;
using SimpleCA.Core.Contracts;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Security.Providers.Contracts;

namespace SimpleCA.Security.Internal.OcspResponderImpl
{
    /// <summary>
    /// Реализация репозитория для Ocsp-ответчика.
    /// </summary>
    internal sealed class OcspResponderRepository : IOcspResponderRepository
    {
        private readonly IRootCertificateProvider _rootCertificateProvider;
        private IUnitOfWork Ufw => _rootCertificateProvider.Ufw;
        private readonly List<X509Certificate2> _rootCertificates;

        public OcspResponderRepository(
            IRootCertificateProvider rootCertificateProvider)
        {
            _rootCertificateProvider = rootCertificateProvider;
            _rootCertificates = _rootCertificateProvider
                .EntityRepo
                .Query
                .Select(_ => _.Encoded.GetX509Cert().ImportPrivateKeyToCert(_.PrivateKey))
                .ToList();
        }
        /// <inheritdoc />
        public ValueTask<X509Certificate2[]> GetChain(X509Certificate2 issuerCertificate)
        {
            if (!_rootCertificates.Any(_ => _.Thumbprint == issuerCertificate.Thumbprint))
                return new ValueTask<X509Certificate2[]>(new X509Certificate2[] { });
            return new ValueTask<X509Certificate2[]>(new[]
            {
                _rootCertificates.First(_ => _.Thumbprint == issuerCertificate.Thumbprint)
            });
        }

        /// <inheritdoc />
        public async ValueTask<IEnumerable<X509Certificate2>> GetIssuerCertificates()
        {
            var rootEntity = await _rootCertificateProvider.GetActualRootCertificate().ConfigureAwait(false);
            return new List<X509Certificate2>(1){
                rootEntity.Encoded.GetX509Cert()
            };
        }

        /// <inheritdoc />
        public ValueTask<DateTimeOffset> GetNextUpdate()
        {
            return new ValueTask<DateTimeOffset>(DateTimeOffset.Now.AddHours(1));
        }

        /// <inheritdoc />
        public ValueTask<AsymmetricAlgorithm> GetResponderPrivateKey(X509Certificate2 caCertificate)
        {
            return new ValueTask<AsymmetricAlgorithm>(
                _rootCertificates.First(_ => _.Thumbprint == caCertificate.Thumbprint).PrivateKey);
        }

        /// <inheritdoc />
        public Task<CaCompromisedStatus> IsCaCompromised(X509Certificate2 caCertificate)
        {
            return Task.FromResult(new CaCompromisedStatus
            {
                IsCompromised = false,
            });
        }

        /// <inheritdoc />
        public async Task<bool> SerialExists(string serial, X509Certificate2 issuerCertificate)
        {
            if (!_rootCertificates.Exists(_ => _.Thumbprint == issuerCertificate.Thumbprint))
            {
                return false;
            }
            var rootCertEntity = await _rootCertificateProvider
                .EntityRepo
                .Query
                .FirstAsync(_ => _.SerialNumber == issuerCertificate.SerialNumber)
                .ConfigureAwait(false);

            return rootCertEntity.IssuedCertificates.Any(_ => _.SerialNumber == serial);
        }

        /// <inheritdoc />
        public async ValueTask<CertificateRevocationStatus> SerialIsRevoked(string serial, X509Certificate2 issuerCertificate)
        {
            if (!_rootCertificates.Exists(_ => _.Thumbprint == issuerCertificate.Thumbprint))
            {
                return new CertificateRevocationStatus
                {
                    IsRevoked = true,
                    RevokedInfo = new RevokedInfo
                    {
                        Date = DateTime.Now,
                        Reason = RevocationReason.Unspecified
                    }
                };
            }
            var certEntity = await Ufw.Repository<IssuedCertificate>()
                .FirstOrDefaultAsync(_ => _.SerialNumber == serial)
                .ConfigureAwait(false);

            if(certEntity == null)
                return new CertificateRevocationStatus
                {
                    IsRevoked = true,
                    RevokedInfo = new RevokedInfo
                    {
                        Date = DateTime.Now,
                        Reason = RevocationReason.Unspecified
                    }
                };

            if(certEntity.IsRevoked)
                return new CertificateRevocationStatus
                {
                    IsRevoked = true,
                    RevokedInfo = new RevokedInfo
                    {
                        Date = certEntity.RevocationDate!.Value,
                        Reason = RevocationReason.Unspecified
                    }
                };

            return new CertificateRevocationStatus
            {
                IsRevoked = false
            };
        }

        /// <inheritdoc />
        public void Dispose()
        {
            _rootCertificates.ForEach(c => c.Dispose());
            _rootCertificates.Clear();
        }

        #region [ private methods ]



        #endregion [ private methods ]
    }
}
