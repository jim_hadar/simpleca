﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using ResourceManager.Security;
using SimpleCA.Core.Exceptions;

namespace SimpleCA.Security.Internal
{
    internal static class RsaHelper
    {
        /// <summary>
        /// Импорт приватного ключа из потока <paramref name="stream"/>
        /// </summary>
        /// <param name="stream">Поток, из которого импортируется ключ.</param>
        /// <returns>Ключ rsa</returns>
        /// <exception cref="CommonException">В случае невалидности переданного ключа</exception>
        internal static RSA ImportRsaPrivateKey(this Stream stream)
        {
            try
            {
                using var streamReader = new StreamReader(stream);
                string key = streamReader.ReadToEnd();
                var keyLines = key.Trim().Split("\n");
                string keyInfo = string.Join(Environment.NewLine, keyLines[1..^1]);
                var privateKeyBytes = Convert.FromBase64String(keyInfo);
                var rsa = RSA.Create();
                if (key.Contains("BEGIN RSA PRIVATE KEY"))
                {
                    rsa.ImportRSAPrivateKey(privateKeyBytes, out _);
                }
                else if (key.Contains("BEGIN PRIVATE KEY"))
                {
                    rsa.ImportPkcs8PrivateKey(privateKeyBytes, out _);
                }
                else
                {
                    throw new SecurityException(SecurityMessageConstants.CannotImportRsaPrivateKey);
                }

                return rsa;
            }
            catch (SecurityException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new SecurityException($"{SecurityMessageConstants.CannotImportRsaPrivateKey}", ex);
            }
        }
        
        /// <summary>
        /// Импорт rsa приватного ключа из строки
        /// </summary>
        /// <param name="privateKey">rsa ключ</param>
        /// <returns></returns>
        internal static RSA ImportPrivateKeyFromString(string privateKey)
        {
            using var stream = new MemoryStream();
            using var writer = new StreamWriter(stream);
            writer.Write(privateKey);
            writer.Flush();
            stream.Position = 0;
            var rsa = stream.ImportRsaPrivateKey();
            return rsa;
        }
        
        /// <summary>
        /// Конвертация ключа RSA <seealso cref="RSA"/> в формат Base64.
        /// </summary>
        /// <param name="rsa">The RSA.</param>
        /// <returns>Строковое представление ключа.</returns>
        internal static string ToBase64(this RSA rsa)
        {
            string name = rsa.SignatureAlgorithm.ToUpper();
            var builder = new StringBuilder();
            builder.AppendLine($"-----BEGIN RSA PRIVATE KEY-----");
            builder.AppendLine(Convert.ToBase64String(rsa.ExportRSAPrivateKey(), Base64FormattingOptions.InsertLineBreaks));
            builder.AppendLine($"-----END RSA PRIVATE KEY-----");
            return builder.ToString();
        }

        /// <summary>
        /// Генерация закрытого ключа указанной длины.
        /// </summary>
        /// <param name="keyInsString">Закрытый ключ в строковм представлении в формате Pkcs8, обернутый в --BEGIN PRIVATE KEY--.</param>
        /// <param name="keySize">Размер ключа.</param>
        /// <returns></returns>
        public static RSA GenerateRsaKey(int keySize = CaConstants.PrivateKeySize)
            => new RSACryptoServiceProvider(keySize);
    }
}
