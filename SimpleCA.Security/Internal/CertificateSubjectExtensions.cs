﻿using System.Linq;
using SimpleCA.Domain.Models.Certificates;

namespace SimpleCA.Security.Internal
{
    internal static class CertificateSubjectExtensions
    {
        /// <summary>
        /// Парсинг Субъекта из строки к объект <seealso cref="CertificateSubject"/>
        /// </summary>
        /// <typeparam name="T">Тип объекта.</typeparam>
        /// <param name="subject">Субъект сертификат в виде единой строки.</param>
        /// <returns></returns>
        internal static T FromString<T>(this string subject)
            where T : CertificateSubject, new()
        {
            var result = new T();
            static string GetValue(string[] fields, string nameField)
            {
                return fields.Any(_ => _.Contains($"{nameField}=")) 
                    ? fields.First(_ => _.Contains($"{nameField}=")).Split('=')[1] 
                    : string.Empty;
            }
            var values = subject.Split(',');

            result.CommonName = GetValue(values, "CN");
            result.OrganizationUnit = GetValue(values, "OU");
            result.Organization = GetValue(values, "O");
            result.City = GetValue(values, "L");
            result.State = GetValue(values, "ST");
            result.Country = GetValue(values, "C");
            return result;
        }
    }
}
