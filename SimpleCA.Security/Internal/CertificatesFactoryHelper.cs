﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Microsoft.Extensions.Localization;
using ResourceManager.Security;
using SimpleCA.Core.Exceptions;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Domain.Models.Certificates;

namespace SimpleCA.Security.Internal
{
    /// <summary>
    /// Helpers для поддержки инфраструктуры выпуска / создания сертификатов.
    /// </summary>
    internal static class CertificatesFactoryHelper
    {
        /// <summary>
        /// Конвертация сертификата в строку формата PKCS8.
        /// </summary>
        /// <param name="certificate">Сертификат для конвертации.</param>
        /// <returns>Строковое представление сертификата в формате #PKCS8.</returns>
        internal static string ToPKCS8String(this X509Certificate2 certificate)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine("-----BEGIN CERTIFICATE-----");
            builder.AppendLine(Convert.ToBase64String(certificate.RawData, Base64FormattingOptions.InsertLineBreaks));
            builder.AppendLine("-----END CERTIFICATE-----");
            return builder.ToString();
        }

        /// <summary>
        /// Создание запроса на сертификат.
        /// </summary>
        /// <param name="subject">Субъект.</param>
        /// <param name="rsaKey">Выходной объект RSA ключа.</param>
        /// <param name="rsaKeyString">The RSA key string.</param>
        /// <param name="extensions">Расширения для запроса на сертификат.</param>
        /// <returns>Запрос на сертификат.</returns>
        internal static CertificateRequest CreateCsr(
            string subject,
            in RSA rsaKey,
            params X509Extension[] extensions)
        {
            var certificateRequest = new CertificateRequest(
                subject,
                rsaKey,
                HashAlgorithmName.SHA256,
                RSASignaturePadding.Pkcs1);
            foreach (var ext in extensions)
            {
                certificateRequest.CertificateExtensions.Add(ext);
            }

            return certificateRequest;
        }

        /// <summary>
        /// Заполнение сущности <paramref name="certificateEntity"/> данными из сертификата <paramref name="certificate"/>
        /// </summary>
        /// <param name="certificate">Сертификат</param>
        /// <param name="certificateEntity">Куда сохранять полученные из содержимого сертификата данные</param>
        /// <returns></returns>
        internal static ICertificate ConvertEncoded(this X509Certificate2 certificate, ICertificate certificateEntity)
        {
            SetValueForCertificate(certificateEntity, certificate);
            return certificateEntity;
        }

        /// <summary>
        /// Считывание сертификата из строки в кодировке Base64.
        /// </summary>
        /// <param name="certString">The cert string.</param>
        /// <returns>Се</returns>
        internal static X509Certificate2 GetX509Cert(this string certString)
        {
            var certLines = certString.Trim().Split("\n");
            certString = string.Join("\n", certLines[1..^1]);
            var bytes = Convert.FromBase64String(certString);

            var cert = new X509Certificate2(bytes);
            return cert;
        }

        /// <summary>
        /// Заполнение объекта <see cref="certificate"/> данными из сертификата <see cref="cert"/>
        /// </summary>
        /// <param name="certificate">The certificate.</param>
        /// <param name="cert">The cert.</param>
        internal static void SetValueForCertificate(this ICertificate certificate, X509Certificate2 cert)
        {
            certificate.Issuer = cert.Issuer;
            certificate.SerialNumber = cert.SerialNumber;
            certificate.SignatureAlgorithm = cert.SignatureAlgorithm.FriendlyName;
            certificate.Subject = cert.SubjectName.Name;
            certificate.ValidFrom = cert.NotBefore;
            certificate.ValidTo = cert.NotAfter;
            certificate.Version = cert.Version;
        }

        /// <summary>
        /// Создание секции Sans для запроса на сертификат.
        /// </summary>
        /// <param name="sans">Список альтернативных имен</param>
        /// <param name="localizer">Локализатор.</param>
        /// <param name="sansString">Строковое представление результата</param>
        /// <returns>Расширение для запроса на сертификат</returns>
        /// <exception cref="CommonException">В случае передачи невалидных параметров</exception>
        public static X509Extension BuildSubjectAlternativeNamesExt(
            List<SubjectAlternativeName> sans,
            IStringLocalizer<SecurityMessageConstants> localizer,
            out string sansString)
        {
            var builder = new SubjectAlternativeNameBuilder();
            foreach (var san in sans)
            {
                switch (san.Type)
                {
                    case SubjectAlternativeNameType.Uri:
                        if (!Uri.TryCreate(san.Value, UriKind.RelativeOrAbsolute, out var uri))
                        {
                            throw new SecurityException(localizer[SecurityMessageConstants.InvalidUriSubjectAlternativeName]);
                        }
                        builder.AddUri(new Uri(san.Value));
                        break;
                    case SubjectAlternativeNameType.IpAddress:
                        if (!IPAddress.TryParse(san.Value, out var ip))
                        {
                            throw new SecurityException(localizer[SecurityMessageConstants.InvalidIpSubjectAlternativeName]);
                        }
                        builder.AddIpAddress(ip);
                        break;
                    case SubjectAlternativeNameType.DnsName:
                        builder.AddDnsName(san.Value);
                        break;
                    case SubjectAlternativeNameType.DirectoryName:
                        builder.AddUserPrincipalName(san.Value);
                        break;
                    default:
                        throw new SecurityException(localizer[SecurityMessageConstants.InvalidSubjectAlternativeNameType]);
                }
            }

            var extension = builder.Build();

            sansString = extension.Format(true);

            return extension;
        }

        /// <summary>
        /// Импорт закрытого ключа из строки в сертификат.
        /// </summary>
        /// <param name="cert">Сертификат.</param>
        /// <param name="privateKey">Закрытый ключ.</param>
        public static X509Certificate2 ImportPrivateKeyToCert(this X509Certificate2 cert, string privateKey)
        {
            using var stream = new MemoryStream();
            using var writer = new StreamWriter(stream);
            writer.Write(privateKey);
            writer.Flush();
            stream.Position = 0;
            var rsa = stream.ImportRsaPrivateKey();
            return cert.CopyWithPrivateKey(rsa);
        }

    }
}
