namespace SimpleCA.Security.Internal
{
    /// <summary>
    /// Константы атрибутов.
    /// </summary>
    internal class OidConstants
    {
        /// <summary>
        /// Атрибут проверки подлинности клиента в сертификате.
        /// </summary>
        internal const string OidAuthClientExt = "1.3.6.1.5.5.7.3.2";

        /// <summary>
        /// Атрибут проверки подлинности сервера в сертификате.
        /// </summary>
        internal const string OidAuthServerExt = "1.3.6.1.5.5.7.3.1";
    }
}
