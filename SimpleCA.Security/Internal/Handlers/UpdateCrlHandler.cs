﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Localization;
using NLog.Fluent;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Operators;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.X509.Extension;
using ResourceManager.Security;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Logging.Contracts;
using SimpleCA.Security.Internal.Commands;
using SimpleCA.Security.Providers.Contracts;

namespace SimpleCA.Security.Internal.Handlers
{
    /// <summary>
    /// Обработчик команды обновления файла Crl
    /// </summary>
    internal class UpdateCrlHandler : INotificationHandler<UpdateCrlCommand>
    {
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly ILogger _logger;
        private readonly IRootCertificateProvider _rootCertificateProvider;
        private readonly IIssuedCertificateProvider _issuedCertificateProvider;
        private readonly IStringLocalizer<IssuedCertificateConstants> _localizer;
        
        private string CrlFilePath => Path.Combine(_hostEnvironment.ContentRootPath, CaConstants.CrlFileName);

        public UpdateCrlHandler(
            ILogger logger,
            IStringLocalizer<IssuedCertificateConstants> localizer,
            IWebHostEnvironment hostEnvironment,
            IRootCertificateProvider rootCertificateProvider,
            IIssuedCertificateProvider issuedCertificateProvider)
        {
            _hostEnvironment = hostEnvironment;
            _logger = logger;
            _rootCertificateProvider = rootCertificateProvider;
            _issuedCertificateProvider = issuedCertificateProvider;
            _localizer = localizer;
        }

        /// <inheritdoc />
        public async Task Handle(UpdateCrlCommand notification, CancellationToken cancellationToken)
        {
            try
            {   
                var actualRootCertEntity = await _rootCertificateProvider
                    .GetActualRootCertificate(cancellationToken)
                    .ConfigureAwait(false);

                var revokedCerts = await _issuedCertificateProvider
                    .GetRevokedCertificates(actualRootCertEntity.Id)
                    .ConfigureAwait(false);

                var crl = GenCrl(actualRootCertEntity, revokedCerts);

                await SaveCrlFile(crl).ConfigureAwait(false);

                _logger.Info(_localizer[IssuedCertificateConstants.UpdateCrlFileSuccess]);
            }
            catch (Exception ex)
            {
                _logger.Error(ex, IssuedCertificateConstants.UpdateCrlFileError);
            }
        }


        private X509Crl GenCrl(
            RootCertificate actualRootCertEntity,
            List<IssuedCertificate> revokedCerts)
        {
            X509Crl? oldCrl = null;
            if (File.Exists(CrlFilePath))
            {
                var x509CrlParser = new X509CrlParser();
                oldCrl = x509CrlParser.ReadCrl(File.ReadAllBytes(CrlFilePath));
            }

            var x509Parser = new X509CertificateParser();
            using var rootCert = actualRootCertEntity.Encoded.GetX509Cert();
            using var rootCertWithPrivateKey = rootCert.ImportPrivateKeyToCert(actualRootCertEntity.PrivateKey);
            var rootCertBc = x509Parser.ReadCertificate(rootCert.GetRawCertData());
            X509V2CrlGenerator crlGen = new X509V2CrlGenerator();
            crlGen.SetIssuerDN(rootCertBc.IssuerDN);
            crlGen.SetThisUpdate(DateTime.Now);
            crlGen.SetNextUpdate(DateTime.Now.Date.AddDays(1));

            crlGen.AddExtension(X509Extensions.AuthorityKeyIdentifier,
                false,
                new AuthorityKeyIdentifierStructure(rootCertBc));

            crlGen.AddExtension(X509Extensions.CrlNumber,
                false,
                new CrlNumber(BigInteger.One));        

            if(oldCrl != null)
            {
                crlGen.AddCrl(oldCrl);
            }

            SetRevokedCertificates(crlGen, revokedCerts, oldCrl);

            ISignatureFactory f = new Asn1SignatureFactory("SHA256withRSA",
            TransformRsaPrivateKey(rootCertWithPrivateKey.PrivateKey));             

            var crl = crlGen.Generate(f);
            return crl;
        }

        private AsymmetricKeyParameter TransformRsaPrivateKey(AsymmetricAlgorithm privateKey)
        {
            RSA? prov = privateKey as RSA;
            RSAParameters parameters = prov!.ExportParameters(true);

            return new RsaPrivateCrtKeyParameters(
                new BigInteger(1, parameters.Modulus),
                new BigInteger(1, parameters.Exponent),
                new BigInteger(1, parameters.D),
                new BigInteger(1, parameters.P),
                new BigInteger(1, parameters.Q),
                new BigInteger(1, parameters.DP),
                new BigInteger(1, parameters.DQ),
                new BigInteger(1, parameters.InverseQ));
        }

        private void SetRevokedCertificates(
            X509V2CrlGenerator generator,
            List<IssuedCertificate> revokedCertificates, 
            X509Crl? oldCrl)
        {
            bool oldCrlNull = oldCrl == null;
            var x509CertificateParser = new X509CertificateParser();
            var certs = revokedCertificates.Select(_ => _.Encoded.GetX509Cert()).ToList();
            for(int i = 0; i < revokedCertificates.Count; i++)
            {
                var cert = certs[i];
                var entity = revokedCertificates[i];
                var bcCert = x509CertificateParser.ReadCertificate(cert.GetRawCertData());
                if (oldCrlNull || !oldCrl!.IsRevoked(bcCert))
                {
                    generator.AddCrlEntry(bcCert.SerialNumber, entity.RevocationDate!.Value, CrlReason.Unspecified);
                }                
            }
        }

        private async Task SaveCrlFile(X509Crl x509Crl)
        {
            await using var fileStream = new StreamWriter(File.Open(CrlFilePath, FileMode.Create));

            PemWriter pemWriter = new PemWriter(fileStream);
            pemWriter.WriteObject(x509Crl);
            await pemWriter.Writer.FlushAsync().ConfigureAwait(false);
            pemWriter.Writer.Close();
        }
    }
}
