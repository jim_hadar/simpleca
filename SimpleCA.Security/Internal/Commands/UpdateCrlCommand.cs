﻿using MediatR;

namespace SimpleCA.Security.Internal.Commands
{
    /// <summary>
    /// Уведомление о необходимости обновления файла CRL (список отзывов сертификатов)
    /// </summary>
    internal class UpdateCrlCommand : INotification
    {
        internal static UpdateCrlCommand Instance { get; } = new UpdateCrlCommand();
    }
}
