﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleCA.Security.Internal
{
    internal class CaConstants
    {
        /// <summary>
        /// Имя субъекта для корневого сертификата.
        /// </summary>
        internal const string CaSubjectName = "Cn=Simple CA,ST=Spb,C=Russia,L=Spb";

        /// <summary>
        /// Название файла Crl на диске.
        /// </summary>
        internal const string CrlFileName = "ca.crl";

        /// <summary>
        /// Private key size
        /// </summary>
        internal const int PrivateKeySize = 4096;

        /// <summary>
        /// Срок действия корневого сертификата = 5 лет
        /// </summary>
        internal const int ExpirateCaCertYears = 5;

    }
}
