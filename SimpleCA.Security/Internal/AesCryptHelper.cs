using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SimpleCA.Security.Internal
{
    internal class AesCryptHelper
    {
        /// <summary>
        /// Шифрование данных по алгоритму AES
        /// </summary>
        /// <param name="aesKey">The key.</param>
        /// <param name="text">The text.</param>
        /// <returns></returns>
        public static async Task<string> EncryptAsync(string aesKey, string text)
        {
            Aes aes = Aes.Create();
            aes.GenerateIV();
            aes.Key = Encoding.UTF8.GetBytes(aesKey);
            byte[] encrypted;
            ICryptoTransform crypt = aes.CreateEncryptor(aes.Key, aes.IV);
            await using (MemoryStream ms = new MemoryStream())
            {
                await using (CryptoStream cs = new CryptoStream(ms, crypt, CryptoStreamMode.Write))
                {
                    await using StreamWriter sw = new StreamWriter(cs);
                    await sw.WriteAsync(text).ConfigureAwait(false);
                }
                encrypted = ms.ToArray();
            }
            //Возвращаем поток байт + крепим соль
            return Convert.ToBase64String(encrypted.Concat(aes.IV).ToArray());
		}

        /// <summary>
        /// Расшифровка данных по алгоритму AES
        /// </summary>
        /// <param name="aesKey">The aeskey.</param>
        /// <param name="cipherText">The cipher text.</param>
        /// <returns></returns>
        public static async Task<string> DecryptAsync(string aesKey, string cipherText)
        {
            var shifr = Convert.FromBase64String(cipherText);
            byte[] bytesIv = new byte[16];
            byte[] mess = new byte[shifr.Length - 16];
            
            for (int i = shifr.Length - 16, j = 0; i < shifr.Length; i++, j++)
                bytesIv[j] = shifr[i];
            
            for (int i = 0; i < shifr.Length - 16; i++)
                mess[i] = shifr[i];
            
            Aes aes = Aes.Create();
            
            aes.Key = Encoding.UTF8.GetBytes(aesKey);
            
            aes.IV = bytesIv;
            
            string text = "";
            byte[] data = mess;
            ICryptoTransform crypt = aes.CreateDecryptor(aes.Key, aes.IV);
            await using (MemoryStream ms = new MemoryStream(data))
            {
                await using CryptoStream cs = new CryptoStream(ms, crypt, CryptoStreamMode.Read);
                using StreamReader sr = new StreamReader(cs);
                text = await sr.ReadToEndAsync().ConfigureAwait(false);
            }
            return text;
        }
    }
}
