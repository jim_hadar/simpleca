﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Localization;
using ResourceManager.Security;
using SimpleCA.Core.Exceptions;
using SimpleCA.Domain.Models.Certificates.IssuedCertificateModels;
using SimpleCA.Domain.Models.Paging;
using SimpleCA.Domain.Queries.Certificates;
using SimpleCA.Logging.Contracts;
using SimpleCA.Security.Providers.Contracts;

namespace SimpleCA.Security.Handlers.Queries
{
    /// <summary>
    /// Обработчик запроса получения списка выпущенных сертификатов с постраничной разбивкой.
    /// </summary>
    public sealed class GetIssuedCertificatesHandler : IRequestHandler<GetIssuedCertificatesQuery, PagedList<IssuedCertificateDto>>
    {
        private readonly ILogger _logger;
        private readonly IStringLocalizer<IssuedCertificateConstants> _localizer;
        private readonly IIssuedCertificateProvider _provider;
        public GetIssuedCertificatesHandler(
            ILogger logger,
            IIssuedCertificateProvider provider,
            IStringLocalizer<IssuedCertificateConstants> localizer)
        {
            _localizer = localizer;
            _logger = logger;
            _provider = provider;
        }

        /// <inheritdoc />
        public async Task<PagedList<IssuedCertificateDto>> Handle(GetIssuedCertificatesQuery request, CancellationToken cancellationToken)
        {
            try
            {
                return await _provider.GetPagedAsync(request.Filter, cancellationToken).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                throw new SecurityException(_localizer[IssuedCertificateConstants.GetPagedError], ex);
            }
        }
    }
}
