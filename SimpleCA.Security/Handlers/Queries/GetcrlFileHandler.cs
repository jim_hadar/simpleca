﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Localization;
using ResourceManager.Security;
using SimpleCA.Core.Exceptions;
using SimpleCA.Domain.Queries.Certificates;
using SimpleCA.Logging.Contracts;
using SimpleCA.Security.Internal;
using SimpleCA.Security.Internal.Commands;

namespace SimpleCA.Security.Handlers.Queries
{
    /// <summary>
    /// Обработчик запроса получения файла Crl.
    /// </summary>
    public sealed class GetCrlFileHandler : IRequestHandler<GetCrlFileQuery, byte[]>
    {
        private readonly IMediator _mediator;
        private readonly ILogger _logger;
        private readonly IStringLocalizer<IssuedCertificateConstants> _localizer;
        private readonly IWebHostEnvironment _hostEnvironment;

        private string CrlFilePath => Path.Combine(_hostEnvironment.ContentRootPath, CaConstants.CrlFileName);

        public GetCrlFileHandler(
            IMediator mediator,
            ILogger logger,
            IStringLocalizer<IssuedCertificateConstants> localizer,
            IWebHostEnvironment hostEnvironment)
        {
            _mediator = mediator;
            _logger = logger;
            _localizer = localizer;
            _hostEnvironment = hostEnvironment;
        }

        /// <inheritdoc />
        public async Task<byte[]> Handle(GetCrlFileQuery request, CancellationToken cancellationToken)
        {
            try
            {
                if (!File.Exists(CrlFilePath))
                {
                    await _mediator.Publish(UpdateCrlCommand.Instance, cancellationToken).ConfigureAwait(false);
                }
                return await File.ReadAllBytesAsync(CrlFilePath).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                throw new SecurityException(_localizer[IssuedCertificateConstants.GetCrlFileError], ex);
            }
        }
    }
}
