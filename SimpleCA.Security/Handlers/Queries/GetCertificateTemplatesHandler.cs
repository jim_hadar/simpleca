﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Localization;
using ResourceManager.Security;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Domain.Models.Certificates;
using SimpleCA.Domain.Queries.Certificates;
using SimpleCA.Security.Providers.Contracts;

namespace SimpleCA.Security.Handlers.Queries
{
    /// <summary>
    /// Обработчик запроса получения шаблона сертификата по его идентификатору.
    /// </summary>
    public class GetCertificateTemplatesHandler : IRequestHandler<GetCertificateTemplatesQuery, List<CertificateTemplateDto>>
    {
        private readonly IIssuedCertificateTemplateProvider _provider;
        private readonly IStringLocalizer<CertificateTemplateConstants> _localizer;

        public GetCertificateTemplatesHandler(
            IIssuedCertificateTemplateProvider provider,
            IStringLocalizer<CertificateTemplateConstants> localizer)
        {
            _provider = provider;
            _localizer = localizer;
        }

        /// <inheritdoc />
        public async Task<List<CertificateTemplateDto>> Handle(GetCertificateTemplatesQuery request,
            CancellationToken cancellationToken)
        {
            try
            {
                return await _provider.GetAllAsync(cancellationToken).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                throw new SecurityException(
                    _localizer[CertificateTemplateConstants.GetAllError].ToInvariantFormat(ex.Message), ex);
            }
        }
    }
}
