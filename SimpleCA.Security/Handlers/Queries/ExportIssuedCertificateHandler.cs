﻿using System;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Localization;
using ResourceManager.Security;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Core.Helpers;
using SimpleCA.Domain.Models.Certificates.IssuedCertificateModels;
using SimpleCA.Domain.Queries.Certificates;
using SimpleCA.Security.Internal;
using SimpleCA.Security.Providers.Contracts;

namespace SimpleCA.Security.Handlers.Queries
{
    /// <summary>
    /// Обработчик команды экспорта выпущенного сертификата.
    /// </summary>
    public sealed class ExportIssuedCertificateHandler : IRequestHandler<ExportIssuedCertificateQuery, byte[]>
    {
        private readonly IIssuedCertificateProvider _provider;
        private readonly IWebHostEnvironment _hostEnvironment;
        private readonly IStringLocalizer<IssuedCertificateConstants> _localizer;
        public ExportIssuedCertificateHandler(
            IIssuedCertificateProvider provider,
            IWebHostEnvironment hostEnvironment,
            IStringLocalizer<IssuedCertificateConstants> localizer)
        {
            _provider = provider;
            _hostEnvironment = hostEnvironment;
            _localizer = localizer;
        }

        /// <inheritdoc />
        public async Task<byte[]> Handle(ExportIssuedCertificateQuery request, CancellationToken cancellationToken)
        {
            var tempDirectory = Path.Combine(_hostEnvironment.ContentRootPath, "temp");
            var tempArchivePath = Path.Combine(tempDirectory, Guid.NewGuid().ToString());
            var model = request.Model;
            try
            {
                var (certEntity, rootCertEntity) = await _provider.Export(request.Model, cancellationToken).ConfigureAwait(false);
                var format = model.Format;

                using var rootCert = certEntity.RootCertificate.Encoded.GetX509Cert();

                if (!Directory.Exists(tempDirectory))
                    Directory.CreateDirectory(tempDirectory);

                Directory.CreateDirectory(tempArchivePath);

                await File.WriteAllTextAsync(Path.Combine(tempArchivePath, "ca.pem"),
                        certEntity.RootCertificate.Encoded);

                if (format == IssuedCertificateOutputFormat.CertInPem_KeyInPKCS8)
                {
                    await File.WriteAllTextAsync(Path.Combine(tempArchivePath, "client.pem"), certEntity.Encoded);
                    await File.WriteAllTextAsync(Path.Combine(tempArchivePath, "client.key"),
                        certEntity.PrivateKey);
                }
                else
                {
                    using var cert = certEntity.Encoded.GetX509Cert();
                    using var certWithPrivateKey = cert.ImportPrivateKeyToCert(certEntity.PrivateKey);
                    await File.WriteAllBytesAsync(Path.Combine(tempArchivePath, "client.p12"),
                        certWithPrivateKey.Export(X509ContentType.Pkcs12));
                }

                var archiveFileName = Path.Combine(tempDirectory, certEntity.SerialNumber + ".zip");

                ArchivatorHelper.CreateZipArchiveForDir(archiveFileName, tempArchivePath);

                var bytesFile = await File.ReadAllBytesAsync(archiveFileName);

                File.Delete(archiveFileName);

                return bytesFile;
            }
            catch (Exception ex)
            {
                throw new SecurityException(_localizer[IssuedCertificateConstants.ExportCertificateError].ToInvariantFormat(ex.Message), ex);
            }
            finally
            {
                DelDir(tempArchivePath);
            }
        }

        void DelDir(string dirPath)
        {
            try
            {
                if (Directory.Exists(dirPath))
                    Directory.Delete(dirPath, true);
            }
            catch
            {
                //
            }
        }
    }
}
