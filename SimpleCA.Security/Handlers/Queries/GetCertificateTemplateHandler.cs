﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Localization;
using ResourceManager.Security;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Domain.Models.Certificates;
using SimpleCA.Domain.Queries.Certificates;
using SimpleCA.Security.Providers.Contracts;

namespace SimpleCA.Security.Handlers.Queries
{
    /// <summary>
    /// Обработчик запроса получения шаблона сертификата по его идентификатору.
    /// </summary>
    public class GetCertificateTemplateHandler : IRequestHandler<GetCertificateTemplateQuery, CertificateTemplateDto>
    {
        private readonly IIssuedCertificateTemplateProvider _provider;
        private readonly IStringLocalizer<CertificateTemplateConstants> _localizer;

        public GetCertificateTemplateHandler(
            IIssuedCertificateTemplateProvider provider,
            IStringLocalizer<CertificateTemplateConstants> localizer)
        {
            _provider = provider;
            _localizer = localizer;
        }

        /// <inheritdoc />
        public async Task<CertificateTemplateDto> Handle(GetCertificateTemplateQuery request,
            CancellationToken cancellationToken)
        {
            try
            {
                return await _provider.GetByIdAsync(request.Id, cancellationToken).ConfigureAwait(false);
            }
            catch (EntityNotFoundException)
            {
                throw;
            }
            catch (Exception ex)
            {
                throw new SecurityException(
                    _localizer[CertificateTemplateConstants.GetError].ToInvariantFormat(request.Id, ex.Message), ex);
            }
        }
    }
}
