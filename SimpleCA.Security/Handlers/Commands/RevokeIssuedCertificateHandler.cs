﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Localization;
using Org.BouncyCastle.Ocsp;
using ResourceManager.Security;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Domain.Commands.Certificates;
using SimpleCA.Logging.Contracts;
using SimpleCA.Security.Internal.Commands;
using SimpleCA.Security.Providers.Contracts;

namespace SimpleCA.Security.Handlers.Commands
{
    /// <summary>
    /// Обработчик команды отзыва выпущенного сертификата.
    /// </summary>
    public sealed class RevokeIssuedCertificateHandler : IRequestHandler<RevokeIssuedCertififcateCommand, Unit>
    {
        private readonly IIssuedCertificateProvider _provider;
        private readonly ILogger _logger;
        private readonly IStringLocalizer<IssuedCertificateConstants> _localizer;
        private readonly IMediator _mediator;
        public RevokeIssuedCertificateHandler(
            IIssuedCertificateProvider provider,
            ILogger logger,
            IStringLocalizer<IssuedCertificateConstants> localizer,
            IMediator mediator)
        {
            _provider = provider;
            _logger = logger;
            _localizer = localizer;
            _mediator = mediator;
        }

        /// <inheritdoc />
        public async Task<Unit> Handle(RevokeIssuedCertififcateCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var certEntity = await _provider.EntityRepo
                    .FirstOrDefaultAsync(_ => _.Id == request.CertificateId, cancellationToken)
                    .ConfigureAwait(false);
                if (certEntity == null || certEntity.IsRevoked)
                    return Unit.Value;

                await _provider.RevokeAsync(request.CertificateId, cancellationToken).ConfigureAwait(false);

                _logger.Info(_localizer[IssuedCertificateConstants.CertificateRevokedSuccess].ToInvariantFormat(certEntity.Subject));

                await _mediator.Publish(UpdateCrlCommand.Instance, cancellationToken).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                throw new SecurityException(_localizer[IssuedCertificateConstants.CertificateRevokeError], ex);
            }
            return Unit.Value;
        }
    }
}
