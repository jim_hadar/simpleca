﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Localization;
using ResourceManager.Security;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Domain.Commands.Certificates;
using SimpleCA.Logging.Contracts;
using SimpleCA.Security.Providers.Contracts;

namespace SimpleCA.Security.Handlers.Commands
{
    /// <summary>
    /// Обработчик команды удаления шаблона на сертификат.
    /// </summary>
    public class DeleteCertificateTemplateHandler : IRequestHandler<DeleteCertificateTemplateCommand, Unit>
    {
        private readonly IIssuedCertificateTemplateProvider _provider;
        private readonly ILogger _logger;
        private readonly IStringLocalizer<CertificateTemplateConstants> _localizer;

        public DeleteCertificateTemplateHandler(
            IIssuedCertificateTemplateProvider provider,
            ILogger logger,
            IStringLocalizer<CertificateTemplateConstants> localizer)
        {
            _provider = provider;
            _logger = logger;
            _localizer = localizer;
        }

        /// <inheritdoc />        
        public async Task<Unit> Handle(DeleteCertificateTemplateCommand request, CancellationToken cancellationToken)
        {
            try
            {
                var entity = await _provider.EntityRepo.FirstOrDefaultAsync(_ => _.Id == request.Id, cancellationToken)
                    .ConfigureAwait(false);
                if (entity != null)
                {
                    _provider.Ufw.BeginTransaction();
                    var dto = await _provider.DeleteAsync(request.Id, cancellationToken).ConfigureAwait(false);
                    _logger.Info(_localizer[CertificateTemplateConstants.DeleteSuccess].ToInvariantFormat(entity.Name));
                    _provider.Ufw.CommitTransaction();
                }

                return Unit.Value;
            }
            catch (Exception ex)
            {
                _provider.Ufw.RollbackTransaction();
                throw new SecurityException(
                    _localizer[CertificateTemplateConstants.DeleteError].ToInvariantFormat(ex.Message), ex);
            }
        }
    }
}
