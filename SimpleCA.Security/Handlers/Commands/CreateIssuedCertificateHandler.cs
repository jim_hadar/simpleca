﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Localization;
using Org.BouncyCastle.Ocsp;
using ResourceManager.Security;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Domain.Commands.Certificates;
using SimpleCA.Logging.Contracts;
using SimpleCA.Security.Providers.Contracts;

namespace SimpleCA.Security.Handlers.Commands
{
    /// <summary>
    /// Обработчик команды выпуска сертификата.
    /// </summary>
    public sealed class CreateIssuedCertificateHandler : IRequestHandler<CreateIssuedCertificateCommand, int>
    {
        private readonly IStringLocalizer<IssuedCertificateConstants> _localizer;
        private readonly IMediator _mediator;
        private readonly IIssuedCertificateProvider _provider;
        private readonly ILogger _logger;
        public CreateIssuedCertificateHandler(
            ILogger logger,
            IMediator mediator,
            IIssuedCertificateProvider provider,
            IStringLocalizer<IssuedCertificateConstants> localizer)
        {
            _localizer = localizer;
            _logger = logger;
            _provider = provider;
            _mediator = mediator;
        }
        /// <inheritdoc />
        public async Task<int> Handle(CreateIssuedCertificateCommand request, CancellationToken cancellationToken)
        {
            try
            {
                _provider.Ufw.BeginTransaction();
                var result = await _provider.CreateAsync(request.Model, cancellationToken).ConfigureAwait(false);
                _logger.Info(_localizer[IssuedCertificateConstants.CreateSuccess].ToInvariantFormat(result.Subject));
                _provider.Ufw.CommitTransaction();
                return result.Id;
            }
            catch (Exception ex)
            {
                _provider.Ufw.RollbackTransaction();
                var message = _localizer[IssuedCertificateConstants.CreateError].ToInvariantFormat(ex.Message);
                throw new SecurityException(message, ex);
            }
        }
    }
}
