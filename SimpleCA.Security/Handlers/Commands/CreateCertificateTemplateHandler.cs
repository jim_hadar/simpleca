﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Localization;
using ResourceManager.Security;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Domain.Commands.Certificates;
using SimpleCA.Logging.Contracts;
using SimpleCA.Security.Providers.Contracts;

namespace SimpleCA.Security.Handlers.Commands
{
    /// <summary>
    /// Обработчик команды создания шаблона на сертификат.
    /// </summary>
    public class CreateCertificateTemplateHandler : IRequestHandler<CreateCertificateTemplateCommand, int>
    {
        private readonly IIssuedCertificateTemplateProvider _provider;
        private readonly ILogger _logger;
        private readonly IStringLocalizer<CertificateTemplateConstants> _localizer;

        public CreateCertificateTemplateHandler(
            IIssuedCertificateTemplateProvider provider,
            ILogger logger,
            IStringLocalizer<CertificateTemplateConstants> localizer)
        {
            _provider = provider;
            _logger = logger;
            _localizer = localizer;
        }

        /// <inheritdoc />        
        public async Task<int> Handle(CreateCertificateTemplateCommand request, CancellationToken cancellationToken)
        {
            try
            {
                _provider.Ufw.BeginTransaction();
                var dto = await _provider.CreateAsync(request.Model, cancellationToken).ConfigureAwait(false);
                _logger.Info(_localizer[CertificateTemplateConstants.CreateSuccess].ToInvariantFormat(dto.Name));
                _provider.Ufw.CommitTransaction();
                return dto.Id;
            }
            catch (Exception ex)
            {
                _provider.Ufw.RollbackTransaction();
                throw new SecurityException(
                    _localizer[CertificateTemplateConstants.CreateError].ToInvariantFormat(ex.Message), ex);
            }
        }
    }
}
