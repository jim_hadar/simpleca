﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.Extensions.Localization;
using ResourceManager.Security;
using SimpleCA.Core.Contracts;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Domain.Commands.Certificates;
using SimpleCA.Logging.Contracts;
using SimpleCA.Security.Providers.Contracts;

namespace SimpleCA.Security.Handlers.Commands
{
    /// <summary>
    /// Обработчик команды обновления шаблона на сертификат.
    /// </summary>
    public class UpdateCertificateTemplateHandler : IRequestHandler<UpdateCertificateTemplateCommand, Unit>
    {
        private readonly IIssuedCertificateTemplateProvider _provider;
        private readonly ILogger _logger;
        private readonly IStringLocalizer<CertificateTemplateConstants> _localizer;

        public UpdateCertificateTemplateHandler(
            IIssuedCertificateTemplateProvider provider,
            ILogger logger,
            IStringLocalizer<CertificateTemplateConstants> localizer)
        {
            _provider = provider;
            _logger = logger;
            _localizer = localizer;
        }

        /// <inheritdoc />        
        public async Task<Unit> Handle(UpdateCertificateTemplateCommand request, CancellationToken cancellationToken)
        {
            try
            {
                _provider.Ufw.BeginTransaction();
                var dto = await _provider.UpdateAsync(request.Model, cancellationToken).ConfigureAwait(false);
                _logger.Info(_localizer[CertificateTemplateConstants.UpdateSuccess].ToInvariantFormat(dto.Name));
                _provider.Ufw.CommitTransaction();
                return Unit.Value;
            }
            catch (Exception ex)
            {
                _provider.Ufw.RollbackTransaction();
                throw new SecurityException(
                    _localizer[CertificateTemplateConstants.UpdateError].ToInvariantFormat(ex.Message), ex);
            }
        }
    }
}
