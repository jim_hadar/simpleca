﻿using Microsoft.Extensions.DependencyInjection;
using OcspResponder.Contracts;
using SimpleCA.Security.Internal.OcspResponderImpl;
using SimpleCA.Security.Providers.Contracts;
using SimpleCA.Security.Providers.Implementations;
using SimpleCA.Security.Services;

namespace SimpleCA.Security.Code
{
    public static class Ioc
    {
        public static void ConfigureSecurity(this IServiceCollection services)
        {
            services.AddScoped<IIssuedCertificateTemplateProvider, IssuedCertificateTemplateProvider>();
            services.AddScoped<IRootCertificateProvider, RootCertificateProvider>();
            services.AddScoped<IIssuedCertificateProvider, IssuedCertificateProvider>();
            services.AddHostedService<SecurityBackgroundService>();

            services.AddSingleton<IOcspLogger, OcspLogger>();
            services.AddScoped<IOcspResponderRepository, OcspResponderRepository>();
            services.AddScoped<IOcspResponder, OcspResponder.Implementations.OcspResponder>();
        }
        
    }
}
