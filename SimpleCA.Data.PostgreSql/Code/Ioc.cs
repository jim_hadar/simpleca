using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SimpleCA.Core.Contracts;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Data.PostgreSql.Contexts;
using SimpleCA.Data.PostgreSql.Implementations;

namespace SimpleCA.Data.PostgreSql.Code
{
    public static class Ioc
    {
        public static void ConfigureData(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<SimpleCaContext>(options =>
            {
                string connString = configuration.GetConnectionString("DefaultConnection");
                options.UseLazyLoadingProxies().UseNpgsql(connString);
            });

            services.AddIdentity<User, Role>(options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequiredLength = 1;
                })
                .AddEntityFrameworkStores<SimpleCaContext>()
                .AddUserManager<UserManager<User>>()
                .AddDefaultTokenProviders();

            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.InitializeData().Wait();
        }

    }
}
