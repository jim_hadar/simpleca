using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SimpleCA.Core.Contracts;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Data.PostgreSql.Contexts;
using SimpleCA.Domain.Models.Enums;

namespace SimpleCA.Data.PostgreSql.Code
{
    public static class DbInitializer
    {
        public static async Task MigrateAsync(IServiceProvider serviceProvider)
        {
            try
            {
                var context = serviceProvider.GetRequiredService<SimpleCaContext>();
                var migrations = (await context.Database.GetPendingMigrationsAsync().ConfigureAwait(false))
                    .ToList();
                if (migrations.Any())
                {
                    await context.Database.MigrateAsync().ConfigureAwait(false);
                }
                Console.WriteLine($"Db deployed successfull");
            }
#pragma warning disable CA1031 // Do not catch general exception types
            catch (Exception ex)
#pragma warning restore CA1031 // Do not catch general exception types
            {
                Console.WriteLine($"Initialize migrations error: {ex.Message}");
            }
        }
        public static async Task InitializeData(this IServiceCollection serviceCollection)
        {
            try
            {
                var sp = serviceCollection.BuildServiceProvider();
                using var scope = sp.CreateScope();

                await MigrateAsync(scope.ServiceProvider).ConfigureAwait(false);

                var ufw = scope.ServiceProvider.GetRequiredService<IUnitOfWork>();
                var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<Role>>();
                var userManager = scope.ServiceProvider.GetRequiredService<UserManager<User>>();
                var roles = roleManager.Roles.ToList();

                if (!roles.Any())
                {
                    foreach (var roleId in Enum.GetValues(typeof(UserRole)))
                    {
                        var role = new Role
                        {
#pragma warning disable CS8605 // Unboxing a possibly null value.
                            Id = (int) roleId,
#pragma warning restore CS8605 // Unboxing a possibly null value.
                            Name = Enum.GetName(typeof(UserRole), roleId),
                        };
                        var result = await roleManager.CreateAsync(role).ConfigureAwait(false);
                    }
                }

                if (!ufw.Repository<User>().Query.Any())
                {
                    var user = new User
                    {
                        UserName = "administrator",
                    };
                    var result = await userManager.CreateAsync(user, "Qwerty7").ConfigureAwait(false);
                    if (result == IdentityResult.Success)
                    {
                        user.Roles.Add(new UserToRoleLink
                        {
                            RoleId = (int) UserRole.FullAccess,
                            UserId = user.Id
                        });
                        await userManager.UpdateAsync(user).ConfigureAwait(false);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
            }
        }
    }
}
