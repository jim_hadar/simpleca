﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SimpleCA.Data.PostgreSql.Migrations
{
    public partial class SansFieldForIssuedCert : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "sans",
                table: "issued_certificates",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "sans",
                table: "issued_certificates");
        }
    }
}
