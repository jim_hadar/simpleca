﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace SimpleCA.Data.PostgreSql.Migrations
{
    public partial class AddedСertificateEntities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "issued_certificate_templates",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(nullable: false),
                    subject = table.Column<string>(nullable: false),
                    valid_period = table.Column<int>(nullable: false),
                    extended_key_usage_client_auth = table.Column<bool>(nullable: false),
                    extended_key_usage_server_auth = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_issued_certificate_templates", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "root_certificates",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(nullable: false),
                    description = table.Column<string>(nullable: true),
                    encoded = table.Column<string>(nullable: false),
                    private_key = table.Column<string>(nullable: false),
                    version = table.Column<int>(nullable: false),
                    subject = table.Column<string>(nullable: false),
                    issuer = table.Column<string>(nullable: false),
                    valid_from = table.Column<DateTime>(nullable: false),
                    valid_to = table.Column<DateTime>(nullable: false),
                    serial_number = table.Column<string>(nullable: false),
                    signature_algorithm = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_root_certificates", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "issued_certificates",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    encoded = table.Column<string>(nullable: false),
                    private_key = table.Column<string>(nullable: false),
                    version = table.Column<int>(nullable: false),
                    subject = table.Column<string>(nullable: false),
                    issuer = table.Column<string>(nullable: false),
                    valid_from = table.Column<DateTime>(nullable: false),
                    valid_to = table.Column<DateTime>(nullable: false),
                    serial_number = table.Column<string>(nullable: false),
                    signature_algorithm = table.Column<string>(nullable: false),
                    root_certificate_id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_issued_certificates", x => x.id);
                    table.ForeignKey(
                        name: "issued_cert_root_cert_root_cert_id",
                        column: x => x.root_certificate_id,
                        principalTable: "root_certificates",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "ix_issued_certificates_root_certificate_id",
                table: "issued_certificates",
                column: "root_certificate_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "issued_certificate_templates");

            migrationBuilder.DropTable(
                name: "issued_certificates");

            migrationBuilder.DropTable(
                name: "root_certificates");
        }
    }
}
