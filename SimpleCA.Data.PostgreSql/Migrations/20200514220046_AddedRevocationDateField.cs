﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SimpleCA.Data.PostgreSql.Migrations
{
    public partial class AddedRevocationDateField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "revocation_date",
                table: "issued_certificates",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "revocation_date",
                table: "issued_certificates");
        }
    }
}
