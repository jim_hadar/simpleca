using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Data.PostgreSql.Contexts;

namespace SimpleCA.Data.PostgreSql.EntityConfigurations.Identity
{
    /// <summary>
    /// Entity configuration for <see cref="User"/>
    /// </summary>
    /// <seealso cref="IEntityTypeConfiguration{TEntity}" />
    class UserEntityTypeConfiguration : 
        IEntityTypeConfiguration<User>
    {
        /// <inheritdoc />
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.Property(_ => _.Id)
                .ValueGeneratedOnAdd();

            builder.Ignore(_ => _.DomainEvents);

            builder.ToTable("users", SimpleCaContext.PublicSchemeName);
        }
    }
}
