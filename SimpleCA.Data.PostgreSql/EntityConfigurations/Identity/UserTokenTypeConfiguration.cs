using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SimpleCA.Data.PostgreSql.Contexts;

namespace SimpleCA.Data.PostgreSql.EntityConfigurations.Identity
{
    /// <summary>
    /// Entity configuration for <see cref="IdentityUserToken{TKey}"/>
    /// </summary>
    /// <seealso cref="IEntityTypeConfiguration{TEntity}" />
    class UserTokenTypeConfiguration
        : IEntityTypeConfiguration<IdentityUserToken<int>>
    {
        /// <inheritdoc />
        public void Configure(EntityTypeBuilder<IdentityUserToken<int>> builder)
        {
            builder.ToTable("user_tokens", SimpleCaContext.PublicSchemeName);
        }
    }
}
