using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Data.PostgreSql.Contexts;

namespace SimpleCA.Data.PostgreSql.EntityConfigurations.Identity
{
    /// <summary>
    /// Entity configuration for <see cref="Role"/>
    /// </summary>
    /// <seealso cref="IEntityTypeConfiguration{TEntity}" />
    class RoleEntityTypeConfiguration
        : IEntityTypeConfiguration<Role>
    {

        /// <inheritdoc />
        public void Configure(EntityTypeBuilder<Role> builder)
        {
            builder.Property(_ => _.Id).ValueGeneratedOnAdd();
            builder.ToTable("roles", SimpleCaContext.PublicSchemeName);
        }
    }
}
