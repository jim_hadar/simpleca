using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Data.PostgreSql.Contexts;

namespace SimpleCA.Data.PostgreSql.EntityConfigurations.Identity
{
    /// <summary>
    /// Entity configuration for <see cref="UserToRoleLink"/>
    /// </summary>
    /// <seealso cref="IEntityTypeConfiguration{TEntity}" />
    class UserToRoleLinkEntityTypeConfiguration
        : IEntityTypeConfiguration<UserToRoleLink>
    {

        /// <inheritdoc />
        public void Configure(EntityTypeBuilder<UserToRoleLink> builder)
        {
            builder.HasKey(_ => new { _.UserId, _.RoleId });
            builder.HasOne(_ => _.User)
                .WithMany(_ => _.Roles)
                .HasForeignKey(_ => _.UserId);

            builder.HasOne(_ => _.Role)
                .WithMany(_ => _.Users)
                .HasForeignKey(_ => _.RoleId);

            builder.ToTable("user_to_role_links", SimpleCaContext.PublicSchemeName);
        }
    }
}
