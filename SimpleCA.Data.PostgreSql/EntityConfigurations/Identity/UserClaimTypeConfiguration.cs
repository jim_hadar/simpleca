using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SimpleCA.Data.PostgreSql.Contexts;

namespace SimpleCA.Data.PostgreSql.EntityConfigurations.Identity
{
    // <summary>
    /// Entity configuration for <see cref="IdentityUserClaim{TKey}"/>
    /// </summary>
    /// <seealso cref="IEntityTypeConfiguration{TEntity}" />
    class UserClaimTypeConfiguration
        : IEntityTypeConfiguration<IdentityUserClaim<int>>
    {
        /// <inheritdoc />
        public void Configure(EntityTypeBuilder<IdentityUserClaim<int>> builder)
        {
            builder.ToTable("user_claims", SimpleCaContext.PublicSchemeName);
        }
    }
}
