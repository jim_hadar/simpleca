using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SimpleCA.Data.PostgreSql.Contexts;

namespace SimpleCA.Data.PostgreSql.EntityConfigurations.Identity
{
    /// <summary>
    /// Entity configuration for <see cref="IdentityUserLogin{TKey}"/>
    /// </summary>
    /// <seealso cref="IEntityTypeConfiguration{TEntity}" />
    class UserLoginEntityTypeConfiguration
        : IEntityTypeConfiguration<IdentityUserLogin<int>>
    {
        /// <inheritdoc />
        public void Configure(EntityTypeBuilder<IdentityUserLogin<int>> builder)
        {
            builder.ToTable("user_logins", SimpleCaContext.PublicSchemeName);
        }
    }
}
