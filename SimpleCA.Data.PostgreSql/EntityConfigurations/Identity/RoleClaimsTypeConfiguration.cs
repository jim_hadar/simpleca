using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SimpleCA.Data.PostgreSql.Contexts;

namespace SimpleCA.Data.PostgreSql.EntityConfigurations.Identity
{
    /// <summary>
    /// Entity configuration for <see cref="IdentityRoleClaim{TKey}"/>
    /// </summary>
    /// <seealso cref="IEntityTypeConfiguration{TEntity}" />
    class RoleClaimsTypeConfiguration
        : IEntityTypeConfiguration<IdentityRoleClaim<int>>
    {
        /// <inheritdoc />
        public void Configure(EntityTypeBuilder<IdentityRoleClaim<int>> builder)
        {
            builder.ToTable("role_claims", SimpleCaContext.PublicSchemeName);
        }
    }
}
