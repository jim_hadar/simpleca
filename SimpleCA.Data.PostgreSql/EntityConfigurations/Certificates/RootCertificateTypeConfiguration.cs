using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SimpleCA.Data.Models.Certificates;

namespace SimpleCA.Data.PostgreSql.EntityConfigurations.Certificates
{
    class RootCertificateTypeConfiguration
        : IEntityTypeConfiguration<RootCertificate>
    {
        public void Configure(EntityTypeBuilder<RootCertificate> builder)
        {
            builder.HasKey(_ => _.Id);
            builder.HasMany(_ => _.IssuedCertificates)
                .WithOne(_ => _.RootCertificate)
                .HasForeignKey(_ => _.RootCertificateId)
                .HasConstraintName("issued_cert_root_cert_root_cert_id")
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
