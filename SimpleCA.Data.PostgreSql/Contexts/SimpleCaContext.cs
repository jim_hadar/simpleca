using System;
using System.Reflection;
using MediatR;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Npgsql.NameTranslation;
using SimpleCA.Core.Extensions;
using SimpleCA.Data.Models;
using SimpleCA.Data.Models.Certificates;
using SimpleCA.Data.Models.Identity;

namespace SimpleCA.Data.PostgreSql.Contexts
{
    public class SimpleCaContext : IdentityDbContext<User, Role, int,
        IdentityUserClaim<int>,
        UserToRoleLink, IdentityUserLogin<int>,
        IdentityRoleClaim<int>,
        IdentityUserToken<int>>
    {
        private readonly IConfiguration _config;

        /// <summary>
        /// Название схемы для Microsoft Identity Store
        /// </summary>
        protected internal const string IdentitySchemeName = "asp_identity";

        /// <summary>
        /// Название схемы для других таблиц БД
        /// </summary>
        protected internal const string PublicSchemeName = "public";

        /// <inheritdoc />
#pragma warning disable CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
        public SimpleCaContext(DbContextOptions<SimpleCaContext> options, IConfiguration config)
#pragma warning restore CS8618 // Non-nullable field is uninitialized. Consider declaring as nullable.
            : base(options)
        {
            _config = config;
        }

        /// <inheritdoc />
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    //.UseNpgsql(_config.GetConnectionString("DefaultConnection"))
                    .UseNpgsql("Server=localhost; Port=5432; Database=simple_ca; User Id=postgres; Password=sql;")
                    .UseLazyLoadingProxies();
            }

            optionsBuilder.EnableSensitiveDataLogging();
        }

        /// <inheritdoc />
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            // TODO: только для PostgreSQL 9.6 и ниже
            //modelBuilder.UseSerialColumns();

            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetAssembly(typeof(SimpleCaContext)));

            EntitiesToSnackcase(modelBuilder);
        }

        #region [ DbSets ]

        /// <summary>
        /// Логи по Voltron.
        /// </summary>
        public DbSet<LogEntity> Logs { get; set; }
        
        /// <summary>
        /// Выпущенные сертификаты.
        /// </summary>
        public DbSet<IssuedCertificate> IssuedCertificates { get; set; }
        
        /// <summary>
        /// Корневые сертификаты.
        /// </summary>
        public DbSet<RootCertificate> RootCertificates { get; set; }
        
        /// <summary>
        /// Шаблоны сертификатов для выпуска.
        /// </summary>
        public DbSet<IssuedCertificateTemplate> IssuedCertificateTemplates { get; set; }

        #endregion

        #region [ Help methods ]

        /// <summary>
        /// Преобразование всех сущностей БД к ноатции snackcase.
        /// </summary>
        /// <param name="builder">The builder.</param>
        private void EntitiesToSnackcase(ModelBuilder builder)
        {
            var mapper = new NpgsqlSnakeCaseNameTranslator();
            foreach (var entity in builder.Model.GetEntityTypes())
            {
                // Replace table names
                entity.SetTableName(entity.GetTableName().ToSnakeCase());

                // Replace column names            
                foreach (var property in entity.GetProperties())
                {
                    property.SetColumnName(property.GetColumnName().ToSnakeCase());
                }

                foreach (var key in entity.GetKeys())
                {
                    key.SetName(key.GetName().ToSnakeCase());
                }

                foreach (var key in entity.GetForeignKeys())
                {
                    key.SetConstraintName(key.GetConstraintName().ToSnakeCase());
                }

                foreach (var index in entity.GetIndexes())
                {
                    index.SetName(index.GetName().ToSnakeCase());
                }
            }
        }

        #endregion
    }
}
