﻿using Microsoft.Extensions.DependencyInjection;
using SimpleCA.Logging.Contracts;
using SimpleCA.Logging.Implementations;

namespace SimpleCA.Logging.Code
{
    /// <summary>
    /// Initialize IServiceCollection
    /// </summary>
    public static class Ioc
    {
        /// <summary>
        /// Конфигурирование логгера для DI
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureLogger(this IServiceCollection services)
        {
            services.AddSingleton<ILoggerFactory, NlogLoggerFactory>();
            services.AddSingleton<ILogger>((s) =>
            {
                var factory = s.GetService<ILoggerFactory>();
                return factory.DefaultLogger();
            });
        }
    }
}
