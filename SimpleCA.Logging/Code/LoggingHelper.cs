using NLog;

namespace SimpleCA.Logging.Code
{
    /// <summary>
    /// Helper for logger
    /// </summary>
    public static class LoggingHelper
    {
        /// <summary>
        /// Initializes the logger.
        /// </summary>
        /// <param name="dbConnection">The database connection.</param>
        /// <param name="logBaseDir">The log base dir.</param>
        public static void InitLogger(string dbConnection, string logBaseDir = "")
        {
            if (!string.IsNullOrWhiteSpace(dbConnection))
            {
                GlobalDiagnosticsContext.Set("DefaultConnection", dbConnection);
            }

            if (!string.IsNullOrWhiteSpace(logBaseDir))
            {
                LogManager.Configuration.Variables["basedir"] = logBaseDir;
            }

            LogManager.AutoShutdown = true;
        }

        /// <summary>
        /// Shuts down this instance.
        /// </summary>
        public static void Shutdown()
        {
            if(LogManager.Configuration != null)
                LogManager.Shutdown();
        }

    }
}