using System;
using System.Net.Sockets;
using Microsoft.AspNetCore.Http;
using NLog;
using Npgsql;
using SimpleCA.Logging.Models;
using ILogger = SimpleCA.Logging.Contracts.ILogger;

namespace SimpleCA.Logging.Implementations
{
    /// <summary>
    /// Implementation of <see cref="Contracts.ILogger"/>
    /// </summary>
    /// <seealso cref="Contracts.ILogger" />
    public sealed class NLogLogger : ILogger
    {
        /// <summary>
        /// NLog logger.
        /// </summary>
        private readonly Logger _log;

        /// <summary>
        /// The accessor
        /// </summary>
        private readonly IHttpContextAccessor? _accessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="NLogLogger"/> class.
        /// </summary>
        /// <param name="loggerName">Name of the logger.</param>
        /// <param name="accessor">The accessor.</param>
        public NLogLogger(string loggerName, IHttpContextAccessor? accessor)
        {
            _accessor = accessor;
            _log = LogManager.GetLogger(loggerName);
        }

        /// <inheritdoc />
        public void Debug(string message)
        {
            Log(LogLevel.Debug, null, message);
        }

        /// <inheritdoc />
        public void Debug(string format, params object[] args)
        {
            Log(LogLevel.Debug, null, format, args);
        }

        /// <inheritdoc />
        public void Debug(Exception exception, string format, params object[] args)
        {
            Log(LogLevel.Debug, exception, format, args);
        }
        
        /// <inheritdoc />
        public void Info(string format)
        {
            Log(LogLevel.Info, null, format);
        }

        /// <inheritdoc />
        public void Info(string format, params object[] args)
        {
            Log(LogLevel.Info, null, format, args);
        }

        /// <inheritdoc />
        public void Trace(string message)
        {
            Log(LogLevel.Trace, null, message);
        }

        /// <inheritdoc />
        public void Trace(string format, params object[] args)
        {
            Log(LogLevel.Trace, null, format, args);
        }

        /// <inheritdoc />
        public void Trace(Exception exception, string format, params object[] args)
        {
            Log(LogLevel.Trace, exception, format, args);
        }

        /// <inheritdoc />
        public void Warn(string message)
        {
            Log(LogLevel.Warn, null, message);
        }

        /// <inheritdoc />
        public void Warn(string format, params object[] args)
        {
            Log(LogLevel.Warn, null, format, args);
        }

        /// <inheritdoc />
        public void Warn(Exception exception, string format, params object[] args)
        {
            Log(LogLevel.Warn, exception, format, args);
        }

        /// <inheritdoc />
        public void Error(string message)
        {
            Log(LogLevel.Error, null, message);
        }

        /// <inheritdoc />
        public void Error(string format, params object[] args)
        {
            Log(LogLevel.Error, null, format, args);
        }

        /// <inheritdoc />
        public void Error(Exception exception, string format, params object[] args)
        {
            Log(LogLevel.Error, exception, format, args);
        }

        #region [ Help methods ]

        private void Log(
            LogLevel level,
            Exception? exception,
            string format,
            params object[] args)
        {
            try
            {
                var props = new AdditionalEventProperties(_accessor?.HttpContext?.User?.Identity?.Name);
                _log.WithProperty("additional", props)
                    .Log(level, exception, format, args);
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine($"Logging argument exception error: {ex.Message}");
            }
            catch (PostgresException ex)
            {
                Console.WriteLine($"Logging database exception error: {ex.Message}");
            }
            catch (SocketException ex)
            {
                Console.WriteLine($"Logging socket exception error: {ex.Message}");
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Logging unhandled error: {ex.Message}");
            }
        }

        #endregion
    }
}