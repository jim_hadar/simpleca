using Microsoft.AspNetCore.Http;
using SimpleCA.Logging.Contracts;

namespace SimpleCA.Logging.Implementations
{
    /// <inheritdoc />
    public sealed class NlogLoggerFactory : ILoggerFactory
    {
        private readonly IHttpContextAccessor _accessor;

        /// <summary>
        /// Initializes a new instance of the <see cref="NlogLoggerFactory"/> class.
        /// </summary>
        /// <param name="accessor">The accessor.</param>
        public NlogLoggerFactory(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        /// <inheritdoc />
        public ILogger GetLogger(string loggerName) => new NLogLogger(loggerName, _accessor);
    }
}