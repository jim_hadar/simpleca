using System;

namespace SimpleCA.Logging.Contracts
{
    /// <summary>
    /// ILogger
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Naming", "CA1716:Identifiers should not match keywords", Justification = "<Pending>")]
    public interface ILogger
    {
        #region [ logger contants ]

        public const string DefaultLoggerName = "simple_ca";

        #endregion
        
        /// <summary>
        /// Логгер уровня Debug.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        void Debug(string message);

        /// <summary>
        /// Логгер уровня debug с интерполяцией строк.
        /// </summary>
        /// <param name="format">Шаблон.</param>
        /// <param name="args">Аргументы для шаблона.</param>
        void Debug(string format, params object[] args);

        /// <summary>
        /// Логгер уровня debug с передачей исключения
        /// </summary>
        /// <param name="exception">Исключение.</param>
        /// <param name="format">Шаблон.</param>
        /// <param name="args">Аргументы для шаблона.</param>
        void Debug(Exception exception, string format, params object[] args);

        /// <summary>
        /// Логгер уровня info.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        void Info(string message);

        /// <summary>
        /// Логгер уровня Info.
        /// </summary>
        /// <param name="format">Шаблон сообщения.</param>
        /// <param name="args">Аргументы для шаблона.</param>
        void Info(string format, params object[] args);

        /// <summary>
        /// Логгер уровня trace.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        void Trace(string message);

        /// <summary>
        /// Логгер уровня trace
        /// </summary>
        /// <param name="format">Шаблон сообщения.</param>
        /// <param name="args">Аргументы шаблона.</param>
        void Trace(string format, params object[] args);

        /// <summary>
        /// Логгер уровня trace
        /// </summary>
        /// <param name="exception">Исключение.</param>
        /// <param name="format">Шаблон сообщения.</param>
        /// <param name="args">Аргументы шаблона.</param>
        void Trace(Exception exception, string format, params object[] args);

        /// <summary>
        /// Шаблон уровня Warning.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        void Warn(string message);

        /// <summary>
        /// Логгер уровня Warning.
        /// </summary>
        /// <param name="format">Шаблон сообщения.</param>
        /// <param name="args">Аргументы шаблона.</param>
        void Warn(string format, params object[] args);

        /// <summary>
        /// Логгер уровня Warning.
        /// </summary>
        /// <param name="exception">Исключение.</param>
        /// <param name="format">Шаблона сообщения.</param>
        /// <param name="args">Аргументы шаблона.</param>
        void Warn(Exception exception, string format, params object[] args);

        /// <summary>
        /// Логгер уровня Error.
        /// </summary>
        /// <param name="message">Сообщение.</param>
        void Error(string message);

        /// <summary>
        /// Сообщение уровня Error.
        /// </summary>
        /// <param name="format">Шаблон сообщения.</param>
        /// <param name="args">Аргументы шаблона.</param>
        void Error(string format, params object[] args);


        /// <summary>
        /// Шаблон уровня Error 
        /// </summary>
        /// <param name="exception">Исключение.</param>
        /// <param name="format">Шаблон сообщения.</param>
        /// <param name="args">Аргументы шаблона.</param>
        void Error(Exception exception, string format, params object[] args);
    }
}