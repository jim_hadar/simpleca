namespace SimpleCA.Logging.Contracts
{
    /// <summary>
    /// The logger factory.
    /// </summary>
    public interface ILoggerFactory
    {
        /// <summary>
        /// Возвращает логгер с указанным именем.
        /// </summary>
        /// <param name="loggerName">Название логгера.</param>
        /// <returns></returns>
        ILogger GetLogger(string loggerName);

        /// <summary>
        /// Логгер по умолчанию 
        /// </summary>
        /// <returns></returns>
        ILogger DefaultLogger() => GetLogger(ILogger.DefaultLoggerName);
        
    }
}