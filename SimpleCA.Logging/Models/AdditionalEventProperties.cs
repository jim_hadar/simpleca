namespace SimpleCA.Logging.Models
{
    internal sealed class AdditionalEventProperties
    {
        public AdditionalEventProperties(string? user)
        {
            User = user ?? "";
        }
        /// <summary>
        /// Имя пользователя для логгирования.
        /// </summary>
        public string User { get; }

    }
}