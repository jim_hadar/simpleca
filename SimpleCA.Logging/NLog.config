<?xml version="1.0" encoding="utf-8" ?>
<nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.nlog-project.org/schemas/NLog.xsd NLog.xsd"
      autoReload="true"
      throwExceptions="true">

    <variable name="basedir" value="${basedir}/logs" />

    <variable name="ca" value="simple_ca" />

    <add assembly="NLog.Web.AspNetCore"/>
    <add assembly="NLog.Appsettings.Standard"/>

    <targets>
        <target xsi:type="AsyncWrapper" 
                timeToSleepBetweenBatches="0" 
                optimizeBufferReuse="true"
                overflowAction="Grow"
                name="ca">
            <target
                    xsi:type="File"
                    layout="[${time}] ${message}${onexception:${newline}EXCEPTION\:${exception:format=ToString,StackTrace}${newline}}"
                    createDirs="true"
                    fileName="${basedir}/${shortdate}/${ca}.txt"
                    archiveFileName="${basedir}/logs/${shortdate}/${ca}.{#####}.txt"
                    archiveAboveSize="8388608"
                    archiveNumbering="Rolling"
                    maxArchiveFiles="86400"
                    concurrentWrites="true"
                    keepFileOpen="false"
                    encoding="utf-8"
            />
        </target>
        <target xsi:type="Database"
                name="database"
                dbProvider="Npgsql.NpgsqlConnection, Npgsql"
                connectionString="${gdc:item=DefaultConnection}"
                commandText="INSERT INTO logs(date, exception, level, logger, message, stacktrace, username, thread) VALUES (CAST(@Date AS timestamptz), @Exception, @Level, @Logger, @Message, @Stacktrace, @User, @Thread);">
            />
            <parameter name="@Date" layout="${longdate}" />
            <parameter name="@Exception" layout="${exception}" />
            <parameter name="@Level" layout="${level}" />
            <parameter name="@Logger" layout="${logger}" />
            <parameter name="@Message" layout="${message}" />
            <parameter name="@Stacktrace" layout="${exception:format=ToString,StackTrace}" />
            <parameter name="@User" layout="${event-properties:additional:objectpath=User}"/>
            <parameter name="@Thread" layout="${threadid}" />
            />
        </target>
    </targets>

    <rules>
        <logger name="*" minlevel="Info" writeTo="database" />
        <logger name="ca" appendTo="ca" minlevel="Trace" />
    </rules>
</nlog>