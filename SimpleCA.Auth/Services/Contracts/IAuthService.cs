using System.Threading.Tasks;
using SimpleCA.Domain.Models.Users;

namespace SimpleCA.Auth.Services.Contracts
{
    /// <summary>
    /// Сервис авторизации
    /// </summary>
    public interface IAuthService
    {
        /// <summary>
        /// Авторизация Jwt пользователя.
        /// </summary>
        /// <param name="credentials">Данные пользователя.</param>
        /// <exception cref="VoltronException">В случае неудачной попытки авторизации.</exception>
        /// <returns>Данные пользователя.</returns>
        Task<JwtAuthSuccessResponse> LoginJwt(CredentialsModel credentials);

        /// <summary>
        /// Разлогинивание пользователя
        /// </summary>
        /// <returns>Асинхронная операция.</returns>
        Task Logout();
    }
}
