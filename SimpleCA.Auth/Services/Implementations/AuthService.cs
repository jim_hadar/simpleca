﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Options;
using ResourceManager.Auth;
using SimpleCA.Auth.Code;
using SimpleCA.Auth.Models;
using SimpleCA.Auth.Services.Contracts;
using SimpleCA.Core.Exceptions;
using SimpleCA.Core.Extensions;
using SimpleCA.Data.Models.Identity;
using SimpleCA.Domain.Models.Users;
using SimpleCA.Logging.Contracts;

namespace SimpleCA.Auth.Services.Implementations
{
    /// <inheritdoc />
    public sealed class AuthService : IAuthService
    {
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly JwtIssuerOptions _jwtOptions;
        private readonly ILogger _logger;
        private readonly IStringLocalizer<AuthConstants> _localizer;

        public AuthService(UserManager<User> userManager,
                            SignInManager<User> signInManager,
                            RoleManager<Role> roleManager,
                            IOptionsSnapshot<JwtIssuerOptions> jwtOptions,
                            ILoggerFactory loggerFactory,
                            IStringLocalizer<AuthConstants> localizer)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _jwtOptions = jwtOptions.Value;
            _logger = loggerFactory.GetLogger(ILogger.DefaultLoggerName);
            _localizer = localizer;
        }

        /// <inheritdoc />
        public async Task<JwtAuthSuccessResponse> LoginJwt(CredentialsModel credentials)
        {
            try
            {
                var user = await CheckUser(credentials).ConfigureAwait(false);
#pragma warning disable CS8604 // Possible null reference argument.
                var identity = await GetClaimsIdentity(credentials.UserName).ConfigureAwait(false);
#pragma warning restore CS8604 // Possible null reference argument.
                var result = await GetFullJwtToken(user.UserName, identity).ConfigureAwait(false);
                _logger.Info(_localizer[AuthConstants.LoggedSuccess].ToInvariantFormat(user.UserName));
                return result;
            }
            catch (CommonException)
            {
                throw;
            }
            catch (Exception ex)
            {
                _logger.Error(ex, ex.Message);
                throw;
            }
        }

        /// <inheritdoc />
        public async Task Logout()
        {
            await _signInManager.SignOutAsync().ConfigureAwait(false);
        }

        #region [ Help methods ]

        /// <summary>
        /// Generates JWT token.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <param name="identity">The identity.</param>
        /// <returns></returns>
        private async Task<JwtAuthSuccessResponse> GetFullJwtToken(string userName, ClaimsIdentity identity)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userName),
                new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator().ConfigureAwait(false)),
                new Claim(JwtRegisteredClaimNames.Iat,
                    _jwtOptions.IssuedAt.DatetimeToUnixTimeStamp().ToString(CultureInfo.CurrentCulture),
                    ClaimValueTypes.Integer64),
                identity.FindFirst(ClaimTypes.Role),
                identity.FindFirst(ClaimTypes.Name),
            };

            var jwt = new JwtSecurityToken(
            issuer: _jwtOptions.Issuer,
            audience: _jwtOptions.Audience,
            claims: claims,
            notBefore: _jwtOptions.NotBefore,
            expires: _jwtOptions.Expiration,
            signingCredentials: _jwtOptions.SigningCredentials);

            var authToken = new JwtSecurityTokenHandler().WriteToken(jwt);

            var response = new JwtAuthSuccessResponse
            {
                UserId = Convert.ToInt32(identity.Claims.Single(c => c.Type == ClaimTypes.NameIdentifier).Value,
                    CultureInfo.CurrentCulture),
                AuthToken = authToken,
                ExpiresIn = (int)_jwtOptions.ValidFor.TotalSeconds,
                UserName = userName
            };

            return response;
        }

        /// <summary>
        /// Получение claim'ов от роли пользователя
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        private async Task<ClaimsIdentity> GetClaimsIdentity(string userName)
        {
            ClaimsIdentity identity = new ClaimsIdentity(new GenericIdentity(userName, "Token"));

            var user = await _userManager.FindByNameAsync(userName).ConfigureAwait(false);
            var roles = await _userManager.GetRolesAsync(user).ConfigureAwait(false);
            List<Claim> foundClaims = new List<Claim>();
            foreach (string role in roles)
            {
                var r = await _roleManager.FindByNameAsync(role).ConfigureAwait(false);
                foundClaims.AddRange(await _roleManager.GetClaimsAsync(r).ConfigureAwait(false));
            }
            identity.AddClaims(foundClaims);
            identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id.ToString(CultureInfo.CurrentCulture)));
            identity.AddClaim(new Claim(ClaimTypes.Role, roles.FirstOrDefault()));
            return identity;
        }

        /// <summary>
        /// Checks the user.
        /// </summary>
        /// <param name="credentials">The credentials.</param>
        /// <returns></returns>
        /// <exception cref="Voltron.Domain.Exceptions.AuthorizationErrorException"></exception>
        private async Task<User> CheckUser(CredentialsModel credentials)
        {
#pragma warning disable CS8604 // Possible null reference argument.
            var user = await GetUser(credentials.UserName).ConfigureAwait(false);
#pragma warning restore CS8604 // Possible null reference argument.
            if (user == null)
                throw new AuthorizationErrorException(_localizer[AuthConstants.InvalidLoginOrPass].ToInvariantFormat());
#pragma warning disable CS8604 // Possible null reference argument.
            await CheckPasswordAsync(user, credentials.Password).ConfigureAwait(false);
#pragma warning restore CS8604 // Possible null reference argument.
            return user;
        }

        /// <summary>
        /// Gets the user.
        /// </summary>
        /// <param name="userName">Name of the user.</param>
        /// <returns></returns>
        private async Task<User?> GetUser(string userName)
            => await _userManager
                .FindByNameAsync(userName)
                .ConfigureAwait(false);

        /// <summary>
        /// Checks the password asynchronous.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="password">The password.</param>
        /// <exception cref="Voltron.Domain.Exceptions.AuthorizationErrorException">
        /// </exception>
        private async Task CheckPasswordAsync(User user, string password)
        {
            var checkPass = await _signInManager
                .PasswordSignInAsync(user, password, false, false)
                .ConfigureAwait(false);

            if (!checkPass.Succeeded)
            {
                throw new AuthorizationErrorException(_localizer[AuthConstants.InvalidLoginOrPass].ToInvariantFormat());
            }
        }

        #endregion
    }
}
