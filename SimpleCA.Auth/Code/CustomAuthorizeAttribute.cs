using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SimpleCA.Domain.Models.Enums;

namespace SimpleCA.Auth.Code
{
    /// <summary>
    /// Кастомный атрибут авторизации
    /// </summary>
    public class CustomAuthorizeAttribute : AuthorizeAttribute, IAsyncAuthorizationFilter
    {
        private readonly List<UserRole> _allowedRoles;

        public CustomAuthorizeAttribute(params UserRole[] roles)
        {
            _allowedRoles = !roles.Any()
                ? new List<UserRole>{UserRole.Readonly, UserRole.FullAccess}
                : roles.ToList();
        }

        /// <inheritdoc />
        public async Task OnAuthorizationAsync(AuthorizationFilterContext context)
        {
            try
            {
                await ValidateUser(context).ConfigureAwait(false);
            }
            catch (Exception)
            {
                context.Result = new BadRequestResult();
            }
        }

        private async Task ValidateUser(AuthorizationFilterContext context)
        {
            var curUser = context.HttpContext.User;
            if (!curUser.Identity.IsAuthenticated)
            {
                return;
            }

            if (!curUser.HasClaim(_ => _.Type == ClaimTypes.Role))
            {
                context.Result = new ForbidResult();
                await Logout(context).ConfigureAwait(false);
                return;
            }

            if (!CheckUserRole(curUser))
            {
                context.Result = new ForbidResult();
            }
        }

        private bool CheckUserRole(ClaimsPrincipal identity)
        {
            var curUserRole = (UserRole)Enum.Parse(typeof(UserRole), identity.Claims.First(_ => _.Type == ClaimTypes.Role).Value);
            return _allowedRoles.Contains(curUserRole);
        }
        
        private async Task Logout(AuthorizationFilterContext context)
        {
            await context.HttpContext.SignOutAsync().ConfigureAwait(false);
            context.Result = new ContentResult() { StatusCode = StatusCodes.Status401Unauthorized, Content = "Ошибка авторизации" };
        }
    }
}
