namespace SimpleCA.Auth.Models
{
    /// <summary>
    /// Параметры авторизации из AppSettings.json
    /// </summary>
    public sealed class AppSettingsAuthConfig
    {
        /// <summary>
        /// Gets or sets the issuer.
        /// </summary>
        public string Issuer { get; set; } = string.Empty;

        /// <summary>
        /// Gets or sets the audience.
        /// </summary>
        public string Audience { get; set; } = string.Empty;

        /// <summary>
        /// The authentication secret key.
        /// </summary>
        public string JwtSecretKey { get; set; } = string.Empty;
    }
}
