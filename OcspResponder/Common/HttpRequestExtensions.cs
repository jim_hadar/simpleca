using System;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using OcspResponder.Models;

namespace OcspResponder.Common
{
    /// <summary>
    /// Методы расширения для <see cref="HttpRequestMessage"/>
    /// </summary>
    public static class HttpRequestExtensions
    {
        /// <summary>
        /// Преобразование <see cref="HttpRequestMessage"/> к <see cref="OcspHttpRequest"/>
        /// </summary>
        /// <param name="requestMessage"><see cref="HttpRequestMessage"/></param>
        /// <returns><see cref="OcspHttpRequest"/></returns>
        public static async Task<OcspHttpRequest> ToOcspHttpRequest(this HttpRequestMessage requestMessage)
        {
            var httpRequestBase = new OcspHttpRequest
            {
                HttpMethod = requestMessage.Method.Method,
                MediaType = requestMessage.Content.Headers.ContentType.MediaType,
                RequestUri = requestMessage.RequestUri,
                Content = await requestMessage.Content.ReadAsByteArrayAsync().ConfigureAwait(false)
            };

            return httpRequestBase;
        }

        /// <summary>
        /// Преобразование <see cref="HttpRequest"/> к <see cref="OcspHttpRequest"/>
        /// </summary>
        /// <param name="request"><see cref="HttpRequest"/></param>
        /// <returns><see cref="OcspHttpRequest"/></returns>
        public static async Task<OcspHttpRequest> ToOcspHttpRequest(this HttpRequest request)
        {
            var ocspHttpRequest = new OcspHttpRequest();
            ocspHttpRequest.HttpMethod = request.Method;
            ocspHttpRequest.MediaType = request.ContentType;
            ocspHttpRequest.RequestUri = request.GetUri();
            ocspHttpRequest.Content = await request.GetRawBodyBytesAsync().ConfigureAwait(false);

            return ocspHttpRequest;
        }

        private const string UnknownHostName = "UNKNOWN-HOST";

        /// <summary>
        /// Gets http request Uri from request object
        /// </summary>
        /// <param name="request">The <see cref="HttpRequest"/></param>
        /// <returns>A New Uri object representing request Uri</returns>
        private static Uri GetUri(this HttpRequest request)
        {
            if (null == request)
            {
                throw new ArgumentNullException(nameof(request));
            }

            if (true == string.IsNullOrWhiteSpace(request.Scheme))
            {
                throw new ArgumentException("Http request Scheme is not specified");
            }

            string hostName = request.Host.HasValue ? request.Host.ToString() : UnknownHostName;

            var builder = new StringBuilder();

            builder.Append(request.Scheme)
                .Append("://")
                .Append(hostName);

            if (true == request.Path.HasValue)
            {
                builder.Append(request.Path.Value);
            }

            if (true == request.QueryString.HasValue)
            {
                builder.Append(request.QueryString);
            }

            return new Uri(builder.ToString());
        }

        /// <summary>
        /// Retrieves the raw body as a byte array from the Request.Body stream
        /// </summary>
        /// <param name="request">The <see cref="HttpRequest"/></param>
        /// <returns></returns>
        private static async Task<byte[]> GetRawBodyBytesAsync(this HttpRequest request)
        {
            if (request.Method == "POST")
            {
                await using var ms = new MemoryStream(2048);
                await request.Body.CopyToAsync(ms).ConfigureAwait(false);
                return ms.ToArray();
            }
            else
            {
                return Convert.FromBase64String(request.RouteValues.Values.Last().ToString()!);
            }
        }
    }
}
