using System.Net.Http;
using OcspResponder.Models;

namespace OcspResponder.Common
{
    /// <summary>
    /// Методы расширения для <see cref="OcspHttpResponse"/>
    /// </summary>
    public static class HttpResponseExtensions
    {
        /// <summary>
        /// Преобразование <see cref="OcspHttpResponse"/> к <see cref="HttpResponseMessage"/>
        /// </summary>
        /// <param name="ocspHttpResponse"><see cref="OcspHttpResponse"/></param>
        /// <returns><see cref="HttpResponseMessage"/></returns>
        public static HttpResponseMessage ToHttpResponseMessage(this OcspHttpResponse ocspHttpResponse)
        {
            var httpResponseMessage = new HttpResponseMessage(ocspHttpResponse.Status)
            {
                Content = new ByteArrayContent(ocspHttpResponse.Content)
            };

            httpResponseMessage.Content.Headers.ContentType.MediaType = ocspHttpResponse.MediaType;
            return httpResponseMessage;
        }
    }
}
