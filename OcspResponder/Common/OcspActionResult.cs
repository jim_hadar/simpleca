using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using OcspResponder.Models;

namespace OcspResponder.Common
{
    /// <summary>
    /// An Ocsp response
    /// </summary>
    public class OcspActionResult : IActionResult
    {
        /// <inheritdoc />
        public async Task ExecuteResultAsync(ActionContext context)
        {
            var contentResult = new FileContentResult(OcspHttpResponse.Content, OcspHttpResponse.MediaType);
            await contentResult.ExecuteResultAsync(context).ConfigureAwait(false);
        }

        /// <summary>
        /// <see cref="OcspHttpResponse"/> 
        /// </summary>
        private OcspHttpResponse OcspHttpResponse { get; }

        /// <summary>
        /// Создание  <see cref="IActionResult"/> для ответа OCSP.
        /// </summary>
        /// <param name="ocspHttpResponse"><see cref="OcspHttpResponse"/></param>
        public OcspActionResult(OcspHttpResponse ocspHttpResponse)
        {
            OcspHttpResponse = ocspHttpResponse;
        }
    }
}
