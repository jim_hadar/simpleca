﻿namespace OcspResponder.Localization
{
    /// <summary>
    /// 
    /// </summary>
    public class LocalizationConstants
    {
        public const string OcspRequestMediaTypeRequired =
            "OCSP requests requires 'application/ocsp-request' media's type on header";

        public const string OcspReqErrorResult = "Error when creating OcspReq from the request. Exception: {0}";
    }
}
