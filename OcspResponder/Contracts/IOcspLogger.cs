namespace OcspResponder.Contracts
{
    /// <summary>
    /// Реализация логгера для OCSP-ответчика
    /// </summary>
    public interface IOcspLogger
    {
        /// <summary>
        /// Сообщение уровня Debug
        /// </summary>
        /// <param name="message">Текст сообщения.</param>
        void Debug(string message);

        /// <summary>
        /// Сообщение уровня Warning
        /// </summary>
        /// <param name="message">Текст сообщения.</param>
        void Warn(string message);

        /// <summary>
        /// Сообщение уровня Error.
        /// </summary>
        /// <param name="message">Текст сообщения.</param>
        void Error(string message);
    }
}
