﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using OcspResponder.Models;

namespace OcspResponder.Contracts
{
    /// <summary>
    /// Интерфейс, который исползует OCSP Responder для проверки сертификата в CA
    /// </summary>
    public interface IOcspResponderRepository : IDisposable
    {
        /// <summary>
        /// Проверка существования сертификата с указанным серийным номером.
        /// </summary>
        /// <param name="serial">Серийный номер сертификата.</param>
        /// <param name="issuerCertificate">Сертификат для проверки.</param>
        /// <returns><c>true</c> Если сертификат существует; иначе, false</returns>
        Task<bool> SerialExists(string serial, X509Certificate2 issuerCertificate);

        /// <summary>
        /// Проверка того, что переданный сертификат отозван в CA
        /// </summary>
        /// <param name="serial">Серийный номер сертификата.</param>
        /// <param name="issuerCertificate">Сертификат.</param>
        /// <returns>A <see cref="CertificateRevocationStatus"/>Информация об отзыве сертификата.</returns>
        ValueTask<CertificateRevocationStatus> SerialIsRevoked(string serial, X509Certificate2 issuerCertificate);

        /// <summary>
        /// Проверка, что корневой CA скомпроментирован.
        /// </summary>
        /// <param name="caCertificate">Корневой сертификат для проверки.</param>
        /// <returns>A <see cref="CaCompromisedStatus"/>Информация о том, отозван ли сертификат.</returns>
        Task<CaCompromisedStatus> IsCaCompromised(X509Certificate2 caCertificate);

        /// <summary>
        /// Получение закрытого ключа CA.
        /// </summary>
        /// <param name="caCertificate">CA сертфикат.</param>
        /// <returns>A <see cref="AsymmetricAlgorithm"/> that represents the private key of the CA</returns>
        ValueTask<AsymmetricAlgorithm> GetResponderPrivateKey(X509Certificate2 caCertificate);

        /// <summary>
        /// Цепочка сертификатов, связанная с ответчиком.
        /// </summary>
        /// <param name="issuerCertificate"></param>
        /// <returns>An array of <see cref="X509Certificate2"/></returns>
        ValueTask<X509Certificate2[]> GetChain(X509Certificate2 issuerCertificate);

        /// <summary>
        /// Получает дату, когда клиент должен в следующий раз запросить информацию о статусе сертификата.
        /// </summary>
        /// <returns>A <see cref="DateTime"/>Дата следующего запроса информации о статусе сертификата клиентом.</returns>
        ValueTask<DateTimeOffset> GetNextUpdate();

        /// <summary>
        ///  Gets the issuer certificate that this repository is responsible to evaluate
        /// </summary>
        /// <returns>A <see cref="X509Certificate2"/> that represents the issuer's certificate</returns>
        ValueTask<IEnumerable<X509Certificate2>> GetIssuerCertificates();
    }
}
