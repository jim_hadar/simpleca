using System.Threading.Tasks;
using OcspResponder.Models;

namespace OcspResponder.Contracts
{
    /// <summary>
    /// Реализация OCSP-ответчика.
    /// </summary>
    public interface IOcspResponder
    {
        /// <summary>
        /// Результат обработки запроса статуса сертфиката.
        /// </summary>
        /// <param name="httpRequest">Запрос HTTP.</param>
        /// <returns>Результат запроса статуса отзыва сертификата.</returns>
        Task<OcspHttpResponse> Respond(OcspHttpRequest httpRequest);
    }
}
