using System.Net.Http;
using Org.BouncyCastle.Ocsp;
using Org.BouncyCastle.X509;

namespace OcspResponder.Internal
{
    /// <summary>
    /// Результат извлечения запроса на статус отзыва сертификата <see cref="OcspReq"/> из <see cref="HttpRequestMessage"/>
    /// </summary>
    internal class OcspReqResult
    {
        /// <summary>
        /// Статус запроса статус из перечисления <see cref="OcspRespStatus"/>
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Объект отзыва сертфиката <see cref="OcspReq"/>, извлеченный из <see cref="HttpRequestMessage"/>
        /// </summary>
        public OcspReq OcspRequest { get; set; } = null!;

        /// <summary>
        /// Описание ошибки, если не удалось извлечь объект из тела запроса.
        /// </summary>
        public string Error { get; set; } = string.Empty;

        /// <summary>
        /// Recognized issued certificate
        /// </summary>
        public X509Certificate IssuerCertificate { get; set; } = null!;
    }
}
