﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using OcspResponder.Contracts;
using OcspResponder.Models;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;
using System.Text;
using X509Certificate = Org.BouncyCastle.X509.X509Certificate;

namespace OcspResponder.Internal
{
    /// <inheritdoc />
    internal class BcOcspResponderRepositoryAdapter : IBcOcspResponderRepository
    {
        /// <see cref="OcspResponderRepository"/>
        private IOcspResponderRepository OcspResponderRepository { get; }

        internal BcOcspResponderRepositoryAdapter(IOcspResponderRepository ocspResponderRepository)
        {
            OcspResponderRepository = ocspResponderRepository;
        }

        /// <inheritdoc />
        public Task<bool> SerialExists(BigInteger serial, X509Certificate issuerCertificate)
        {
            using var dotNetCertificate = new X509Certificate2(issuerCertificate.GetEncoded());
            return OcspResponderRepository.SerialExists(GetHexSerialNumber(serial), dotNetCertificate);
        }

        /// <inheritdoc />
        public async Task<CertificateRevocationStatus> SerialIsRevoked(BigInteger serial, X509Certificate issuerCertificate)
        {
            using var dotNetCertificate = new X509Certificate2(issuerCertificate.GetEncoded());
            return await OcspResponderRepository.SerialIsRevoked(GetHexSerialNumber(serial), dotNetCertificate);
        }

        /// <param name="caCertificate"></param>
        /// <inheritdoc />
        public Task<CaCompromisedStatus> IsCaCompromised(X509Certificate caCertificate)
        {
            using var dotNetCertificate = new X509Certificate2(caCertificate.GetEncoded());
            return OcspResponderRepository.IsCaCompromised(dotNetCertificate);
        }

        /// <param name="caCertificate"></param>
        /// <inheritdoc />
        public async Task<AsymmetricKeyParameter> GetResponderPrivateKey(X509Certificate caCertificate)
        {
            using var dotNetCertificate = new X509Certificate2(caCertificate.GetEncoded());
            var privateKey = await OcspResponderRepository.GetResponderPrivateKey(dotNetCertificate).ConfigureAwait(false);
            return DotNetUtilities.GetKeyPair(privateKey).Private;
        }

        /// <inheritdoc />
        public async Task<AsymmetricKeyParameter> GetResponderPublicKey(X509Certificate caCertificate)
        {
            using var dotNetCertificate = new X509Certificate2(caCertificate.GetEncoded());
            var privateKey = await OcspResponderRepository.GetResponderPrivateKey(dotNetCertificate).ConfigureAwait(false);
            return DotNetUtilities.GetKeyPair(privateKey).Public;
        }

        /// <param name="issuerCertificate"></param>
        /// <inheritdoc />
        public async Task<X509Certificate[]> GetChain(X509Certificate issuerCertificate)
        {
            using var dotNetCertificate = new X509Certificate2(issuerCertificate.GetEncoded());
            var certificates = await OcspResponderRepository.GetChain(dotNetCertificate).ConfigureAwait(false);
            return certificates.Select(DotNetUtilities.FromX509Certificate).ToArray();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<X509Certificate>> GetIssuerCertificates()
        {
            var certificates = await OcspResponderRepository.GetIssuerCertificates().ConfigureAwait(false);
            return certificates.Select(DotNetUtilities.FromX509Certificate).ToArray();
        }

        /// <inheritdoc />
        public async Task<DateTimeOffset> GetNextUpdate()
        {
            return await OcspResponderRepository.GetNextUpdate().ConfigureAwait(false);
        }

        private string GetHexSerialNumber(BigInteger serBigInteger)
        {
            var bytes = serBigInteger.ToByteArray();
            var sb = new StringBuilder();

            foreach (var b in bytes)
            {
                sb.AppendFormat("{0:X2}", b);
            }
            return sb.ToString();
        }
    }
}
