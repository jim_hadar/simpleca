using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OcspResponder.Models;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.X509;

namespace OcspResponder.Internal
{
    /// <summary>
    /// OCSP репозиторий, использующий библиотеку BouncyCastle
    /// </summary>
    internal interface IBcOcspResponderRepository
    {
        /// <summary>
        /// Проверка существования сертификата с указанным серийным номером.
        /// </summary>
        /// <param name="serial">Серийный номер сертификата.</param>
        /// <param name="issuerCertificate">Сертификат для проверки.</param>
        /// <returns><c>true</c> Если сертификат существует; иначе, false</returns>
        Task<bool> SerialExists(BigInteger serial, X509Certificate issuerCertificate);

        /// <summary>
        /// Проверка того, что переданный сертификат отозван в CA
        /// </summary>
        /// <param name="serial">Серийный номер сертификата.</param>
        /// <param name="issuerCertificate">Сертификат.</param>
        /// <returns>A <see cref="CertificateRevocationStatus"/>Информация об отзыве сертификата.</returns>
        Task<CertificateRevocationStatus> SerialIsRevoked(BigInteger serial, X509Certificate issuerCertificate);

        /// <summary>
        /// Проверка, что корневой CA скомпроментирован.
        /// </summary>
        /// <param name="caCertificate">Корневой сертификат для проверки.</param>
        /// <returns>A <see cref="CaCompromisedStatus"/>Информация о том, отозван ли сертификат.</returns>
        Task<CaCompromisedStatus> IsCaCompromised(X509Certificate caCertificate);

        /// <summary>
        /// Получение закрытого ключа CA.
        /// </summary>
        /// <param name="caCertificate">CA сертфикат.</param>
        /// <returns>A <see cref="AsymmetricKeyParameter"/> that represents the private key of the CA</returns>
        Task<AsymmetricKeyParameter> GetResponderPrivateKey(X509Certificate caCertificate);

        /// <summary>
        /// Gets the public key of the CA or its designated responder
        /// </summary>
        /// <param name="caCertificate"></param>
        /// <returns>A <see cref="AsymmetricKeyParameter"/> that represents the public key of the CA</returns>
        Task<AsymmetricKeyParameter> GetResponderPublicKey(X509Certificate caCertificate);

        /// <summary>
        /// Цепочка сертификатов, связанная с ответчиком.
        /// </summary>
        /// <param name="issuerCertificate"></param>
        /// <returns>An array of <see cref="X509Certificate"/></returns>
        Task<X509Certificate[]> GetChain(X509Certificate issuerCertificate);

        /// <summary>
        /// Gets the issuer certificates that this repository is responsible to evaluate
        /// </summary>
        /// <returns>A enumerable of <see cref="X509Certificate"/> that represents the issuer's certificates</returns>
        Task<IEnumerable<X509Certificate>> GetIssuerCertificates();

        /// <summary>
        /// Получает дату, когда клиент должен в следующий раз запросить информацию о статусе сертификата.
        /// </summary>
        /// <returns>A <see cref="DateTime"/>Дата следующего запроса информации о статусе сертификата клиентом.</returns>
        Task<DateTimeOffset> GetNextUpdate();
    }
}
