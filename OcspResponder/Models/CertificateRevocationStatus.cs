namespace OcspResponder.Models
{
    /// <summary>
    /// Статус отзыва сертификата.
    /// </summary>
    public class CertificateRevocationStatus
    {
        /// <summary>
        /// Если сертификат отозван.
        /// </summary>
        public bool IsRevoked { get; set; }

        /// <summary>
        /// Информация об отзыве сертификата.
        /// </summary>
        public RevokedInfo? RevokedInfo { get; set; } = null;
    }
}
