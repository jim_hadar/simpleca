using System.Net;

namespace OcspResponder.Models
{
    /// <summary>
    /// OCSP ответ.
    /// </summary>
    public class OcspHttpResponse
    {
        /// <summary>
        /// Содержимое ответа.
        /// </summary>
        public byte[] Content { get; } 

        /// <summary>
        /// Тип данных ответа.
        /// </summary>
        public string MediaType { get; }

        /// <summary>
        /// Код ответа HTTP.
        /// </summary>
        public HttpStatusCode Status { get; }

        public OcspHttpResponse(byte[] content, string mediaType, HttpStatusCode status)
        {
            Content = content;
            MediaType = mediaType;
            Status = status;
        }
    }
}
