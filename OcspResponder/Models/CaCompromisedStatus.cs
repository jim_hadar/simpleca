using System;

namespace OcspResponder.Models
{
    /// <summary>
    /// Status of compromising of the CA
    /// </summary>
    public class CaCompromisedStatus
    {
        /// <summary>
        /// If the CA is compromised
        /// </summary>
        public bool IsCompromised { get; set; }

        /// <summary>
        /// When it was compromised
        /// </summary>
        public DateTimeOffset? CompromisedDate { get; set; }
    }
}
