using System;

namespace OcspResponder.Models
{
    /// <summary>
    /// OCSP запрос в преобразованном формате.
    /// </summary>
    public class OcspHttpRequest
    {
        /// <summary>
        /// HTTP метод.
        /// </summary>
        public string HttpMethod { get; set; } = null!;

        /// <summary>
        /// Запрошенный URL.
        /// </summary>
        public Uri RequestUri { get; set; } = null!;

        /// <summary>
        /// Тип контента.
        /// </summary>
        public string MediaType { get; set; } = null!;

        /// <summary>
        /// Содержимое запроса.
        /// </summary>
        public byte[] Content { get; set; } = new byte[1];
    }
}
