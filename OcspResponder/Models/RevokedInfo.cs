using System;

namespace OcspResponder.Models
{
    /// <summary>
    /// Информация об отзыве сертификата.
    /// </summary>
    public class RevokedInfo
    {
        /// <summary>
        /// Дата отзыва сертификата.
        /// </summary>
        public DateTimeOffset Date { get; set; }

        /// <summary>
        /// Причина отзыва
        /// </summary>
        public RevocationReason Reason { get; set; }
    }
}
