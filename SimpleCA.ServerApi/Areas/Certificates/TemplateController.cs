﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimpleCA.Auth.Code;
using SimpleCA.Domain.Commands.Certificates;
using SimpleCA.Domain.Models.Certificates;
using SimpleCA.Domain.Models.Enums;
using SimpleCA.Domain.Queries.Certificates;
using SimpleCA.ServerApi.Controllers;

namespace SimpleCA.ServerApi.Areas.Certificates
{
    /// <summary>
    /// Контроллер для работы с шаблонами сертификатов.
    /// </summary>
    [Area("Certificates")]
    [Route("api/v1/[area]/[controller]")]
    [ApiController]
    [CustomAuthorize]
    public class TemplateController : BaseController
    {
        /// <inheritdoc />
        public TemplateController(IHttpContextAccessor httpContextAccessor, IMediator mediator) : base(httpContextAccessor, mediator)
        {
        }

        /// <summary>
        /// Создание шаблона сертификата
        /// </summary>
        /// <param name="model">Модель шаблона для создания..</param>
        /// <param name="token">The token.</param>
        /// <returns>Идентификатор созданного шаблона</returns>
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [HttpPost]
        [CustomAuthorize(UserRole.FullAccess)]
        public async Task<int> Create([FromBody] CertificateTemplateCreateDto model, CancellationToken token)
            => await Mediator.Send(new CreateCertificateTemplateCommand(model), token).ConfigureAwait(false);

        /// <summary>
        /// Обновление шаблона сертификата.
        /// </summary>
        /// <param name="model">Модель шаблона для обновления.</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [CustomAuthorize(UserRole.FullAccess)]
        [HttpPut]
        public async Task<IActionResult> Update([FromBody] CertificateTemplateUpdateDto model, CancellationToken token)
        {
            await Mediator.Send(new UpdateCertificateTemplateCommand(model), token).ConfigureAwait(false);
            return Ok();
        }

        /// <summary>
        /// Удаление шаблона по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор шаблона для удаления.</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(string), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [HttpDelete("{id:int}")]
        [CustomAuthorize(UserRole.FullAccess)]
        public async Task<IActionResult> Delete(int id, CancellationToken token)
        {
            await Mediator.Send(new DeleteCertificateTemplateCommand(id), token).ConfigureAwait(false);
            return NoContent();
        }

        /// <summary>
        /// Получение шаблона на сертификат по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатор шаблона на сертификат.</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(CertificateTemplateDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [HttpGet("{id:int}")]
        public async Task<CertificateTemplateDto> GetById(int id, CancellationToken token)
            => await Mediator.Send(new GetCertificateTemplateQuery(id), token).ConfigureAwait(false);

        /// <summary>
        /// Получение всех шаблонов на сертификаты.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(List<CertificateTemplateDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [HttpGet("all")]
        public async Task<List<CertificateTemplateDto>> GetAll(CancellationToken token)
            => await Mediator.Send(GetCertificateTemplatesQuery.Instance, token).ConfigureAwait(false);

    }
}
