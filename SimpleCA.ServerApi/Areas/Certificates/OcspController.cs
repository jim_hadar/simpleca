﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OcspResponder.Common;
using OcspResponder.Contracts;
using SimpleCA.Auth.Code;
using SimpleCA.ServerApi.Controllers;

namespace SimpleCA.ServerApi.Areas.Certificates
{
    /// <summary>
    /// Контроллер для работы с OCSP.
    /// </summary>
    [Area("Certificates")]
    [Route("api/v1/[area]/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class OcspController : BaseController
    {
        private readonly IOcspResponder _ocspResponder;
        /// <inheritdoc />
        public OcspController(
            IHttpContextAccessor httpContextAccessor,
            IMediator mediator,
            IOcspResponder ocspResponder)
            : base(httpContextAccessor, mediator)
        {
            _ocspResponder = ocspResponder;
        }

        /// <summary>
        /// Ocsp-ответчик для запросов типа POST.
        /// </summary>
        /// <returns>OCSP-ответ.</returns>
        [HttpPost]
        public async Task<IActionResult> Respond()
        {
            var ocspHttpRequest = await Request.ToOcspHttpRequest().ConfigureAwait(false);
            var ocspHttpResponse = await _ocspResponder
                .Respond(ocspHttpRequest)
                .ConfigureAwait(false);
            return new OcspActionResult(ocspHttpResponse);
        }

        /// <summary>
        /// Ocsp-ответчик для запросов типа GET.
        /// </summary>
        /// <param name="encoded">Закодированный OCSP-запрос.</param>
        /// <returns>OCSP-ответ.</returns>
        [HttpGet("{encoded}")]
        public async Task<IActionResult> Respond(string encoded)
        {
            var ocspHttpRequest = await Request.ToOcspHttpRequest().ConfigureAwait(false);
            var ocspHttpResponse = await _ocspResponder.Respond(ocspHttpRequest).ConfigureAwait(false);
            return new OcspActionResult(ocspHttpResponse);
        }
    }
}
