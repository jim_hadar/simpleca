﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimpleCA.Auth.Code;
using SimpleCA.Domain.Commands.Certificates;
using SimpleCA.Domain.Models.Certificates.IssuedCertificateModels;
using SimpleCA.Domain.Models.Paging;
using SimpleCA.Domain.Models.Paging.Filters;
using SimpleCA.Domain.Queries.Certificates;
using SimpleCA.ServerApi.Controllers;

namespace SimpleCA.ServerApi.Areas.Certificates
{
    /// <summary>
    /// Контроллер для работы с выпускаемыми сертификатами.
    /// </summary>
    [Area("Certificates")]
    [Route("api/v1/[area]/[controller]")]
    [ApiController]
    [CustomAuthorize]
    public class IssuedController : BaseController
    {
        /// <inheritdoc />
        public IssuedController(IHttpContextAccessor httpContextAccessor, IMediator mediator) 
            : base(httpContextAccessor, mediator)
        {
        }

        /// <summary>
        /// Получение списка выпущенных сертификатов с постраничной разбивкой.
        /// </summary>
        /// <param name="model">Фильтр.</param>
        /// <param name="token"></param>
        /// <returns>Результат фильтрации.</returns>
        [HttpGet]
        [ProducesResponseType(typeof(PagedList<IssuedCertificateDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<PagedList<IssuedCertificateDto>> GetPagedAsync(
            [FromQuery] PagingParams<IssuedCertificateFilter> model,
            CancellationToken token)
            => await Mediator.Send(new GetIssuedCertificatesQuery(model), token).ConfigureAwait(false);

        /// <summary>
        /// Создание сертификата.
        /// </summary>
        /// <param name="model">Данные для создания сертификата.</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost]
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<int> CreateAsync(
            [FromBody] IssuedCertificateCreateDto model,
            CancellationToken token)
            => await Mediator.Send(new CreateIssuedCertificateCommand(model), token).ConfigureAwait(false);

        /// <summary>
        /// Экспорт сертификата.
        /// </summary>
        /// <param name="model">Параметры экспорта сертификата.</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("export")]
        [ProducesResponseType(typeof(byte[]), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Export(
            [FromBody] ExportIssuedCertificateDto model,
            CancellationToken token)
        {
            var result = await Mediator.Send(new ExportIssuedCertificateQuery(model), token).ConfigureAwait(false);
            return ExportArchive(result, "issued_certificate.zip");
        }

        /// <summary>
        /// Отзыв сертификата.
        /// </summary>
        /// <param name="id">Идентификатор сертификата для отзыва.</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{id:int}/revoke")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> Revoke(
            int id,
            CancellationToken token)
        {
            await Mediator.Send(new RevokeIssuedCertififcateCommand(id), token).ConfigureAwait(false);
            return Ok();
        }

        /// <summary>
        /// Получение файла Crl
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("crl")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetCrlFile(
            CancellationToken token)
        {
            var crlBytes = await Mediator.Send(GetCrlFileQuery.Instance, token).ConfigureAwait(false);
            return ExportCert(crlBytes, "list.crl");
        }
    }
}
