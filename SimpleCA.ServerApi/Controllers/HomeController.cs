using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SimpleCA.ServerApi.Controllers
{
    /// <summary>
    /// Контроллер по умолчанию.
    /// </summary>
    [AllowAnonymous]
    [Route("api/v1/[controller]")]
    [ApiController]
    public class HomeController : BaseController
    {
        /// <inheritdoc />
        public HomeController(IHttpContextAccessor httpContextAccessor, IMediator mediator) 
            : base(httpContextAccessor, mediator)
        {
        }

        /// <summary>
        /// Редирект на swagger
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Index()
        {
            return Redirect("/swagger");
        }
    }
}
