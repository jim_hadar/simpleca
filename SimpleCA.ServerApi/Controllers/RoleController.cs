using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimpleCA.Auth.Code;
using SimpleCA.Domain.Models.Users;
using SimpleCA.Domain.Queries.Users;

namespace SimpleCA.ServerApi.Controllers
{
    /// <summary>
    /// Контроллер для работы с ролями пользователей.
    /// </summary>
    [CustomAuthorize]
    [ApiController]
    [Route("api/v1/[controller]")]
    public class RoleController : BaseController
    {
        /// <inheritdoc />
        public RoleController(IHttpContextAccessor httpContextAccessor, IMediator mediator)
            : base(httpContextAccessor, mediator)
        {
        }

        /// <summary>
        /// Получение всех ролей пользователей.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(typeof(List<RoleDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<List<RoleDto>> GetAllRoles(CancellationToken token)
            => await Mediator.Send(GetUserRolesQuery.Instance, token).ConfigureAwait(false);

        /// <summary>
        /// Получение роли по ее идентификатору. 
        /// </summary>
        /// <param name="id">Идентификатор роли.</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet("{id:int}")]
        [ProducesResponseType(typeof(RoleDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GetRoleBydId(int id, CancellationToken token)
        {
            var roles = await Mediator.Send(GetUserRolesQuery.Instance, token).ConfigureAwait(false);
            if (roles.All(_ => _.Id != id))
                return NotFound();
            return Ok(roles.First(_ => _.Id == id));
        }
    }
}
