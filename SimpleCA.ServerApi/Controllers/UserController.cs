using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimpleCA.Auth.Code;
using SimpleCA.Domain.Commands.Users;
using SimpleCA.Domain.Models.Enums;
using SimpleCA.Domain.Models.Paging;
using SimpleCA.Domain.Models.Paging.Filters;
using SimpleCA.Domain.Models.Users;
using SimpleCA.Domain.Queries.Users;

namespace SimpleCA.ServerApi.Controllers
{
    /// <summary>
    /// Контроллер для работы с пользователями.
    /// </summary>
    [Route("api/v1/[controller]")]
    [ApiController]
    [CustomAuthorize]
    public class UserController : BaseController
    {
        /// <inheritdoc />
        public UserController(IHttpContextAccessor httpContextAccessor, IMediator mediator) : base(httpContextAccessor, mediator)
        {
        }

        /// <summary>
        /// Cоздание пользователя
        /// </summary>
        /// <param name="model">Модель пользователя для создания.</param>
        /// <param name="token"></param>
        /// <returns>Идентификтатор созданного пользователя.</returns>
        [ProducesResponseType(typeof(int), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [HttpPost]
        [CustomAuthorize(UserRole.FullAccess)]
        public async Task<int> CreateUser([FromBody] UserAddDto model, CancellationToken token)
            => await Mediator.Send(new CreateUserCommand(model), token).ConfigureAwait(false);

        /// <summary>
        /// Обновление пользователя.
        /// </summary>
        /// <param name="model">Модель пользователя для создания.</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [HttpPut]
        [CustomAuthorize(UserRole.FullAccess)]
        public async Task<IActionResult> UpdateUser([FromBody] UserUpdateDto model, CancellationToken token)
        {
            await Mediator.Publish(new UpdateUserCommand(model), token).ConfigureAwait(false);
            return Ok();
        }

        /// <summary>
        /// Удаление пользователя по его идентификатору. 
        /// </summary>
        /// <param name="id">Идентификатор пользователя для удаления.</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpDelete("{id:int}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status204NoContent)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> DeleteUser(int id, CancellationToken token)
        {
            await Mediator.Publish(new DeleteUserCommand(id), token).ConfigureAwait(false);
            return NoContent();
        }

        /// <summary>
        /// Получение пользователя по его идентификатору.
        /// </summary>
        /// <param name="id">Идентификатору пользователя.</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [ProducesResponseType(typeof(UserDto), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [HttpGet("{id:int}")]
        public async Task<UserDto> GetUserById(int id, CancellationToken token)
            => await Mediator.Send(new GetUserQuery(id), token).ConfigureAwait(false);

        /// <summary>
        /// Получение пользователей с постраничной разбивкой.
        /// </summary>
        /// <param name="filter">Фильтр.</param>
        /// <param name="token"></param>
        /// <returns>Пользователи с постраничной разбивкой.</returns>
        [ProducesResponseType(typeof(PagedList<UserDto>), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        [HttpGet]
        public async Task<PagedList<UserDto>> GetPagedUsers([FromQuery]PagingParams<UserFilter> filter, CancellationToken token)
            => await Mediator.Send(new GetPagedUsersQuery(filter), token).ConfigureAwait(false);
        
    }
}
