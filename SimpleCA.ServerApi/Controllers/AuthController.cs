using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SimpleCA.Domain.Commands.Users;
using SimpleCA.Domain.Models.Users;

namespace SimpleCA.ServerApi.Controllers
{
    /// <summary>
    /// Контроллер авторизации.
    /// </summary>
    [AllowAnonymous]
    [Route("api/v1/[controller]")]
    public class AuthController : BaseController
    {
        /// <inheritdoc />
        public AuthController(IHttpContextAccessor httpContextAccessor, IMediator mediator) 
            : base(httpContextAccessor, mediator)
        {
        }

        /// <summary>
        /// Авторизация пользователя.
        /// </summary>
        /// <param name="model">Данные для аутентификации пользователя.</param>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpPost("[action]")]
        [ProducesResponseType(typeof(JwtAuthSuccessResponse), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
        public async Task<JwtAuthSuccessResponse> Login([FromBody] CredentialsModel model, CancellationToken token)
            => await Mediator.Send(new LoginUserCommand(model), token).ConfigureAwait(false);
    }
}
