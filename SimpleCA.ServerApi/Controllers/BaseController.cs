﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SimpleCA.ServerApi.Controllers
{
    /// <summary>
    /// Базовый класс для всех контроллеров, содержит ссылки на репозитории и контекст.
    /// </summary>
    [ApiController]
    public class BaseController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        protected IHttpContextAccessor HttpContextAccessor;

        /// <summary>
        /// The mediator
        /// </summary>
        protected readonly IMediator Mediator;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContextAccessor"></param>
        /// <param name="mediator"></param>
        public BaseController(IHttpContextAccessor httpContextAccessor, IMediator mediator)
        {
            HttpContextAccessor = httpContextAccessor;
            Mediator = mediator;
        }

        /// <summary>
        /// Экспорт сертификата / запроса на сертификат
        /// </summary>
        /// <param name="bytesFile">Байтовое представление файла</param>
        /// <param name="fileName">Имя файла</param>
        /// <returns></returns>
        protected IActionResult ExportCert(byte[] bytesFile, string fileName)
        {
            //Добавляем заголовок Access-Control-Expose-Headers для того, чтобы браузер мог читать Content-Disposition
            AddAccessControlDisposeHeader();
            return File(bytesFile, "application/x-pem-file;charset=UTF-8", fileName);
        }

        /// <summary>
        /// Экспорт данных и проставление Mime-type "application/zip;charset=UTF-8"
        /// </summary>
        /// <param name="bytesFile">The bytes file.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <returns></returns>
        protected IActionResult ExportArchive(byte[] bytesFile, string fileName)
        {
            //Добавляем заголовок Access-Control-Expose-Headers для того, чтобы браузер мог читать Content-Disposition
            AddAccessControlDisposeHeader();
            return File(bytesFile, "application/zip;charset=UTF-8", fileName);
        }

        /// <summary>
        /// Добавление к ответу HTTP заголовка Access-Control-Expose-Headers со значениями:
        /// Content-Disposition
        /// </summary>
        protected void AddAccessControlDisposeHeader()
        {
            Response.Headers.Add("Access-Control-Expose-Headers", "Content-Disposition");
        }
    }
}
