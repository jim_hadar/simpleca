﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;
using SimpleCA.Core.Exceptions;
using SimpleCA.Logging.Contracts;

namespace SimpleCA.ServerApi.Code
{
    /// <inheritdoc />
    public class HttpGlobalExceptionFilter : IExceptionFilter
    {
        /// <inheritdoc />
        public void OnException(ExceptionContext context)
        {
            if (context == null)
                return;
            switch (context.Exception)
            {
                case EntityNotFoundException notFoundEx:
                    context.Result = new NotFoundResult();
                    break;
                case CommonException exception:
                    {
                        var loggerFactory = context.HttpContext.RequestServices.GetRequiredService<ILoggerFactory>();
                        var logger = loggerFactory.DefaultLogger();
                        logger.Error(context.Exception, context.Exception.Message);
                        context.Result = new BadRequestObjectResult(exception.Message);
                        break;
                    }
                default:
                    {
                        var loggerFactory = context.HttpContext.RequestServices.GetRequiredService<ILoggerFactory>();
                        var logger = loggerFactory.DefaultLogger();
                        logger.Error(context.Exception, context.Exception.Message);
                        context.Result = new BadRequestObjectResult("Unhandled error.");
                        break;
                    }
            }
        }
    }
}
