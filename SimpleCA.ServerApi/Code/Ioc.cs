﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;
using AutoMapper;
using FluentValidation.AspNetCore;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using SimpleCA.Auth.Code;
using SimpleCA.Business.Validators;
using SimpleCA.Data.PostgreSql.Code;
using SimpleCA.Logging.Code;
using System.Threading.Tasks;
using SimpleCA.Security.Code;

namespace SimpleCA.ServerApi.Code
{
    /// <summary>
    /// Ioc for IServiceCollection
    /// </summary>
    public static class Ioc
    {
        /// <summary>
        /// Initializes the di services.
        /// </summary>
        /// <param name="services">The services.</param>
        /// <param name="configuration">The configuration.</param>
        internal static void InitializeDiServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddLocalization();
            services.Configure<RequestLocalizationOptions>(
                opts =>
                {
                    /* your configurations*/
                    var supportedCultures = new List<CultureInfo>
                    {
                        new CultureInfo("en"),
                        new CultureInfo("ru")
                    };

                    opts.DefaultRequestCulture = new RequestCulture("en", "en");
                    // Formatting numbers, dates, etc.
                    opts.SupportedCultures = supportedCultures;
                    // UI strings that we have localized.
                    opts.SupportedUICultures = supportedCultures;
                    opts.AddInitialRequestCultureProvider(new CustomRequestCultureProvider(context =>
                    {
                        var lang = context.RequestServices.GetRequiredService<IConfiguration>().GetValue<string>("Language");
                        return Task.FromResult(new ProviderCultureResult(lang));
                    }));
                });

            services.ConfigureMediatR();
            services.ConfigureData(configuration);
            services.ConfigureSecurity();
            services.ConfigureAutoMapper();

            services.AddControllers(opt =>
                {
                    opt.Filters.Add(typeof(HttpGlobalExceptionFilter));
                    opt.Filters.Add(typeof(ValidatorActionFilter));
                })
                .ConfigureApiBehaviorOptions(options =>
                {
                    options.SuppressConsumesConstraintForFormFileParameters = true;
                    options.SuppressInferBindingSourcesForParameters = true;
                    options.SuppressModelStateInvalidFilter = true;
                    options.SuppressMapClientErrors = true;
                })
                .AddJsonOptions(opt =>
                {
                    opt.JsonSerializerOptions.PropertyNameCaseInsensitive = false;
                    opt.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                    opt.JsonSerializerOptions.IgnoreNullValues = false;
                    opt.JsonSerializerOptions.IgnoreReadOnlyProperties = false;
                    opt.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                })
                .AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblies(new List<Assembly>()
                {
                    Assembly.GetAssembly(typeof(CredentialsValidator))!,
                    Assembly.GetAssembly(typeof(Security.Code.Ioc))!
                }));

            services.ConfigureLogger();

            services.ConfigureSwagger();

            services.ConfigureJwtAuth(configuration);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        internal static void ConfigureApp(this IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseRouting();
            app.UseFileServer();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRequestLocalization();

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.All
            });

            app.Use(async (context, next) =>
            {
#pragma warning disable CA2007 // Consider calling ConfigureAwait on the awaited task
                await next();
#pragma warning restore CA2007 // Consider calling ConfigureAwait on the awaited task
            });

            app.UseCors(builder =>
            {
                builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            });

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.All
            });

            app.Use(async (context, next) =>
            {
#pragma warning disable CA2007 // Consider calling ConfigureAwait on the awaited task
                await next();
#pragma warning restore CA2007 // Consider calling ConfigureAwait on the awaited task
            });

            app.UseCors(builder =>
            {
                builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            });

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API v1");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute("default", "api/v1/{controller=Home}/{action=Index}/{id?}");
                endpoints.MapFallbackToController("index", "home");
            });
        }

        /// <summary>
        /// Configures the mediat r.
        /// </summary>
        /// <param name="services">The services.</param>
        private static void ConfigureMediatR(this IServiceCollection services)
        {
            var assemblyBusiness = Assembly.GetAssembly(typeof(Business.Validators.RulesBuilder<>));
            var assemblySecurity = Assembly.GetAssembly(typeof(Security.Code.Ioc));
            services.AddMediatR(assemblyBusiness, assemblySecurity);
        }

        private static void ConfigureAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(Assembly.GetAssembly(typeof(Business.Validators.RulesBuilder<>)),
                Assembly.GetAssembly(typeof(Security.AutoMapperProfiles.IssuedCertificateTemplateProfile)));
        }

        private static void ConfigureSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                foreach (Assembly a in AppDomain.CurrentDomain.GetAssemblies())
                {
                    foreach (Type t in a.GetTypes())
                    {
                        if (t.IsEnum)
                        {
                            c.MapType(t, () => new OpenApiSchema
                            {
                                Type = "string",
                                Enum = t.GetEnumNames().Select(name => new OpenApiString(name)).Cast<IOpenApiAny>().ToList(),
                                Nullable = true
                            });
                        }
                    }
                }

                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "API",
                    Version = "v1",
                    Description = "API для работы с ядром",
                    Contact = new OpenApiContact
                    {
                        Name = "",
                        Email = "test@mail.ru",
                    }
                });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme{
                            Reference = new OpenApiReference{
                                Id = "Bearer", //The name of the previously defined security scheme.
                                Type = ReferenceType.SecurityScheme
                            }
                        },
                        new List<string>()
                    }
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
                c.ResolveConflictingActions(apiDescriptions => apiDescriptions.First());
            });
        }
    }
}
