using System.Linq;
using System.Threading.Tasks;
using MediatR;
using SimpleCA.Data.Models;

namespace SimpleCA.Data.Extensions
{
    public static class MediatorExtensions
    {
        public static async Task DispatchDomainEventsAsync(this IMediator mediator, IBaseEntity entity)
        {
            var domainEvents = entity.DomainEvents.ToList();
            entity.ClearDomainEvents();
            foreach (var domainEvent in domainEvents)
                await mediator.Publish(domainEvent).ConfigureAwait(false);
        }
    }
}