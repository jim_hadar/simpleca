using System;

namespace SimpleCA.Data.Models.Certificates
{
    /// <summary>
    /// Описание сущности сертификат.
    /// </summary>
    public interface ICertificate
    {
        /// <summary>
        /// Идентификатор сертификата.
        /// </summary>
        int Id { get; set; }
        
        /// <summary>
        /// Base64 представление сертификата
        /// </summary>
        string Encoded { get; set; }
        
        /// <summary>
        /// Private Key
        /// </summary>
        string PrivateKey { get; set; }
        
        /// <summary>
        /// Версия.
        /// </summary>
        int Version { get; set; }

        /// <summary>
        /// CN = XXX, O = XXX...
        /// </summary>
        string Subject { get; set; }

        /// <summary>
        /// Издатель.
        /// </summary>
        string Issuer { get; set; }

        /// <summary>
        /// Сертификат действителен "с"
        /// </summary>
        DateTime ValidFrom { get; set; }

        /// <summary>
        /// Сертификат действителен "по"
        /// </summary>
        DateTime ValidTo { get; set; }

        /// <summary>
        /// Serial number
        /// </summary>
        string SerialNumber { get; set; }

        /// <summary>
        /// 
        /// </summary>
        string SignatureAlgorithm { get; set; }
    }
}
