﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleCA.Data.Models.Certificates
{
    public class IssuedCertificate : ICertificate
    {
        /// <inheritdoc />
        [Key]
        public int Id { get; set; }

        /// <inheritdoc />
        public string Encoded { get; set; } = string.Empty;

        /// <inheritdoc />
        public string PrivateKey { get; set; } = string.Empty;
        
        /// <inheritdoc />
        public int Version { get; set; }

        /// <inheritdoc />
        public string Subject { get; set; } = string.Empty;

        /// <inheritdoc />
        public string Issuer { get; set; } = string.Empty;
        
        /// <inheritdoc />
        public DateTime ValidFrom { get; set; }

        /// <inheritdoc />
        public DateTime ValidTo { get; set; }

        /// <inheritdoc />
        public string SerialNumber { get; set; } = string.Empty;

        /// <inheritdoc />
        public string SignatureAlgorithm { get; set; } = string.Empty;
        
        /// <summary>
        /// Идентификатор корневого сертификата.
        /// </summary>
        public int RootCertificateId { get; set; }
        
        /// <summary>
        /// Дата отзыва
        /// </summary>
        public DateTime? RevocationDate { get; set; }

        /// <summary>
        /// Альтернативные имена домена.
        /// </summary>
        public string Sans { get; set; } = string.Empty;

        /// <summary>
        /// Отозван ли сертификат.
        /// </summary>
        [NotMapped]
        public bool IsRevoked => RevocationDate.HasValue;

        #region [ Navigation ]

        /// <summary>
        /// Ссылка на корневой сертификат.
        /// </summary>
        public virtual RootCertificate RootCertificate { get; set; } = null!;

        #endregion

    }
}
