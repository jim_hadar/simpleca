using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SimpleCA.Data.Models.Certificates
{
    public class RootCertificate : ICertificate
    {
        /// <summary>
        /// ID корневого сертификата
        /// </summary>
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Friendly название корневого сертификата
        /// </summary>
        public string Name { get; set; } = string.Empty;
        
        /// <summary>
        /// Описание.
        /// </summary>
        public string? Description { get; set; }

        /// <inheritdoc />
        public string Encoded { get; set; } = string.Empty;
        
        /// <inheritdoc />
        public string PrivateKey { get; set; } = string.Empty;
        
        /// <inheritdoc />
        public int Version { get; set; }
        
        /// <inheritdoc />
        public string Subject { get; set; } = string.Empty;
        
        /// <inheritdoc />
        public string Issuer { get; set; } = string.Empty;
        
        /// <inheritdoc />
        public DateTime ValidFrom { get; set; }

        /// <inheritdoc />
        public DateTime ValidTo { get; set; }

        /// <inheritdoc />
        public string SerialNumber { get; set; } = string.Empty;

        /// <inheritdoc />
        public string SignatureAlgorithm { get; set; } = string.Empty;

        #region [ Navigation ]

        /// <summary>
        /// Список выпущенных сертификатов.
        /// </summary>
        public virtual ICollection<IssuedCertificate> IssuedCertificates { get; set; }
            = new List<IssuedCertificate>();

        #endregion
    }
}
