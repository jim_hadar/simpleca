using Microsoft.AspNetCore.Identity;

namespace SimpleCA.Data.Models.Identity
{
    /// <summary>
    /// связь между пользователями и ролями.
    /// </summary>
    public class UserToRoleLink: IdentityUserRole<int>
    {
        /// <summary>
        /// Роль пользователя. 
        /// </summary>
        public virtual Role Role { get; set; } = null!;

        /// <summary>
        /// Пользователь
        /// </summary>
        public virtual User User { get; set; } = null!;
    }
}