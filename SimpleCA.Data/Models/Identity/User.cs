using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MediatR;
using Microsoft.AspNetCore.Identity;

namespace SimpleCA.Data.Models.Identity
{
    /// <summary>
    /// Пользователь
    /// </summary>
    [Table("user")]
    public class User : IdentityUser<int>, IBaseEntity
    {
        #region [ IBaseEntity impl ]

        private List<INotification> _domainEvents = new List<INotification>();

        /// <inheritdoc />
        public IReadOnlyCollection<INotification> DomainEvents => _domainEvents.AsReadOnly();

        /// <inheritdoc />
        public void AddDomainEvent(INotification eventItem)
        {
            _domainEvents ??= new List<INotification>();
            _domainEvents.Add(eventItem);
        }

        /// <inheritdoc />
        public void RemoveDomainEvent(INotification eventItem)
        {
            _domainEvents?.Remove(eventItem);
        }

        /// <inheritdoc />
        public void ClearDomainEvents()
        {
            _domainEvents?.Clear();
        }

        #endregion

        /// <summary>
        /// Описание пользователя
        /// </summary>
        public string? Description { get; set; }

        /// <summary>
        /// Gets or sets the date changed.
        /// </summary>
        public DateTime DateChanged { get; set; } = DateTime.Now;

        #region [ Navigation ]

        public virtual ICollection<UserToRoleLink> Roles { get; set; }
            = new List<UserToRoleLink>();

        #endregion
    }
}