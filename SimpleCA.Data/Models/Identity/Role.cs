using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace SimpleCA.Data.Models.Identity
{
    public class Role: IdentityRole<int>
    {
        public virtual ICollection<UserToRoleLink> Users { get; set; }
            = new List<UserToRoleLink>();
    }
}