using System;
using System.ComponentModel.DataAnnotations;

namespace SimpleCA.Data.Models
{
    /// <summary>
    /// Логгер
    /// </summary>
    /// <seealso cref="BaseEntity" />
    public class LogEntity : BaseEntity
    {
        /// <summary>
        /// Идентификатор события.
        /// </summary>
        [Key]
        public long Id { get; set; }

        /// <summary>
        /// Дата события.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Текст исключения
        /// </summary>
        public string? Exception { get; set; }

        /// <summary>
        /// Уровень события
        /// </summary>
        public string Level { get; set; } = "Info";

        /// <summary>
        /// Тип логера.
        /// </summary>
        public string Logger { get; set; } = "voltron";

        /// <summary>
        /// Сообщение.
        /// </summary>
        public string? Message { get; set; }

        /// <summary>
        /// Трассировка исключения.
        /// </summary>
        public string? Stacktrace { get; set; }

        /// <summary>
        /// Пользователь.
        /// </summary>
        public string? Username { get; set; }

        /// <summary>
        /// ThreadId
        /// </summary>
        public string? Thread { get; set; }
    }
}