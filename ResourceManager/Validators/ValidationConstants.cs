﻿namespace ResourceManager.Validators
{
    /// <summary>
    /// Общие сообщения валидации
    /// </summary>
    public class ValidationConstants
    {
        /// <summary>
        /// Сообщение о том, что поле не может быть пустым
        /// </summary>
        public const string NotEmptyFieldMessage = "Field '{PropertyName}' cannot be empty";

        /// <summary>
        /// Сообщение о том, что длина поля не должна превышать 50 символов.
        /// </summary>
        public const string Max50FieldLength = "Field length '{PropertyName}' must not exceed 50 characters";
        
        /// <summary>
        /// Сообщение о том, что длина поля не должна превышать 250 символов.
        /// </summary>
        public const string Max250FieldLength = "Field length '{PropertyName}' must not exceed 250 characters";

        /// <summary>
        /// Сообщение о том, что поле должно быть уникальным
        /// </summary>
        public const string UniqueFieldMessage = "Field '{PropertyName}' must be unique";

        /// <summary>
        /// Сообщение о том, что поле может иметь только определнную длину 
        /// </summary>
        public const string RequireLength =
            "Field length '{PropertyName}' must be at least 6 characters and no more than 50 characters";

        /// <summary>
        /// Сообщение о том, что поле может содеражть только латинские буквы и цифры, а также спец. символы _ и -
        /// </summary>
        public const string OnlyLatinAndSpecSymbolsTemplate =
            "The field '{PropertyName}' can contain only latin letters, numbers and special characters _, -";

        /// <summary>
        /// Сообщение о том, что поле должно содержать цифры.
        /// </summary>
        public const string ContainsDigitsTemplate = "The field '{PropertyName}' must be contain digits";

        /// <summary>
        /// Сообщение о том, что поле должно содержать символы в нижнем регистре.
        /// </summary>
        public const string ContainsLowerCaseTemplate =
            "The field '{PropertyName}' must be contain characters in lower case";
        
        /// <summary>
        /// Сообщение о том, что поле должно содержать символы в верхнем регистре.
        /// </summary>
        public const string ContainsUpperCaseTemplate =
            "The field '{PropertyName}' must be contain characters in upper case";
        
        /// <summary>
        /// Сообщение о том, что поле должно содержать нецифровые и нечисловые символы.
        /// </summary>
        public const string ContainsNonAlphaNumericTemplate = 
            "The field '{PropertyName}' must be contain non alpha numeric characters.";

        /// <summary>
        /// Сообщение о том, что время действия сертификата не может быть меньше 180 дней.
        /// </summary>
        public const string Min180DaysCertificateValidPeriod = "The field '{PropertyName}' must be greater or equal 180 days.";

        /// <summary>
        /// Сообщение о том, что время действия сертификата не может быть больше 3652 дней.
        /// </summary>
        public const string Max3652DaysCertificateValidPeriod =
            "The field '{PropertyName}' must be less or equal 3652 days.";

        /// <summary>
        /// The min1 symbol max2 symbol lengthСообщение о том, что значение поля может быть не менее 1 символа и не более 2-х символов.
        /// </summary>
        public const string Min1SymbolMax2SymbolLength =
            "The field '{PropertyName}' length must be greater or equal 1 and less or equal 2.";

        /// <summary>
        /// Сообщение о том, что поле может содержать только кириллицу, латиницу, цифры и знак пробела.
        /// </summary>
        public const string CertificateSubjectTemplate =
            "Field '{PropertyName}' must be contains only alphabet characters, digits and whitespace";

        /// <summary>
        /// Сообщение о том, что флаг использования для проверки подлинности клиента может быть установлен только если флаг 
        /// проверки подлинности сервера снят.
        /// </summary>
        public const string CertificateTemplateMustBeExtendedClient =
            "The field '{PropertyName}' value can only set if the flag 'ExtendedKeyUsageServerAuth' unchecked";

        /// <summary>
        /// Сообщение о том, что флаг использования для проверки подлинности сервера может быть установлен только если флаг
        /// проверки подлинности клиента снят.
        /// </summary>
        public const string CertificateTemplateMustBeExtendedServer =
            "The field '{PropertyName}' value can only set if the flag 'ExtendedKeyUsageClientAuth' unchecked";

        /// <summary>
        /// Сообщение о том, что шаблон сертификата не найден.
        /// </summary>
        public const string IssuedCertificateTemplateNotFound =
            "Certificate template not found.";

        /// <summary>
        /// Сообщение о том, что сертификат не найден
        /// </summary>
        public const string IssuedCertificateNotFoundOrRevoked =
            "Issued certificate not found or revoked";
    }
}
