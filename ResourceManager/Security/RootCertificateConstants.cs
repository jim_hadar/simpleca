﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResourceManager.Security
{
    public class RootCertificateConstants
    {
        /// <summary>
        /// Сообщение об ошибке проверки корневого сертификата.
        /// </summary>
        public const string RootCertificateCheckError = "The root certificate checking error: {0}";

        /// <summary>
        /// Сообщение об успешности создания корневого сертификата.
        /// </summary>
        public const string RootCertificateCreate = "The root certificate with ID = {0} created.";
    }
}
