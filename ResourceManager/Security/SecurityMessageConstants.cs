﻿namespace ResourceManager.Security
{
    /// <summary>
    /// 
    /// </summary>
    public class SecurityMessageConstants
    {
        /// <summary>
        /// Сообщение об ощибке импорта rsa ключа
        /// </summary>
        public const string CannotImportRsaPrivateKey = "Cannot import private key";

        /// <summary>
        /// Сообщение о том, что передано не верное альтернативное имя субъекта типа URI
        /// </summary>
        public const string InvalidUriSubjectAlternativeName = "Invalid Subject Alternative Name Uri";

        /// <summary>
        /// Сообщение о том, что передано неверное альтернативное имя субъъекта типа Ip Address
        /// </summary>
        public const string InvalidIpSubjectAlternativeName = "Invalid Subject Alternative Name IpAddress";

        /// <summary>
        /// Сообщение о том, что передан не корректный тип альнарнативного имени субъекта.
        /// </summary>
        public const string InvalidSubjectAlternativeNameType = "Invalid Subject Alternative Name type";
    }
}
