﻿namespace ResourceManager.Security
{
    public class IssuedCertificateConstants
    {
        /// <summary>
        /// Сертификат с указанным идентификатором не найден
        /// </summary>
        public const string NotFound = "Issued certificate with ID = {0} not found";

        /// <summary>
        /// Сообщение об успешном выпуске сертификата.
        /// </summary>
        public const string CreateSuccess = "Issued certificate {0} created";

        /// <summary>
        /// Сообщение об ошибке выпуска сертификата.
        /// </summary>
        public const string CreateError = "Issued certificate error: {0}";

        /// <summary>
        /// Сообщение об ошибке получения списка выпущенных сертификатов.
        /// </summary>
        public const string GetPagedError = "Issued certificates paged list query error";

        /// <summary>
        /// Сообщение об ошибке экспорта сертификата.
        /// </summary>
        public const string ExportCertificateError = "Issued certificate export error: {0}";

        /// <summary>
        /// Сообщение об успешности отзыва сертификата.
        /// </summary>
        public const string CertificateRevokedSuccess = "Issued certificate {0} revoked success.";

        /// <summary>
        /// Сообщение об ошибке отзыва сертификата.
        /// </summary>
        public const string CertificateRevokeError = "Issued certificate revoke error.";

        /// <summary>
        /// Сообщение об ошибке обновления файла Crl.
        /// </summary>
        public const string UpdateCrlFileError = "Certificate revokation list update error.";

        /// <summary>
        /// Сообщение об успешности обновления файла Crl.
        /// </summary>
        public const string UpdateCrlFileSuccess = "Certificate revokation list updated.";

        /// <summary>
        /// Сообщение об ошибке получения файла Crl.
        /// </summary>
        public const string GetCrlFileError = "Crl file getting error.";
    }
}
