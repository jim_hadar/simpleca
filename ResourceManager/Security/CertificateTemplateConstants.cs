﻿namespace ResourceManager.Security
{
    public class CertificateTemplateConstants
    {
        /// <summary>
        /// Сообщение о том, что шаблон сертификтаов не найден.
        /// </summary>
        public const string CertificateTemplateNotFound = "Certificate template with ID = {0} not found.";

        /// <summary>
        /// Сообщение об успешном создании шаблона сертификата.
        /// </summary>
        public const string CreateSuccess = "Certificate template {0} created.";

        /// <summary>
        /// Сообщение об ошибке создания шаблона сертификата.
        /// </summary>
        public const string CreateError = "Certificate template creating error: {0}";

        /// <summary>
        /// Сообщение об успешности обновления шаблона сертификата.
        /// </summary>
        public const string UpdateSuccess = "Certificate template {0} updated.";

        /// <summary>
        /// Сообщение об ошибке обновления шаблона сертификата.
        /// </summary>
        public const string UpdateError = "Certificate template updating error: {0}";

        /// <summary>
        /// Сообщение об успешности удаления шаблона на сертификат.
        /// </summary>
        public const string DeleteSuccess = "Certificate template {0} deleted.";

        /// <summary>
        /// Сообщение об ошибке удаления шаблона на сертификат.
        /// </summary>
        public const string DeleteError = "Certificate template deleting error: {0}";

        /// <summary>
        /// Сообщение об ошибке получения по идентификатору.
        /// </summary>
        public const string GetError = "Get template certificate with ID = {0} error: {1}";

        public const string GetAllError = "Get certificate templates error: {0}";
    }
}
