namespace ResourceManager.Auth
{
    public class AuthConstants
    {
        public const string InvalidLoginOrPass = "Invalid login or password";
        public const string LoggedSuccess = "User {0} is logged in successfully";
    }
}
