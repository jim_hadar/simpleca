﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ResourceManager.Users
{
    /// <summary>
    /// Константы сообщений для пользователей.
    /// </summary>
    public class UserConstants
    {
        /// <summary>
        /// Сообщение успешного создания пользователя.
        /// </summary>
        public const string CreateUserSuccess = "User {0} created successfully.";

        /// <summary>
        /// Сообщение успешного обновления пользователя.
        /// </summary>
        public const string UpdateUserSuccess = "User {0} updated successfully.";

        /// <summary>
        /// Сообщение успешного удаления пользователя.
        /// </summary>
        public const string UserDeleteSuccess = "User {0} deleted successfully.";

        /// <summary>
        /// Сообщение успешного удаления нескольких пользователей.
        /// </summary>
        public const string UsersDeleteSuccess = "Users {0} deleted successfully.";

        /// <summary>
        /// Сообщение ошибки при создании пользователя
        /// </summary>
        public const string CreateUserError = "Can not create user {0}: {1}";

        /// <summary>
        /// Сообщение ошибки при обновлении пользователя.
        /// </summary>
        public const string UpdateUserError = "Can not update user {0}: {1}";

        /// <summary>
        /// Сообщение ошибки при удалении пользователя.
        /// </summary>
        public const string UserDeleteError = "Can not delete user with id = {0}: {1}";

        /// <summary>
        /// Сообщение ошибки при удалении нескольких пользователей.
        /// </summary>
        public const string UsersDeleteError = "Can not delete users {0}";

        /// <summary>
        /// Сообщение об ошибке получения пользователя по его идентификатору.
        /// </summary>
        public const string GetUserQueryError = "Can not query user with id = {0}: {1}";

        /// <summary>
        /// 
        /// </summary>
        public const string UserNotFound = "User not found";

        /// <summary>
        /// 
        /// </summary>
        public const string GetPagedUsersQueryError = "Cannot get paged users: {0}";
    }
}
