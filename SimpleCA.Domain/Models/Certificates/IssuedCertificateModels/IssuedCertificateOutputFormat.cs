namespace SimpleCA.Domain.Models.Certificates.IssuedCertificateModels
{
    /// <summary>
    /// Формат выгрузки клиентского сертификата.
    /// </summary>
    public enum IssuedCertificateOutputFormat
    {
        /// <summary>
        /// Сертификат в формате PEM,
        /// закрытый ключ - в формате PKCS8
        /// </summary>
        CertInPem_KeyInPKCS8 = 1,

        /// <summary>
        /// Сертификат и закрытый ключ в одном файле
        /// </summary>
        PKCS12 = 2,
    }
}
