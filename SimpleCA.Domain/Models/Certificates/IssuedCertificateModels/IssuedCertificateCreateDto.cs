using System.Collections.Generic;

namespace SimpleCA.Domain.Models.Certificates.IssuedCertificateModels
{
    public class IssuedCertificateCreateDto
    {
        /// <summary>
        /// Общее имя.
        /// </summary>
        public string CommonName { get; set; } = string.Empty;
        
        /// <summary>
        /// Идентификатор шаблона.
        /// </summary>
        public int TemplateId { get; set; }
        
        /// <summary>
        /// Альтернативные имена сертификата.
        /// </summary>
        public List<SubjectAlternativeName> Sans { get; set; }
            = new List<SubjectAlternativeName>();
    }
}
