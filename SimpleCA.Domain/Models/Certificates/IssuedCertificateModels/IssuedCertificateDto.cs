﻿using System;

namespace SimpleCA.Domain.Models.Certificates.IssuedCertificateModels
{
    /// <summary>
    /// Dto выпускаемого сертификата.
    /// </summary>
    public class IssuedCertificateDto
    {
        /// <summary>
        /// ID корневого сертификата
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Версия
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// Субъект
        /// </summary>
        public string Subject { get; set; } = string.Empty;

        /// <summary>
        /// Время действия с 
        /// </summary>
        public DateTime ValidFrom { get; set; }

        /// <summary>
        /// Время действия по
        /// </summary>
        public DateTime ValidTo { get; set; }

        /// <summary>
        /// Серийный номер.
        /// </summary>
        public string SerialNumber { get; set; } = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public string SignatureAlgorithm { get; set; } = string.Empty;
        /// <summary>
        /// Дата отзыва
        /// </summary>
        public DateTime? RevocationDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public CertificateSubject ParsedSubject { get; set; } = null!;

        /// <summary>
        /// Альтернативные имена домена.
        /// </summary>
        public string Sans { get; set; } = string.Empty;

        /// <summary>
        /// Отозван ли сертификат.
        /// </summary>
        public bool IsRevoked => RevocationDate.HasValue; 
    }
}
