﻿namespace SimpleCA.Domain.Models.Certificates.IssuedCertificateModels
{
    /// <summary>
    /// Формат экспорта выпущенного сертификата.
    /// </summary>
    public class ExportIssuedCertificateDto
    {
        /// <summary>
        /// Идентификатор сертификата для экспорта.
        /// </summary>
        public int CertificateId { get; set; }

        /// <summary>
        /// Формат сертификата.
        /// </summary>
        public IssuedCertificateOutputFormat Format { get; set; }
    }
}
