namespace SimpleCA.Domain.Models.Certificates
{
    /// <summary>
    /// Альтернативные имена
    /// </summary>
    public class SubjectAlternativeName
    {
        /// <summary>
        /// Тип San
        /// </summary>
        public SubjectAlternativeNameType Type { get; set; }

        /// <summary>
        /// Значение
        /// </summary>
        public string Value { get; set; } = string.Empty;
    }
}
