using System;
using System.Collections.Generic;

namespace SimpleCA.Domain.Models.Certificates
{
    /// <summary>
    /// Расширенное описание субъекта
    /// </summary>
    public class CertificateSubject
    {
        public string? CommonName { get; set; }
        public string? OrganizationUnit { get; set; }
        public string Organization { get; set; } = string.Empty;
        public string? City { get; set; }
        public string State { get; set; } = string.Empty;
        public string Country { get; set; } = string.Empty;

        public override string ToString()
        {
            var stringBuilder = new List<string>(6);
            if (!string.IsNullOrWhiteSpace(CommonName))
            {
                stringBuilder.Add($"CN={CommonName}");
            }

            if (!string.IsNullOrWhiteSpace(OrganizationUnit))
            {
                stringBuilder.Add($"OU={OrganizationUnit}");
            }

            if (!string.IsNullOrWhiteSpace(Organization))
            {
                stringBuilder.Add($"O={Organization}");
            }

            if (!string.IsNullOrWhiteSpace(City))
            {
                stringBuilder.Add($"L={City}");
            }

            if (!string.IsNullOrWhiteSpace(State))
            {
                stringBuilder.Add($"ST={State}");
            }

            if (!string.IsNullOrWhiteSpace(Country))
            {
                stringBuilder.Add($"C={Country}");
            }

            return string.Join(",", stringBuilder);
        }
    }
}
