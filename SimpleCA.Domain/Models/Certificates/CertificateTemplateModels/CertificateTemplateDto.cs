﻿namespace SimpleCA.Domain.Models.Certificates
{
    
    public class CertificateTemplateDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Название шаблона
        /// </summary>
        public string Name { get; set; } = string.Empty;

        /// <summary>
        /// Строка, содержащая CN (Common Name),
        /// OU (Organizational Unit),
        /// O (Organization),
        /// L (City),
        /// ST (State),
        /// C (Contry)
        /// </summary>
        public string Subject { get; set; } = string.Empty;
        
        public CertificateSubject? ParsedSubject { get; set; } 

        /// <summary>
        /// Время, которое сертификат будет действителен. в днях
        /// </summary>
        public int ValidPeriod { get; set; } = 360;

        /// <summary>
        /// Использовать ли шаблон для проверки подлинности клиента
        /// </summary>
        public bool ExtendedKeyUsageClientAuth { get; set; } = true;

        /// <summary>
        /// Использовать ли шаблон для проверки подлинности сервера.
        /// </summary>
        public bool ExtendedKeyUsageServerAuth { get; set; } = false;
    }
}
