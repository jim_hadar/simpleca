namespace SimpleCA.Domain.Models.Certificates
{
    public class CertificateTemplateUpdateDto : CertificateTemplateCreateDto
    {
        public int Id { get; set; }
    }
}
