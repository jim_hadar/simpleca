namespace SimpleCA.Domain.Models.Certificates
{
    public enum SubjectAlternativeNameType
    {
        /// <summary>
        /// DomainName
        /// </summary>
        DnsName = 2,

        /// <summary>
        /// IP адрес
        /// </summary>
        IpAddress = 4,

        /// <summary>
        /// URI
        /// </summary>
        Uri = 8,

        /// <summary>
        /// A string representation of distinguished name(s)
        /// </summary>
        DirectoryName = 16
    }
}
