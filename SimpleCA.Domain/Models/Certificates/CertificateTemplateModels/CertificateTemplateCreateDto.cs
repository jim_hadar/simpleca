namespace SimpleCA.Domain.Models.Certificates
{
    public class CertificateTemplateCreateDto : CertificateSubject
    {
        public string Name { get; set; } = string.Empty;
        
        /// <summary>
        /// Время, которое сертификат будет действителен. в днях
        /// </summary>
        public int ValidPeriod { get; set; } = 360;

        /// <summary>
        /// Использовать ли шаблон для проверки подлинности клиента
        /// </summary>
        public bool ExtendedKeyUsageClientAuth { get; set; } = true;

        /// <summary>
        /// Использовать ли шаблон для проверки подлинности сервера.
        /// </summary>
        public bool ExtendedKeyUsageServerAuth { get; set; } = false;
    }
}
