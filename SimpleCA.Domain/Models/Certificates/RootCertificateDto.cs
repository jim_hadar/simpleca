﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SimpleCA.Domain.Models.Certificates
{
    public class RootCertificateDto
    {
        /// <summary>
        /// ID корневого сертификата
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Friendly название корневого сертификата
        /// </summary>
        public string Name { get; set; } = string.Empty;
        
        /// <summary>
        /// Описание.
        /// </summary>
        public string? Description { get; set; }
        
        /// <summary>
        /// Версия
        /// </summary>
        public int Version { get; set; }
        
        /// <summary>
        /// Субъект
        /// </summary>
        public string Subject { get; set; } = string.Empty;
        
        /// <summary>
        /// Время действия с 
        /// </summary>
        public DateTime ValidFrom { get; set; }

        /// <summary>
        /// Время действия по
        /// </summary>
        public DateTime ValidTo { get; set; }

        /// <summary>
        /// Серийный номер.
        /// </summary>
        public string SerialNumber { get; set; } = string.Empty;

        /// <summary>
        /// 
        /// </summary>
        public string SignatureAlgorithm { get; set; } = string.Empty;
    }
}
