using System;

namespace SimpleCA.Domain.Models.Users
{
    /// <summary>
    /// Модель пользователя.
    /// </summary>
    public class UserDto : UserUpdateDto
    {
        /// <summary>
        /// Название роли пользователя.
        /// </summary>
        public string RoleName { get; set; } = string.Empty;
        
        /// <summary>
        /// Дата изменения пользователя.
        /// </summary>
        public DateTime DateChanged { get; set; }
    }
}
