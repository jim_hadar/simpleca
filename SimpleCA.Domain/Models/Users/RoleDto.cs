namespace SimpleCA.Domain.Models.Users
{
    /// <summary>
    /// Роль пользователя.
    /// </summary>
    public class RoleDto
    {
        /// <summary>
        /// Идентификатор роли.
        /// </summary>
        public int Id { get; set; }
        
        /// <summary>
        /// Название роли.
        /// </summary>
        public string Name { get; set; } = string.Empty;
    }
}
