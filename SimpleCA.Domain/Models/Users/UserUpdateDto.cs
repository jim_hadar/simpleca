namespace SimpleCA.Domain.Models.Users
{
    /// <summary>
    /// Модель обновления пользователя
    /// </summary>
    public class UserUpdateDto : UserAddDto
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public int Id { get; set; }
    }
}
