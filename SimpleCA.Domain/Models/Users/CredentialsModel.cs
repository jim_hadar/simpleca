namespace SimpleCA.Domain.Models.Users
{
    /// <summary>
    /// Данные для авторизации
    /// </summary>
    public sealed class CredentialsModel
    {
        /// <summary>
        /// Имя пользователя.
        /// </summary>
        public string? UserName { get; set; }

        /// <summary>
        /// Пароль пользователя.
        /// </summary>
        public string? Password { get; set; }
    }
}
