using SimpleCA.Domain.Models.Enums;

namespace SimpleCA.Domain.Models.Users
{
    /// <summary>
    /// Модель создания пользователя.
    /// </summary>
    public class UserAddDto
    {
        /// <summary>
        /// Имя пльзователя.
        /// </summary>
        public string UserName { get; set; } = string.Empty;
        
        /// <summary>
        /// Описание пользователя.
        /// </summary>
        public string? Description { get; set; } = null;
        
        /// <summary>
        /// Идентификатор роли пользователя.
        /// </summary>
        public UserRole RoleId { get; set; }
        
        /// <summary>
        /// Пароль пользователя.
        /// </summary>
        public string? Password { get; set; }
    }
}
