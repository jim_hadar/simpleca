namespace SimpleCA.Domain.Models.Users
{
    /// <summary>
    /// Ответ сервера в случае успешной аутентификации
    /// </summary>
    public sealed class JwtAuthSuccessResponse
    {
        /// <summary>
        /// Идентификатор пользователя Voltron
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// JWT token
        /// </summary>
        public string? AuthToken { get; set; }

        /// <summary>
        /// Имя пользователя
        /// </summary>
        public string? UserName { get;set; }

        /// <summary>
        /// Время жизни JWT token
        /// </summary>
        public int ExpiresIn { get; set; }
    }
}
