using System.ComponentModel.DataAnnotations;

namespace SimpleCA.Domain.Models.Enums
{
    /// <summary>
    /// Роль пользователя
    /// </summary>
    public enum UserRole
    {
        /// <summary>
        /// The readonly
        /// </summary>
        [Display(Name = "Доступ для чтения")]
        Readonly = 1,

        /// <summary>
        /// The full access
        /// </summary>
        [Display(Name = "Полный доступ")]
        FullAccess = 2,
    }
}
