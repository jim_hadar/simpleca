using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace SimpleCA.Domain.Models.Paging
{
    /// <inheritdoc />
    public abstract class DefaultFilter<T> : IFilter
    {
        /// <inheritdoc />
        public SortOrderBy OrderBy { get; set; } = SortOrderBy.ASC;

        private string _sortField = "Id";

        protected static readonly HashSet<string> _allowedDefaultSortableFields =
            typeof(T).GetProperties().Select(_ => _.Name.ToLower(CultureInfo.CurrentCulture)).ToHashSet();

        /// <summary>
        /// Массив полей, по которым разрешена сортировка
        /// </summary>
        protected virtual HashSet<string> AllowedSortingFields => _allowedDefaultSortableFields;

        /// <inheritdoc />
        public string SortField
        {
            get => _sortField;
            set
            {
                if (!string.IsNullOrWhiteSpace(value))
                {
                    var fields = AllowedSortingFields;
                    string lowerValue = value.ToLower(CultureInfo.CurrentCulture);
                    if (!fields.Contains(lowerValue))
                    {
                        throw new Exception("not valid sort field");
                    }

                    _sortField = lowerValue;
                }
                else
                {
                    _sortField = "Id";
                }
            }
        }

        /// <inheritdoc />
        public string Search { get; set; } = "";
    }
}
