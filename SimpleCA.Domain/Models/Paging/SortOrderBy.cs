namespace SimpleCA.Domain.Models.Paging
{
    /// <summary>
    /// Сортировать по убыванию / возрастанию
    /// </summary>
    public enum SortOrderBy
    {
        ASC = 1,
        DESC = 2,
    }
}
