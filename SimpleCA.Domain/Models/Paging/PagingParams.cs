using System;
using Microsoft.AspNetCore.Mvc;

namespace SimpleCA.Domain.Models.Paging
{
    [ModelBinder(typeof(PagingModelBinder))]
    public sealed class PagingParams<TFilter> : PagingParams
        where TFilter : class, IFilter, new()
    {
        /// <summary>
        /// Фильтр.
        /// </summary>
        public TFilter Filter { get; }

        public PagingParams()
        {
            Filter = Activator.CreateInstance<TFilter>();
        }
    }

    public abstract class PagingParams
    {
        /// <summary>
        /// Номер страницы
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// Размер страницы
        /// </summary>
        public int PageSize { get; set; }
    }
}
