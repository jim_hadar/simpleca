using SimpleCA.Domain.Models.Certificates.IssuedCertificateModels;

namespace SimpleCA.Domain.Models.Paging.Filters
{
    public class IssuedCertificateFilter : DefaultFilter<IssuedCertificateDto>
    {
        
    }
}
