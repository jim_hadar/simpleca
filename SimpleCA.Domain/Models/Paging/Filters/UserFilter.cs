using SimpleCA.Domain.Models.Users;

namespace SimpleCA.Domain.Models.Paging.Filters
{
    /// <summary>
    /// Фильтр для пользователей.
    /// </summary>
    public class UserFilter : DefaultFilter<UserDto>
    {
        
    }
}
