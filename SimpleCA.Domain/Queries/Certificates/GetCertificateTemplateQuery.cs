using MediatR;
using SimpleCA.Domain.Models.Certificates;

namespace SimpleCA.Domain.Queries.Certificates
{
    /// <summary>
    /// Получение шаблона сертификата по идентификатору.
    /// </summary>
    public class GetCertificateTemplateQuery : IRequest<CertificateTemplateDto>
    {
        public int Id { get; }
        public GetCertificateTemplateQuery(int id) => Id = id;
    }
}
