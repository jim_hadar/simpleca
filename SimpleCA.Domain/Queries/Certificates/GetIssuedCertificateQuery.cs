using MediatR;
using SimpleCA.Domain.Models.Certificates.IssuedCertificateModels;

namespace SimpleCA.Domain.Queries.Certificates
{
    /// <summary>
    /// Получение сертификата по его идентификатору.
    /// </summary>
    public class GetIssuedCertificateQuery : IRequest<IssuedCertificateDto>
    {
        public int Id { get; }

        public GetIssuedCertificateQuery(int id) => Id = id;
    }
}
