﻿using System.Collections.Generic;
using MediatR;
using SimpleCA.Domain.Models.Certificates;
using SimpleCA.Domain.Models.Paging;
using SimpleCA.Domain.Models.Paging.Filters;

namespace SimpleCA.Domain.Queries.Certificates
{
    /// <summary>
    /// Получение шаблонов на сертификаты.
    /// </summary>
    public class GetCertificateTemplatesQuery : IRequest<List<CertificateTemplateDto>>
    {
        public static GetCertificateTemplatesQuery Instance { get; } = new GetCertificateTemplatesQuery();
    }
}
