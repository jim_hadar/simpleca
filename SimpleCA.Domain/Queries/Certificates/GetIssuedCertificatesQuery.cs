using System.Collections.Generic;
using MediatR;
using SimpleCA.Domain.Models.Certificates.IssuedCertificateModels;
using SimpleCA.Domain.Models.Paging;
using SimpleCA.Domain.Models.Paging.Filters;

namespace SimpleCA.Domain.Queries.Certificates
{
    /// <summary>
    /// Запрос получения списка выпущенных сертификатов с постраничной разбивкой.
    /// </summary>
    public class GetIssuedCertificatesQuery : IRequest<PagedList<IssuedCertificateDto>>
    {
        /// <summary>
        /// Фильтр
        /// </summary>
        public PagingParams<IssuedCertificateFilter> Filter { get; }
        public GetIssuedCertificatesQuery(PagingParams<IssuedCertificateFilter> filter) => Filter = filter;
    }
}
