﻿using MediatR;
using SimpleCA.Domain.Models.Certificates.IssuedCertificateModels;

namespace SimpleCA.Domain.Queries.Certificates
{
    /// <summary>
    /// Команда экспорта выпущенного сертификата.
    /// </summary>
    public class ExportIssuedCertificateQuery : IRequest<byte[]>
    {
        public ExportIssuedCertificateDto Model { get; }

        public ExportIssuedCertificateQuery(ExportIssuedCertificateDto model) => Model = model;
    }
}
