﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace SimpleCA.Domain.Queries.Certificates
{
    /// <summary>
    /// Запрос получения файла, содержащего список отзыва Crl.
    /// </summary>
    public sealed class GetCrlFileQuery : IRequest<byte[]>
    {
        public static GetCrlFileQuery Instance { get; } = new GetCrlFileQuery();
    }
}
