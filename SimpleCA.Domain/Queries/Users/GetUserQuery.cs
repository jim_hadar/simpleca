using MediatR;
using SimpleCA.Domain.Models.Users;

namespace SimpleCA.Domain.Queries.Users
{
    /// <summary>
    /// Запрос получения пользователя по его идентификатору.
    /// </summary>
    public class GetUserQuery : IRequest<UserDto>
    {
        /// <summary>
        /// Идентификатор пользователя.
        /// </summary>
        public int UserId { get; }
        
        public GetUserQuery(int userId) => UserId = userId;
    }
}
