using MediatR;
using SimpleCA.Domain.Models.Paging;
using SimpleCA.Domain.Models.Paging.Filters;
using SimpleCA.Domain.Models.Users;

namespace SimpleCA.Domain.Queries.Users
{
    /// <summary>
    /// Запрос получения пользователей с постраничной разбивкой и сортировкой.
    /// </summary>
    public class GetPagedUsersQuery : IRequest<PagedList<UserDto>>
    {
        public PagingParams<UserFilter> Filter { get; }
        
        public GetPagedUsersQuery(PagingParams<UserFilter> filter) => Filter = filter;
    }
}
