using System.Collections.Generic;
using MediatR;
using SimpleCA.Domain.Models.Users;

namespace SimpleCA.Domain.Queries.Users
{
    /// <summary>
    /// Запрос получения списка ролей пользвоателей.
    /// </summary>
    public class GetUserRolesQuery : IRequest<List<RoleDto>>
    {
        public static GetUserRolesQuery Instance { get; } = new GetUserRolesQuery();
    }
}
