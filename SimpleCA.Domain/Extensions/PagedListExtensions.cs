using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SimpleCA.Domain.Models.Paging;

namespace SimpleCA.Domain.Extensions
{
    public static class PagedListExtensions
    {
        public static Task<List<T>> TakePaged<T>(this IQueryable<T> data, PagingParams pagingParams)
        {
            return data.Skip((pagingParams.PageNumber - 1) * pagingParams.PageSize)
                .Take(pagingParams.PageSize).ToListAsync();
        }
        public static PagedList<T> ToPagedList<T>(this List<T> list, PagingParams pagingParams, int totalItems)
            where T : class, new()
        {
            return new PagedList<T>
            {
                Data = list,
                PageNumber = pagingParams.PageNumber,
                PageSize = pagingParams.PageSize,
                TotalItems = totalItems
            };
        }
    }
}
