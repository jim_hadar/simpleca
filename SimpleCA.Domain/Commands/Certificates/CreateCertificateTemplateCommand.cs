﻿using MediatR;
using SimpleCA.Domain.Models.Certificates;

namespace SimpleCA.Domain.Commands.Certificates
{
    /// <summary>
    /// Команда создания шаблона сертификата.
    /// </summary>
    public class CreateCertificateTemplateCommand : IRequest<int>
    {
        public CertificateTemplateCreateDto Model { get; }

        public CreateCertificateTemplateCommand(CertificateTemplateCreateDto model) => Model = model;
    }
}
