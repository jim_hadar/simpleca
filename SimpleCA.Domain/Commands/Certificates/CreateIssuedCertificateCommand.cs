using MediatR;
using SimpleCA.Domain.Models.Certificates.IssuedCertificateModels;

namespace SimpleCA.Domain.Commands.Certificates
{
    /// <summary>
    /// Команда выпуска сертификата и сохранения его в БД.
    /// </summary>
    public class CreateIssuedCertificateCommand : IRequest<int>
    {
        public IssuedCertificateCreateDto Model { get; }
        public CreateIssuedCertificateCommand(IssuedCertificateCreateDto model) => Model = model;
    }
}
