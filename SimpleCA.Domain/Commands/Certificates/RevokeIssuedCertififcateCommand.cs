﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace SimpleCA.Domain.Commands.Certificates
{
    /// <summary>
    /// Команда отзыва сертификата.
    /// </summary>
    public sealed class RevokeIssuedCertififcateCommand : IRequest<Unit>
    {
        /// <summary>
        /// Идентификатор отзываемого сертификата.
        /// </summary>
        public int CertificateId { get; }

        public RevokeIssuedCertififcateCommand(int certificateId) => CertificateId = certificateId;
    }
}
