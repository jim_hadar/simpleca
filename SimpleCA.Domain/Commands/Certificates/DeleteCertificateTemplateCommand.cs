using MediatR;
using SimpleCA.Domain.Models.Paging;

namespace SimpleCA.Domain.Commands.Certificates
{
    /// <summary>
    /// Команда удаления шаблона на сертификат.
    /// </summary>
    public class DeleteCertificateTemplateCommand : IRequest<Unit>
    {
        public int Id { get; }
        
        public DeleteCertificateTemplateCommand(int id) => Id = id;
    }
}
