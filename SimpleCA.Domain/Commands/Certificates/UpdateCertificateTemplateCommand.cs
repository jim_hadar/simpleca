using MediatR;
using SimpleCA.Domain.Models.Certificates;

namespace SimpleCA.Domain.Commands.Certificates
{
    /// <summary>
    /// Команда обновления шаблона на сертификат.
    /// </summary>
    public class UpdateCertificateTemplateCommand : IRequest<Unit>
    {
        public CertificateTemplateUpdateDto Model { get; }
        public UpdateCertificateTemplateCommand(CertificateTemplateUpdateDto model) => Model = model;
    }
}
