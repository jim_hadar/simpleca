using MediatR;

namespace SimpleCA.Domain.Commands.Users
{
    /// <summary>
    /// Команда разлогинивания пользователя.
    /// </summary>
    public class LogoutUserCommand : INotification
    {
        
    }
}
