using MediatR;
using SimpleCA.Domain.Models.Users;

namespace SimpleCA.Domain.Commands.Users
{
    /// <summary>
    /// Команда создания пользователя. Возвращает идентификатор созданного пользователя.
    /// </summary>
    public class CreateUserCommand : IRequest<int>
    {
        /// <summary>
        /// Модель пользователя.
        /// </summary>
        public UserAddDto Model { get; }
        
        public CreateUserCommand(UserAddDto model) => Model = model;
        
    }
}
