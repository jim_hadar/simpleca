using MediatR;
using SimpleCA.Domain.Models.Users;

namespace SimpleCA.Domain.Commands.Users
{
    /// <summary>
    /// Команда обновления пользователя.
    /// </summary>
    public class UpdateUserCommand : INotification
    {
        /// <summary>
        /// Модель обновления пользователя.
        /// </summary>
        public UserUpdateDto Model { get; }
        
        public UpdateUserCommand(UserUpdateDto model) => Model = model;
    }
}
