using MediatR;
using SimpleCA.Domain.Models.Users;

namespace SimpleCA.Domain.Commands.Users
{
    /// <summary>
    /// Команда для авторизации пользователя
    /// </summary>
    public class LoginUserCommand : IRequest<JwtAuthSuccessResponse>
    {
        public CredentialsModel Model { get; set; }
        public LoginUserCommand(CredentialsModel model) => Model = model;
    }
}
