using MediatR;

namespace SimpleCA.Domain.Commands.Users
{
    /// <summary>
    /// Модель удаления пользователя.
    /// </summary>
    public class DeleteUserCommand : INotification
    {
        /// <summary>
        /// Идентификатор пользователя для удаления.
        /// </summary>
        public int UserId { get;  }
        
        public DeleteUserCommand(int userId) => UserId = userId;
    }
}
